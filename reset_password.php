<?php
@session_start();

// System
define( 'TTH_SYSTEM', true );
require_once(str_replace( DIRECTORY_SEPARATOR, '/', dirname( __file__ ) ) . '/define.php');
include_once(_F_FUNCTIONS . DS . "Function.php");
try {
	$db =  new ActiveRecord(TTH_DB_HOST, TTH_DB_USER, TTH_DB_PASS, TTH_DB_NAME);
}
catch(DatabaseConnException $e) {
	echo $e->getMessage();
}

if(isset($_POST['forgot_user_email'])) {
	$user = $_POST['forgot_user_email'];
	$db->table = "core_user";
	$db->condition = "`user_name` LIKE '".$db->clearText($user)."' AND `is_active` = 1 OR `email` = '".$db->clearText($user)."' AND `is_active` = 1";
	$db->order = "";
	$db->limit = 1;
	$rows = $db->select();
	if($db->RowCount>0) {
		$randomString = md5(getRandomString(10));
		$db->table = "core_user";
		$data = array(
			'password' => $db->clearText($randomString)
		);
		$db->condition = "`user_id` = ". intval($rows[0]['user_id']);
		$db->update($data);

		$domain = $_SERVER['HTTP_HOST'];
		$emailTo = $rows[0]['email'];
		$nameTo = $rows[0]['full_name'];

		$subject = "[Olala Apps | Olala Group] Thiết lập lại mật khẩu từ ".$domain;
		$message = 'Chào '.$rows[0]['user_name'].',<br/><br/>'.
		'Bạn đã yêu cầu hệ thống thiết lập mật khẩu cho tài khoản của bạn trên trang Quản trị Website từ <b>'.$domain.'</b>'.
		', với lý do bạn đã quên mật khẩu hiện tại.<br/>'.
		'Quá trình thiết lập mật khẩu mới được hệ thống thực hiện thành công.<br/><br/>Thông tin thiết lập:<br/>'.
		'Tên đăng nhập: <b>'.$rows[0]['user_name'].'</b><br/>'.
		'Mật khẩu: <b>'.$randomString.'</b><br/><br/>'.
		'Bạn nên <b>thay đổi mật khẩu</b> khác ngay sau khi đăng nhập vào hệ thống thành công!<br/>'.
		'Hỗ trợ kỹ thuật (24/7): (+84)97 477 9085 -  Email: support@olalagroup.vn<br/><br/>'.
		'-----<br/><a href="https://olalaweb.vn" title="OLALA | Thiết kế website, Phần mềm"><font face="arial, helvetica, sans-serif" style="color: #3b699a; font-weight: bold; text-decoration: none;">OLALA Technology Solutions</font></a>';
		$send_mail = sendMailFn($emailTo, $nameTo, $subject, $message, '');
		if($send_mail == TRUE)
			echo 'Mật khẩu mới đã được gửi về E-mail của bạn.';
		else
			echo 'Có lỗi từ hệ thống, không thể thực hiện thao tác.';
	}
	else echo 'Tài khoản bạn nhập không tồn tại trong hệ thống.';
}
else echo 'Nội dung không tồn tại.';
