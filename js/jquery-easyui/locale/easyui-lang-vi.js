if ($.fn.pagination){
	$.fn.pagination.defaults.beforePageText = 'Trang';
	$.fn.pagination.defaults.afterPageText = '/ {pages}';
	$.fn.pagination.defaults.displayMsg = 'Hiển thị {from} đến {to} / {total} mục';
}
if ($.fn.datagrid){
	$.fn.datagrid.defaults.loadMsg = 'Đang xử lý, vui lòng đợi ...';
}
if ($.fn.treegrid && $.fn.datagrid){
	$.fn.treegrid.defaults.loadMsg = $.fn.datagrid.defaults.loadMsg;
}
if ($.messager){
	$.messager.defaults.ok = 'OK';
	$.messager.defaults.cancel = 'Hủy bỏ';
}
$.map(['validatebox','textbox','passwordbox','filebox','searchbox',
		'combo','combobox','combogrid','combotree',
		'datebox','datetimebox','numberbox',
		'spinner','numberspinner','timespinner','datetimespinner'], function(plugin){
	if ($.fn[plugin]){
		$.fn[plugin].defaults.missingMessage = 'Trường yêu cầu bắt buộc.';
	}
});
if ($.fn.validatebox){
	$.fn.validatebox.defaults.rules.email.message = 'Vui lòng nhập một địa chỉ email hợp lệ.';
	$.fn.validatebox.defaults.rules.url.message = 'Vui lòng nhập một URL hợp lệ.';
	$.fn.validatebox.defaults.rules.length.message = 'Vui lòng nhập một giá trị giữa {0} và {1}.';
	$.fn.validatebox.defaults.rules.remote.message = 'Vui lòng sửa trường này.';
}
if ($.fn.calendar){
	$.fn.calendar.defaults.weeks = ['CN','T2','T3','T4','T5','T6','T7'];
	$.fn.calendar.defaults.months = ['Thg 1', 'Thg 2', 'Thg 3', 'Thg 4', 'Thg 5', 'Thg 6', 'Thg 7', 'Thg 8', 'Thg 9', 'Thg 10', 'Thg 11', 'Thg 12'];
}
if ($.fn.datebox){
	$.fn.datebox.defaults.currentText = 'Hôm nay';
	$.fn.datebox.defaults.closeText = 'Đóng';
	$.fn.datebox.defaults.okText = 'OK';
}
if ($.fn.datetimebox && $.fn.datebox){
	$.extend($.fn.datetimebox.defaults,{
		currentText: $.fn.datebox.defaults.currentText,
		closeText: $.fn.datebox.defaults.closeText,
		okText: $.fn.datebox.defaults.okText
	});
}
