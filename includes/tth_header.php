<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
?>

<header class="header fixed-top clearfix">
	<!--logo start-->
	<div class="brand">
		<a href="<?php echo HOME_URL_LANG;?>" class="logo logo-sm">
			<img src="<?php echo getConstant('file_logo');?>" alt="<?php echo getConstant('title');?>">
		</a>
		<!-- <div class="sidebar-toggle-box">
			<div class="fa fa-bars"></div>
		</div> -->
	</div>
	<!--logo end-->
	<div class="top-nav clearfix">
        
		<!--clock & user info start-->
		<ul class="nav pull-right top-menu">
			<li class="clock">
				<div class="clockcenter">
					<digiclock>12:45:25</digiclock><br>
					<digidate style="white-space: nowrap;">Thứ 2, 01/01/2014</digidate>
				</div>
			</li>
            <?php
            if(!empty($account["id"])) {
            ?>
			<!-- user login dropdown start-->
			<li class="dropdown">
				<a data-toggle="dropdown" class="dropdown-toggle username" href="#">
					<?php
					$info_user = array();
					$info_user = getInfoUser($account["id"]);
					echo $info_user[4] . '&nbsp <span>' . $info_user[0] . '</span>';
					?>
					<b class="caret"></b>
				</a>
				<ul class="dropdown-menu extended logout">
					
					<li><a href="javascript:_postback();" onclick="Forward('<?php echo HOME_URL_LANG . '/user/user-profile&active=profile';?>');"><i class="fa fa-user"></i>Thông tin cá nhân</a></li>
					<li><a href="javascript:_postback();" onclick="Forward('<?php echo HOME_URL_LANG . '/user/user-profile&active=password';?>');"><i class="fa fa-key"></i>Đổi mật khẩu</a></li>
					<li class="divider"></li>
					<li><a href="javascript:_postback();" onclick="Forward('<?php echo HOME_URL_LANG . '/page/view&active=regulation';?>');"><i class=" fa fa-book"></i>Quy định chung</a></li>
					<li><a href="javascript:_postback();" onclick="Forward('<?php echo HOME_URL_LANG . '/page/view&active=tutorial';?>');"><i class="fa fa-question-circle"></i>Hướng dẫn sử dụng</a></li>
					<li class="divider"></li>
					<li><a href="javascript:_postback();" onclick="Forward('<?php echo HOME_URL_LANG . '/?lock=OK';?>');"><i class="fa fa-lock"></i> Khóa màn hình</a></li>
					<li><a href="javascript:_postback();" onclick="Forward('<?php echo HOME_URL_LANG . '/?logout=OK';?>');"><i class="fa fa-sign-out"></i> Đăng xuất</a></li>

				</ul>
			</li>
			<!-- user login dropdown end -->
            <?php } else { ?>
            <li>
                <button class="btn btn-danger btn-round" onclick="Forward('<?php echo HOME_URL_LANG . '/?ol=login';?>');">Đăng nhập</button>
            </li>
            <?php } ?>
		</ul>
		<!--clock & user info end-->
	</div>
</header>