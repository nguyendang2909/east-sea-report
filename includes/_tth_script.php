<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
?>
<!-- Core js-->
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/bs3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery.dcjqaccordion.2.7.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery.nicescroll.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery.easing.1.3.min.js"></script>
<!-- DataTables JavaScript -->
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/data-tables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/data-tables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/data-tables/dataTables.rowGroup.min.js"></script>
<!-- Bootstrap-select JavaScript -->
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/bootstrap-select/js/bootstrap-select.min.js"></script>
<!-- Bootstrap-typeahead JavaScript -->
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/bootstrap-typeahead/bootstrap-typeahead.min.js"></script>
<!-- Bootstrap-tagsinput JavaScript -->
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
<!-- Common script init for all pages-->
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/fileinput/fileinput.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/auto-numeric.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery.calendar/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/autosize.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery-easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/scripts.js?ver=3.291118"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery.boxes.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/openlayers/ol.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/openlayers/ol-popup.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/openlayers/ol-layerswitcher.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/daterangepicker/daterangepicker.js"></script>
<?php
if(in_array($tth[TTH_PATH], array('blog', 'config', 'agency', 'account', 'tracking'))) {
?>
<!-- CKEditor -->
<script type="text/javascript" src="<?php echo HOME_URL; ?>/editor/ckeditor/ckeditor.js?ver=3.071218"></script>
<!-- CKFinder -->
<script type="text/javascript" src="<?php echo HOME_URL; ?>/editor/ckfinder/ckfinder.js?ver=3.071218"></script>
<?php } ?>
<!--[if lt IE 9]>
<script src="<?php echo HOME_URL; ?>/js/ie8-responsive-file-warning.js"></script>
<![endif]-->