<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}
//

/** Trạng thái phân quyền chung cho từng loại | Loại phân quyền có tác động cao nhất.
 * @param $role_id
 * @param $slug
 * @return string
 * @throws DatabaseConnException
 */
function showStatusCoreOl($role_id, $slug)
{
  global $db;
  $db->table = "core_privilege";
  $db->condition = "`type` = 'ol' and `role_id` = " . $role_id . " and `privilege_slug` = '" . $slug . "'";
  $db->order = "";
  $db->limit = 1;
  $db->select();
  if ($db->RowCount > 0) {
    return '<div class="btn-event-open" data-toggle="tooltip" data-placement="top" title="Đóng" onclick="edit_core_ol($(this), ' . $role_id . ', \'' . $slug . '\');" rel="0">1</div>';
  } else {
    return '<div class="btn-event-close" data-toggle="tooltip" data-placement="top" title="Mở" onclick="edit_core_ol($(this), ' . $role_id . ', \'' . $slug . '\');" rel="1">0</div>';
  }
}

function showCoreAccount($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = $role_id and `type` = 'account'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select("`privilege_slug`");
  foreach ($rows as $row) {
    $privilege[] = $row['privilege_slug'];
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-account" name="variable[]" <?php if (in_array('user-add', $privilege)) echo "checked"; ?> value="user-add"> Thêm thành viên
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-account" name="variable[]" <?php if (in_array('user-edit', $privilege)) echo "checked"; ?> value="user-edit"> Chỉnh sửa thành viên
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-account" name="variable[]" <?php if (in_array('user;delete', $privilege)) echo "checked"; ?> value="user;delete"> Xóa thành viên
        </label>
      </div>
    </div>
  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-account" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}

function showCoreFieldSituation($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = $role_id and `type` = 'field-situation'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select("`privilege_slug`");
  foreach ($rows as $row) {
    $privilege[] = $row['privilege_slug'];
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-field-situation" name="variable[]" <?php if (in_array('field-situation-add', $privilege)) echo "checked"; ?> value="field-situation-add"> Thêm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-field-situation" name="variable[]" <?php if (in_array('field-situation-edit', $privilege)) echo "checked"; ?> value="field-situation-edit"> Chỉnh sửa
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-field-situation" name="variable[]" <?php if (in_array('field-situation;delete', $privilege)) echo "checked"; ?> value="field-situation;delete"> Xóa
        </label>
      </div>
    </div>
  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-field-situation" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}

function showCoreMonthReport($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = $role_id and `type` = 'month-report'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select("`privilege_slug`");
  foreach ($rows as $row) {
    $privilege[] = $row['privilege_slug'];
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-month-report" name="variable[]" <?php if (in_array('month-report-add', $privilege)) echo "checked"; ?> value="month-report-add"> Thêm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-month-report" name="variable[]" <?php if (in_array('month-report-edit', $privilege)) echo "checked"; ?> value="month-report-edit"> Chỉnh sửa
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-month-report" name="variable[]" <?php if (in_array('month-report;delete', $privilege)) echo "checked"; ?> value="month-report;delete"> Xóa
        </label>
      </div>
    </div>
  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-month-report" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}

function showOpinion($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = $role_id and `type` = 'opinion'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select("`privilege_slug`");
  foreach ($rows as $row) {
    $privilege[] = $row['privilege_slug'];
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-opinion" name="variable[]" <?php if (in_array('opinion-add', $privilege)) echo "checked"; ?> value="opinion-add"> Thêm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-opinion" name="variable[]" <?php if (in_array('opinion-edit', $privilege)) echo "checked"; ?> value="opinion-edit"> Chỉnh sửa
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-opinion" name="variable[]" <?php if (in_array('opinion;delete', $privilege)) echo "checked"; ?> value="opinion;delete"> Xóa
        </label>
      </div>
    </div>
  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-opinion" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}

function showCoreEastSeaReport($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = $role_id and `type` = 'east-sea-report'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select("`privilege_slug`");
  foreach ($rows as $row) {
    $privilege[] = $row['privilege_slug'];
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-east-sea-report" name="variable[]" <?php if (in_array('east-sea-report', $privilege)) echo "checked"; ?> value="east-sea-report"> Báo cáo Biển Đông
        </label>
      </div>
    </div>
  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-east-sea-report" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}

function showCoreAgency($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = $role_id and `type` = 'agency'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select("`privilege_slug`");
  foreach ($rows as $row) {
    $privilege[] = $row['privilege_slug'];
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-agency" name="variable[]" <?php if (in_array('agency-add', $privilege)) echo "checked"; ?> value="agency-add"> Thêm đơn vị
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-agency" name="variable[]" <?php if (in_array('agency-edit', $privilege)) echo "checked"; ?> value="agency-edit"> Chỉnh sửa đơn vị
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-agency" name="variable[]" <?php if (in_array('agency-export', $privilege)) echo "checked"; ?> value="agency-export"> Xuất tệp
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-agency" name="variable[]" <?php if (in_array('agency;delete', $privilege)) echo "checked"; ?> value="agency;delete"> Xóa đơn vị
        </label>
      </div>
    </div>
  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-agency" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}

function showCoreRole($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = $role_id and `type` = 'role'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select("`privilege_slug`");
  foreach ($rows as $row) {
    $privilege[] = $row['privilege_slug'];
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-role" name="variable[]" <?php if (in_array('role-add', $privilege)) echo "checked"; ?> value="role-add"> Thêm nhóm chức năng
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-role" name="variable[]" <?php if (in_array('role-edit', $privilege)) echo "checked"; ?> value="role-edit"> Chỉnh sửa nhóm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-role" name="variable[]" <?php if (in_array('role;delete', $privilege)) echo "checked"; ?> value="role;delete"> Xóa nhóm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-role" name="variable[]" <?php if (in_array('dashboard', $privilege)) echo "checked"; ?> value="dashboard"> Phân quyền chức năng
        </label>
      </div>
    </div>
  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-role" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}

function showCoreJobs($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = $role_id and `type` = 'jobs'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select("`privilege_slug`");
  foreach ($rows as $row) {
    $privilege[] = $row['privilege_slug'];
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-jobs" name="variable[]" <?php if (in_array('job-add', $privilege)) echo "checked"; ?> value="job-add"> Thêm công việc
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-jobs" name="variable[]" <?php if (in_array('job-edit', $privilege)) echo "checked"; ?> value="job-edit"> Chỉnh sửa công việc
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-jobs" name="variable[]" <?php if (in_array('jobs;delete', $privilege)) echo "checked"; ?> value="jobs;delete"> Xóa công việc
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-jobs" name="variable[]" <?php if (in_array('my-job', $privilege)) echo "checked"; ?> value="my-job"> Công việc của bạn
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-jobs" name="variable[]" <?php if (in_array('my-add', $privilege)) echo "checked"; ?> value="my-add"> Công việc bạn giao
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-jobs" name="variable[]" <?php if (in_array('job-report', $privilege)) echo "checked"; ?> value="job-report"> Theo dõi báo cáo
        </label>
      </div>
    </div>
  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-jobs" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}

function showCoreMaps($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = $role_id and `type` = 'maps'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select("`privilege_slug`");
  foreach ($rows as $row) {
    $privilege[] = $row['privilege_slug'];
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-maps" name="variable[]" <?php if (in_array('tracking-map', $privilege)) echo "checked"; ?> value="tracking-map"> <strong>Bản đồ trinh sát</strong>
        </label>
      </div>
    </div>


    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-maps" name="variable[]" <?php if (in_array('maps-gis', $privilege)) echo "checked"; ?> value="maps-gis"> <strong>Bản đồ chuyên ngành</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-maps" name="variable[]" <?php if (in_array('maps-gis-add', $privilege)) echo "checked"; ?> value="maps-gis-add"> Thêm vị trí
        </label>
      </div>
    </div>
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-maps" name="variable[]" <?php if (in_array('maps-type', $privilege)) echo "checked"; ?> value="maps-type"> <strong>Loại vị trí</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-maps" name="variable[]" <?php if (in_array('maps-type-add', $privilege)) echo "checked"; ?> value="maps-type-add"> Thêm thể loại
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-maps" name="variable[]" <?php if (in_array('maps-type-edit', $privilege)) echo "checked"; ?> value="maps-type-edit"> Chỉnh sửa thể loại
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-maps" name="variable[]" <?php if (in_array('maps-type;delete', $privilege)) echo "checked"; ?> value="maps-type;delete"> Xóa thể loại
        </label>
      </div>
    </div>
  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-maps" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}


function showCoreCalendar($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = $role_id and `type` = 'calendar'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select("`privilege_slug`");
  foreach ($rows as $row) {
    $privilege[] = $row['privilege_slug'];
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-calendar" name="variable[]" <?php if (in_array('calendar-week', $privilege)) echo "checked"; ?> value="calendar-week"> Lịch tuần
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-calendar" name="variable[]" <?php if (in_array('calendar-month', $privilege)) echo "checked"; ?> value="calendar-month"> Lịch tháng
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-calendar" name="variable[]" <?php if (in_array('calendar-year', $privilege)) echo "checked"; ?> value="calendar-year"> Lịch năm
        </label>
      </div>
    </div>
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-calendar" name="variable[]" <?php if (in_array('calendar-list', $privilege)) echo "checked"; ?> value="calendar-list"> <strong>Cập nhật lịch</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-calendar" name="variable[]" <?php if (in_array('calendar-add', $privilege)) echo "checked"; ?> value="calendar-add"> Thêm lịch
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-calendar" name="variable[]" <?php if (in_array('calendar-edit', $privilege)) echo "checked"; ?> value="calendar-edit"> Chỉnh sửa lịch
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-calendar" name="variable[]" <?php if (in_array('calendar;delete', $privilege)) echo "checked"; ?> value="calendar;delete"> Xóa lịch
        </label>
      </div>
    </div>
  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-calendar" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}

function showCoreNetwork($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = $role_id and `type` = 'network'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select("`privilege_slug`");
  foreach ($rows as $row) {
    $privilege[] = $row['privilege_slug'];
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <!-- Hồ sơ mạng -->
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('network-profile', $privilege)) echo "checked"; ?> value="network-profile"> <strong>Hồ sơ mạng</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('network-profile-add', $privilege)) echo "checked"; ?> value="network-profile-add"> Thêm hồ sơ
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('network-profile-edit', $privilege)) echo "checked"; ?> value="network-profile-edit"> Chỉnh sửa hồ sơ
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('network-profile;delete', $privilege)) echo "checked"; ?> value="network-profile;delete"> Xóa hồ sơ
        </label>
      </div>
    </div>

    <!-- Địa chỉ IP -->
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('network-ip', $privilege)) echo "checked"; ?> value="network-ip"> <strong>Địa chỉ IP</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('network-ip-add', $privilege)) echo "checked"; ?> value="network-ip-add"> Thêm IP
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('network-ip-edit', $privilege)) echo "checked"; ?> value="network-ip-edit"> Chỉnh sửa IP
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('network-ip;delete', $privilege)) echo "checked"; ?> value="network-ip;delete"> Xóa IP
        </label>
      </div>
    </div>

    <!-- MỤC PHẦN CỨNG -->
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('device', $privilege)) echo "checked"; ?> value="device"> <strong>MỤC PHẦN CỨNG</strong>
        </label>
      </div>
      <div class="checkbox" style="margin-top: 15px;">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('device-item', $privilege)) echo "checked"; ?> value="device-item"> <strong>Thiết bị</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('device-item-add', $privilege)) echo "checked"; ?> value="device-item-add"> Thêm thiết bị
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('device-item-edit', $privilege)) echo "checked"; ?> value="device-item-edit"> Chỉnh sửa thiết bị
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('device-item;delete', $privilege)) echo "checked"; ?> value="device-item;delete"> Xóa thiết bị
        </label>
      </div>
      <div class="checkbox" style="margin-top: 15px;">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('device-category', $privilege)) echo "checked"; ?> value="device-category"> <strong>Nhóm thiết bị</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('device-category-add', $privilege)) echo "checked"; ?> value="device-category-add"> Thêm nhóm thiết bị
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('device-category-edit', $privilege)) echo "checked"; ?> value="device-category-edit"> Chỉnh sửa nhóm thiết bị
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('device-category;delete', $privilege)) echo "checked"; ?> value="device-category;delete"> Xóa nhóm thiết bị
        </label>
      </div>
    </div>
    <!-- MỤC PHẦN MỀM -->
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('software', $privilege)) echo "checked"; ?> value="software"> <strong>MỤC PHẦN MỀM</strong>
        </label>
      </div>
      <div class="checkbox" style="margin-top: 15px;">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('software-item', $privilege)) echo "checked"; ?> value="software-item"> <strong>Phần mềm</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('software-item-add', $privilege)) echo "checked"; ?> value="software-item-add"> Thêm phần mềm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('software-item-edit', $privilege)) echo "checked"; ?> value="software-item-edit"> Chỉnh sửa phần mềm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('software-item;delete', $privilege)) echo "checked"; ?> value="software-item;delete"> Xóa phần mềm
        </label>
      </div>
      <div class="checkbox" style="margin-top: 15px;">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('software-category', $privilege)) echo "checked"; ?> value="software-category"> <strong>Nhóm phần mềm</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('software-category-add', $privilege)) echo "checked"; ?> value="software-category-add"> Thêm nhóm phần mềm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('software-category-edit', $privilege)) echo "checked"; ?> value="software-category-edit"> Chỉnh sửa nhóm phần mềm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('software-category;delete', $privilege)) echo "checked"; ?> value="software-category;delete"> Xóa nhóm phần mềm
        </label>
      </div>
    </div>

    <!-- Quản trị mạng -->
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('network-admin', $privilege)) echo "checked"; ?> value="network-admin"> <strong>Quản trị mạng</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('network-admin-add', $privilege)) echo "checked"; ?> value="network-admin-add"> Thêm quản trị mạng
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('network-admin-edit', $privilege)) echo "checked"; ?> value="network-admin-edit"> Chỉnh sửa quản trị
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('network-admin;delete', $privilege)) echo "checked"; ?> value="network-admin;delete"> Xóa quản trị
        </label>
      </div>
    </div>

    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('website', $privilege)) echo "checked"; ?> value="website"> <strong>Trang cổng điện tử</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('website-add', $privilege)) echo "checked"; ?> value="website-add"> Thêm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('website-edit', $privilege)) echo "checked"; ?> value="website-edit"> Chỉnh sửa
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-network" name="variable[]" <?php if (in_array('website;delete', $privilege)) echo "checked"; ?> value="website;delete"> Xóa
        </label>
      </div>
    </div>

  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-network" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}

function showCoreReport($role_id)
{
  global $db;
  $privilege = array();

  $db->table = "core_privilege";
  $db->condition = "`role_id` = $role_id and `type` = 'report'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select("`privilege_slug`");
  foreach ($rows as $row) {
    $privilege[] = $row['privilege_slug'];
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />

  <div class="form-group clearfix">
    <!-- Bao cao KGM -->
    <div class="col-xs-12" style="margin-top: 25px;">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-report" name="variable[]" <?php if (in_array('kgm-report', $privilege)) echo "checked"; ?> value="kgm-report"> <strong>BÁO CÁO KGM</strong>
        </label>
      </div>
      <div class="row">
        <!-- Bao cao ATTT KGM -->
        <div class="col-left-ol">
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-report" name="variable[]" <?php if (in_array('kgm-attt-report', $privilege)) echo "checked"; ?> value="kgm-attt-report"> <strong>Tình hình ATTT</strong>
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('kgm-attt-report-add', $privilege)) echo "checked"; ?> value="kgm-attt-report-add"> Thêm tin
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('kgm-attt-report-edit', $privilege)) echo "checked"; ?> value="kgm-attt-report-edit"> Chỉnh sửa tin
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('kgm-attt-report;delete', $privilege)) echo "checked"; ?> value="kgm-attt-report;delete"> Xóa tin
            </label>
          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- Xac nhan -->
  <div class="col-xs-12" style="margin-top: 25px;">
    <label>
      <input type="checkbox" class="checked-all" rel="checkbox-report" data-toggle="tooltip" data-placement="top" title="Chọn tất cả">
      <button type="submit" class="btn btn-primary btn-round">Xác nhận</button>
    </label>
  </div>

<?php
}

function showCoreTracking($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = $role_id and `type` = 'tracking'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select("`privilege_slug`");
  foreach ($rows as $row) {
    $privilege[] = $row['privilege_slug'];
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />

  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-local', $privilege)) echo "checked"; ?> value="tracking-local"> <strong>Địa bàn</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-local-add', $privilege)) echo "checked"; ?> value="tracking-local-add"> Thêm địa bàn
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-local-edit', $privilege)) echo "checked"; ?> value="tracking-local-edit"> Chỉnh sửa địa bàn
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-local;delete', $privilege)) echo "checked"; ?> value="tracking-local;delete"> Xóa địa bàn
        </label>
      </div>
    </div>

    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('reactive', $privilege)) echo "checked"; ?> value="reactive"> <strong>Đối tượng theo dõi</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('reactive-add', $privilege)) echo "checked"; ?> value="reactive-add"> Thêm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('reactive-edit', $privilege)) echo "checked"; ?> value="reactive-edit"> Chỉnh sửa
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('reactive;delete', $privilege)) echo "checked"; ?> value="reactive;delete"> Xóa
        </label>
      </div>
    </div>

    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-topic', $privilege)) echo "checked"; ?> value="tracking-topic"> <strong>Chủ đề</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-topic-add', $privilege)) echo "checked"; ?> value="tracking-topic-add"> Thêm chủ đề
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-topic-edit', $privilege)) echo "checked"; ?> value="tracking-topic-edit"> Chỉnh sửa chủ đề
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-topic;delete', $privilege)) echo "checked"; ?> value="tracking-topic;delete"> Xóa chủ đề
        </label>
      </div>
    </div>

    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-reactive', $privilege)) echo "checked"; ?> value="tracking-reactive"> <strong>Các trang phản động</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-reactive-add', $privilege)) echo "checked"; ?> value="tracking-reactive-add"> Thêm trang phản động
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-reactive-edit', $privilege)) echo "checked"; ?> value="tracking-reactive-edit"> Chỉnh sửa trang
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-reactive;delete', $privilege)) echo "checked"; ?> value="tracking-reactive;delete"> Xóa trang
        </label>
      </div>
    </div>

    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-keywords', $privilege)) echo "checked"; ?> value="tracking-keywords"> <strong>Từ khóa theo chủ đề</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-keywords-add', $privilege)) echo "checked"; ?> value="tracking-keywords-add"> Thêm từ khóa
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-keywords-edit', $privilege)) echo "checked"; ?> value="tracking-keywords-edit"> Chỉnh sửa từ khóa
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-keywords;delete', $privilege)) echo "checked"; ?> value="tracking-keywords;delete"> Xóa từ khóa
        </label>
      </div>
    </div>

    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('bot', $privilege)) echo "checked"; ?> value="bot">
          <strong>BOT</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('bot-add', $privilege)) echo "checked"; ?> value="bot-add">
          Thêm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('bot-edit', $privilege)) echo "checked"; ?> value="bot-edit"> Chỉnh sửa
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('bot;delete', $privilege)) echo "checked"; ?> value="bot;delete"> Xóa
        </label>
      </div>
    </div>

    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('parish', $privilege)) echo "checked"; ?> value="parish">
          <strong>Giáo xứ</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('parish-add', $privilege)) echo "checked"; ?> value="parish-add">
          Thêm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('parish-edit', $privilege)) echo "checked"; ?> value="parish-edit"> Chỉnh sửa
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('parish;delete', $privilege)) echo "checked"; ?> value="parish;delete"> Xóa
        </label>
      </div>
    </div>

    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('industrial-zone', $privilege)) echo "checked"; ?> value="industrial-zone">
          <strong>Khu công nghiệp</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('industrial-zone-add', $privilege)) echo "checked"; ?> value="industrial-zone-add">
          Thêm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('industrial-zone-edit', $privilege)) echo "checked"; ?> value="industrial-zone-edit"> Chỉnh sửa
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('industrial-zone;delete', $privilege)) echo "checked"; ?> value="industrial-zone;delete"> Xóa
        </label>
      </div>
    </div>

    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('enterprise', $privilege)) echo "checked"; ?> value="enterprise">
          <strong>Doanh nghiệp</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('enterprise-add', $privilege)) echo "checked"; ?> value="enterprise-add">
          Thêm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('enterprise-edit', $privilege)) echo "checked"; ?> value="enterprise-edit"> Chỉnh sửa
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('enterprise;delete', $privilege)) echo "checked"; ?> value="enterprise;delete"> Xóa
        </label>
      </div>
    </div>

    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('hotspot', $privilege)) echo "checked"; ?> value="hotspot">
          <strong>Điểm nóng</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('hotspot-add', $privilege)) echo "checked"; ?> value="hotspot-add">
          Thêm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('hotspot-edit', $privilege)) echo "checked"; ?> value="hotspot-edit"> Chỉnh sửa
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('hotspot;delete', $privilege)) echo "checked"; ?> value="hotspot;delete"> Xóa
        </label>
      </div>
    </div>

    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('sea-zone', $privilege)) echo "checked"; ?> value="sea-zone">
          <strong>Vùng biển</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('sea-zone-add', $privilege)) echo "checked"; ?> value="sea-zone-add">
          Thêm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('sea-zone-edit', $privilege)) echo "checked"; ?> value="sea-zone-edit"> Chỉnh sửa
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('sea-zone;delete', $privilege)) echo "checked"; ?> value="sea-zone;delete"> Xóa
        </label>
      </div>
    </div>

    <div class="col-xs-12" style="margin-top: 25px;">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('location-records', $privilege)) echo "checked"; ?> value="location-records"> <strong>HỒ SƠ HỆ THỐNG</strong>
        </label>
      </div>
      <div class="row">

        <div class="col-left-ol">
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('records-protect', $privilege)) echo "checked"; ?> value="records-protect"> <strong>Bảo vệ</strong>
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('records-protect-add', $privilege)) echo "checked"; ?> value="records-protect-add"> Thêm bảo vệ
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('records-protect-edit', $privilege)) echo "checked"; ?> value="records-protect-edit"> Chỉnh sửa bảo vệ
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('records-protect;delete', $privilege)) echo "checked"; ?> value="records-protect;delete"> Xóa bảo vệ
            </label>
          </div>
        </div>
      </div>
    </div>

    <!-- Trinh sat Facebook  -->
    <div class="col-xs-12" style="margin-top: 25px;">
      <!-- Danh sach facebook theo doi -->
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('fb-tracking', $privilege)) echo "checked"; ?> value="fb-tracking"> <strong>ĐỐI TƯỢNG</strong>
        </label>
      </div>
      <div class="row">
        <div class="col-left-ol">
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('fb-target', $privilege)) echo "checked"; ?> value="fb-target"> <strong>Đối tượng fb</strong>
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('fb-target-add', $privilege)) echo "checked"; ?> value="fb-target-add"> Thêm
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('fb-target-edit', $privilege)) echo "checked"; ?> value="fb-target-edit"> Chỉnh sửa
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('fb-target;delete', $privilege)) echo "checked"; ?> value="fb-target;delete"> Xóa
            </label>
          </div>
        </div>

        <!-- Danh sach nhom facebook theo doi -->
        <div class="col-left-ol">
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('facebook-target-group', $privilege)) echo "checked"; ?> value="facebook-target-group"> <strong>Nhóm đối tượng fb</strong>
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('facebook-target-group-add', $privilege)) echo "checked"; ?> value="facebook-target-group-add"> Thêm Nhóm đối tượng
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('facebook-target-group-edit', $privilege)) echo "checked"; ?> value="facebook-target-group-edit"> Chỉnh sửa Nhóm đối tượng
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('facebook-target-group;delete', $privilege)) echo "checked"; ?> value="facebook-target-group;delete"> Xóa Nhóm đối tượng
            </label>
          </div>
        </div>

        <div class="col-left-ol">
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('fb-post', $privilege)) echo "checked"; ?> value="fb-post"> <strong>Bài viết của đối tượng</strong>
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('fb-post-add', $privilege)) echo "checked"; ?> value="fb-post-add"> Thêm
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('fb-post-edit', $privilege)) echo "checked"; ?> value="fb-post-edit"> Chỉnh sửa
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('fb-post;delete', $privilege)) echo "checked"; ?> value="fb-post;delete"> Xóa
            </label>
          </div>
        </div>
      </div>
    </div>

    <!-- Trinh sat tau thuyen  -->
    <div class="col-xs-12" style="margin-top: 25px;">
      <!-- Danh sach tau thuyen theo doi -->
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('tracking-ship', $privilege)) echo "checked"; ?> value="tracking-ship"> <strong>TRINH SÁT TÀU THUYỀN</strong>
        </label>
      </div>
      <div class="row">
        <!-- Danh sach tau -->
        <div class="col-left-ol">
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship', $privilege)) echo "checked"; ?> value="ship"> <strong>Đối tượng tàu</strong>
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship-add', $privilege)) echo "checked"; ?> value="ship-add"> Thêm
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship-edit', $privilege)) echo "checked"; ?> value="ship-edit"> Chỉnh sửa
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship;delete', $privilege)) echo "checked"; ?> value="ship;delete"> Xóa
            </label>
          </div>
        </div>

        <!-- Du lieu tau crawl ve -->
        <div class="col-left-ol">
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship-report', $privilege)) echo "checked"; ?> value="ship-report"> <strong>Dữ liệu tàu</strong>
            </label>
          </div>
        </div>

        <!-- Danh sach cac loai tau -->
        <div class="col-left-ol">
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship-type', $privilege)) echo "checked"; ?> value="ship-type"> <strong>Loại tàu</strong>
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship-type-add', $privilege)) echo "checked"; ?> value="ship-type-add"> Thêm Loại tàu
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship-type-edit', $privilege)) echo "checked"; ?> value="ship-type-edit"> Chỉnh sửa Loại tàu
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship-type;delete', $privilege)) echo "checked"; ?> value="ship-type;delete"> Xóa Loại tàu
            </label>
          </div>
        </div>

        <!-- Danh sach cac vu khi tau -->
        <div class="col-left-ol">
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship-weapon', $privilege)) echo "checked"; ?> value="ship-weapon"> <strong>Vũ khí tàu</strong>
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship-weapon-add', $privilege)) echo "checked"; ?> value="ship-weapon-add"> Thêm
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship-weapon-edit', $privilege)) echo "checked"; ?> value="ship-weapon-edit"> Chỉnh sửa
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship-weapon;delete', $privilege)) echo "checked"; ?> value="ship-weapon;delete"> Xóa
            </label>
          </div>
        </div>

        <!-- Hai trinh tau -->
        <div class="col-left-ol">
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship-voyage', $privilege)) echo "checked"; ?> value="ship-voyage"> <strong>Hải trình tàu</strong>
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship-voyage-add', $privilege)) echo "checked"; ?> value="ship-voyage-add"> Thêm
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship-voyage-edit', $privilege)) echo "checked"; ?> value="ship-voyage-edit"> Chỉnh sửa
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" class="checkbox-tracking" name="variable[]" <?php if (in_array('ship-voyage;delete', $privilege)) echo "checked"; ?> value="ship-voyage;delete"> Xóa
            </label>
          </div>
        </div>



      </div>
    </div>
  </div>

  <!-- Xac nhan -->
  <div class="col-xs-12" style="margin-top: 25px;">
    <label>
      <input type="checkbox" class="checked-all" rel="checkbox-tracking" data-toggle="tooltip" data-placement="top" title="Chọn tất cả">
      <button type="submit" class="btn btn-primary btn-round">Xác nhận</button>
    </label>
  </div>
<?php
}

function showCoreFolder($role_id)
{
  global $db;
  $dir_dest = ROOT_DIR . DS . 'uploads' . DS . 'folder' . DS;

  $dirs = array_filter(glob($dir_dest . '*'), 'is_dir');
  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = $role_id and `type` = 'folder'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select("`privilege_slug`");
  foreach ($rows as $row) {
    $privilege[] = $row['privilege_slug'];
  }

?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-folder" name="variable[]" <?php if (in_array('folder-write', $privilege)) echo "checked"; ?> value="folder-write"> <strong>Cập nhật File</strong>
        </label>
      </div>
      <?php
      foreach ($dirs as $value) {
        $checked = '';
        $value = str_replace($dir_dest, '', $value);
        if (in_array($value, $privilege)) $checked = ' checked';
        echo '<div class="checkbox"><label><input type="checkbox" class="checkbox-folder" name="variable[]"' . $checked . ' value="' . $value . '"> ' . $value . '</label></div>';
      }
      ?>
    </div>
  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-folder" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}


function showCoreBlog($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = $role_id and `type` = 'blog'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select("`privilege_slug`");
  foreach ($rows as $row) {
    $privilege[] = $row['privilege_slug'];
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-blog" name="variable[]" <?php if (in_array('blog-post', $privilege)) echo "checked"; ?> value="blog-post"> <strong>Trang tin bài viết</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-blog" name="variable[]" <?php if (in_array('blog-post-add', $privilege)) echo "checked"; ?> value="blog-post-add"> Thêm bài viết
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-blog" name="variable[]" <?php if (in_array('blog-post-edit', $privilege)) echo "checked"; ?> value="blog-post-edit"> Chỉnh sửa bài viết
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-blog" name="variable[]" <?php if (in_array('blog-post;delete', $privilege)) echo "checked"; ?> value="blog-post;delete"> Xóa bài viết
        </label>
      </div>
    </div>
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-blog" name="variable[]" <?php if (in_array('blog-category', $privilege)) echo "checked"; ?> value="blog-category"> <strong>Chuyên mục bài viết</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-blog" name="variable[]" <?php if (in_array('blog-category-add', $privilege)) echo "checked"; ?> value="blog-category-add"> Thêm chuyên mục
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-blog" name="variable[]" <?php if (in_array('blog-category-edit', $privilege)) echo "checked"; ?> value="blog-category-edit"> Chỉnh sửa chuyên mục
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-blog" name="variable[]" <?php if (in_array('blog-category;delete', $privilege)) echo "checked"; ?> value="blog-category;delete"> Xóa chuyên mục
        </label>
      </div>
    </div>
  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-blog" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}

function showCoreConfig($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = " . $role_id . " and `type` = 'config'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select();
  $stt = 0;
  foreach ($rows as $row) {
    $privilege[$stt] = $row['privilege_slug'];
    $stt++;
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-config" name="variable[]" <?php if (in_array('config-general', $privilege)) echo "checked"; ?> value="config-general"> <b>Thông tin chung</b>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-config" name="variable[]" <?php if (in_array('config-datetime', $privilege)) echo "checked"; ?> value="config-datetime"> <b>Thời gian</b>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-config" name="variable[]" <?php if (in_array('config-mail', $privilege)) echo "checked"; ?> value="config-mail"> <b>Thư điện tử</b>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-config" name="variable[]" <?php if (in_array('config-plugins', $privilege)) echo "checked"; ?> value="config-plugins"> <b>Trình cắm bổ sung</b>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-config" name="variable[]" <?php if (in_array('config-social-network', $privilege)) echo "checked"; ?> value="config-social-network"> <b>Mạng xã hội</b>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-config" name="variable[]" <?php if (in_array('config-upload', $privilege)) echo "checked"; ?> value="config-upload"> <b>Tệp tin tải lên</b>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-config" name="variable[]" <?php if (in_array('plugin-page', $privilege)) echo "checked"; ?> value="plugin-page"> <b>Phần bổ sung</b>
        </label>
      </div>
    </div>
  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-config" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}

function showCoreCore($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = " . $role_id . " and `type` = 'core'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select();
  $stt = 0;
  foreach ($rows as $row) {
    $privilege[$stt] = $row['privilege_slug'];
    $stt++;
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('core_user', $privilege)) echo "checked"; ?> value="core_user"> <b>Quản lý người dùng</b>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('core_user_add', $privilege)) echo "checked"; ?> value="core_user_add"> Thêm thành viên
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('core_user_edit', $privilege)) echo "checked"; ?> value="core_user_edit"> Chỉnh sửa thành viên
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('core_user;delete', $privilege)) echo "checked"; ?> value="core_user;delete"> Xóa thành viên
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('portal_update', $privilege)) echo "checked"; ?> value="portal_update"> Portal nhân viên
        </label>
      </div>
    </div>
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('core_role', $privilege)) echo "checked"; ?> value="core_role"> <b>Nhóm quản trị</b>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('core_role_add', $privilege)) echo "checked"; ?> value="core_role_add"> Thêm nhóm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('core_role_edit', $privilege)) echo "checked"; ?> value="core_role_edit"> Chỉnh sửa nhóm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('core_role;delete', $privilege)) echo "checked"; ?> value="core_role;delete"> Xóa nhóm
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('core_dashboard', $privilege)) echo "checked"; ?> value="core_dashboard"> Phân quyền quản trị
        </label>
      </div>
    </div>
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('core_agency', $privilege)) echo "checked"; ?> value="core_agency"> <b>Quản lý đơn vị</b>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('core_agency_add', $privilege)) echo "checked"; ?> value="core_agency_add"> Thêm đơn vị
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('core_agency_edit', $privilege)) echo "checked"; ?> value="core_agency_edit"> Chỉnh sửa đơn vị
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('core_agency;delete', $privilege)) echo "checked"; ?> value="core_agency;delete"> Xóa đơn vị
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('portal_store', $privilege)) echo "checked"; ?> value="portal_store"> Portal cửa hàng
        </label>
      </div>
    </div>
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-core" name="variable[]" <?php if (in_array('core_mail', $privilege)) echo "checked"; ?> value="core_mail"> <b>Nhật ký gửi Mail</b>
        </label>
      </div>
    </div>
  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-core" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}

function showCoreBackup($role_id)
{
  global $db;

  $privilege = array();
  $db->table = "core_privilege";
  $db->condition = "`role_id` = " . $role_id . " and `type` = 'backup'";
  $db->order = "";
  $db->limit = "";
  $rows = $db->select();
  $stt = 0;
  foreach ($rows as $row) {
    $privilege[$stt] = $row['privilege_slug'];
    $stt++;
  }
?>
  <input type="hidden" name="role_id" value="<?php echo $role_id ?>" />
  <div class="form-group clearfix">
    <div class="col-left-ol">
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-backup" name="variable[]" <?php if (in_array('backup-process', $privilege)) echo "checked"; ?> value="backup-process"> <b>Tiến trình sao lưu</b>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" class="checkbox-backup" name="variable[]" <?php if (in_array('backup-config', $privilege)) echo "checked"; ?> value="backup-config"> <b>Cấu hình sao lưu</b>
        </label>
      </div>
    </div>
  </div>
  <label><input type="checkbox" class="checked-all" rel="checkbox-backup" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"> <button type="submit" class="btn btn-primary btn-round">Xác nhận</button></label>
<?php
}
