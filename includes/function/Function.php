<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
//Function __autoload()
function __autoload($classname) {
	if (file_exists(_F_CLASSES . DS . $classname . ".class.php")) {
		include(_F_CLASSES . DS . $classname . ".class.php");
	}
	else if (file_exists(_F_CLASSES . DS . $classname . ".php")) {
		include(_F_CLASSES . DS . $classname . ".php");
	}
	else if (file_exists(_F_CLASSES . DS . "class." . $classname . ".php")) {
		include(_F_CLASSES . DS . "class." . $classname . ".php");
	}
	else if (file_exists(_F_CLASSES . DS . str_replace('_', DS, $classname) . ".php")) {
		include(_F_CLASSES . DS . str_replace('_', DS, $classname) . ".php");
	}
    else {
    }
}

//----------------------------------------------------------------------------------------------------------------------
/* Set Reporting Error */
function setReporting() {
	if (DEVELOPMENT_ENVIRONMENT == true) {
		error_reporting(E_ALL);
		ini_set('display_errors', 'On');
	}
	else {
		error_reporting(E_ALL);
		ini_set('display_errors', 'Off');
		ini_set('log_errors', 'On');
        ini_set('error_log', ERROR_LOG_DIR);
    }
}
setReporting();

//----------------------------------------------------------------------------------------------------------------------
// Get site_url()
function site_url(){
	$url = HOME_URL_LANG . $_SERVER['REQUEST_URI'];
	return $url;
}

/*
 * HTML minification
 */
function sanitize_output($buffer) {
    $search = array(
        '/\>[^\S ]+/s',
        '/[^\S ]+\</s',
        '/(\s)+/s'
    );
    $replace = array(
        '>',
        '<',
        '\\1'
    );
    $buffer = preg_replace($search, $replace, $buffer);
    return $buffer;
}

//----------------------------------------------------------------------------------------------------------------------
/** Hàm lấy giá trị bảng page
 * @param $alias
 * @param string $col
 * @return string
 */
function getPage($alias, $col = "content") {
	global $db;
	$alias = $db->clearText($alias);

	$content = '';
	$db->table = "page";
	$db->condition = "alias = '".$alias."'";
	$db->order = "";
	$db->limit = 1;
	$rows = $db->select();
	if($db->RowCount==0) {
		$content="Unknown alias '".$alias."'";
	} else {
		foreach($rows as $row){
			$content = ($row['is_active']+0==1)? $row[$col] : '' ;
		}
	}

	return stripslashes($content);
}

//----------------------------------------------------------------------------------------------------------------------
/** Hàm lấy giá trị bảng constant
 * @param $const
 * @return string
 */
function getConstant($const, $column = 'value') {
	global $db;
	$const = $db->clearText($const);

	$value = '';
	$db->table = "constant";
	$db->condition = "constant = '".$const."'";
	$db->order = "";
	$db->limit = 1;
	$rows = $db->select();
	foreach($rows as $row){
		$value = $row[$column];
	}

	return stripslashes($value);
}


//----------------------------------------------------------------------------------------------------------------------
/**
 * @param $str
 * @param $url
 */
function loadPageError($str, $url) {
	?>
	<div align="center">
		<div id="spinningSquaresG">
			<div id="spinningSquaresG_1" class="spinningSquaresG">
			</div>
			<div id="spinningSquaresG_2" class="spinningSquaresG">
			</div>
			<div id="spinningSquaresG_3" class="spinningSquaresG">
			</div>
			<div id="spinningSquaresG_4" class="spinningSquaresG">
			</div>
			<div id="spinningSquaresG_5" class="spinningSquaresG">
			</div>
			<div id="spinningSquaresG_6" class="spinningSquaresG">
			</div>
			<div id="spinningSquaresG_7" class="spinningSquaresG">
			</div>
			<div id="spinningSquaresG_8" class="spinningSquaresG">
			</div>
		</div>
		<span class="show-error"><?php echo $str?></span>
		<br>Vui lòng đợi giây lát hoặc bấm <a style="font-weight:  bold;" href="<?php echo $url?>">vào đây</a> để tiếp tục...
	</div>
	<head>
		<meta http-equiv="Refresh" content="5; URL=<?php echo $url?>">
	</head>
	<?php
	die();
}

/**
 * @param $str
 * @param $url
 */
function loadPageSuccess($str, $url) {
	?>
	<div align="center">
		<div id="spinningSquaresG">
			<div id="spinningSquaresG_1" class="spinningSquaresG">
			</div>
			<div id="spinningSquaresG_2" class="spinningSquaresG">
			</div>
			<div id="spinningSquaresG_3" class="spinningSquaresG">
			</div>
			<div id="spinningSquaresG_4" class="spinningSquaresG">
			</div>
			<div id="spinningSquaresG_5" class="spinningSquaresG">
			</div>
			<div id="spinningSquaresG_6" class="spinningSquaresG">
			</div>
			<div id="spinningSquaresG_7" class="spinningSquaresG">
			</div>
			<div id="spinningSquaresG_8" class="spinningSquaresG">
			</div>
		</div>
		<span class="show-ok"><?php echo $str?></span>
		<br>Vui lòng đợi giây lát hoặc bấm <a style="font-weight:  bold;" href="<?php echo $url?>">vào đây</a> để tiếp tục...
	</div>
	<head>
		<meta http-equiv="Refresh" content="1; URL=<?php echo $url?>">
	</head>
	<?php
	die();
}

//----------------------------------------------------------------------------------------------------------------------
/**
 * @param $num
 * @return string
 */
function formatNumberVN($num) {
	return number_format(($num+0),0,""," ");
}
/**
 * @param $text
 * @return mixed
 */
function formatNumberToInt($text) {
	$text = str_replace(" ", "", $text);
	return $text+0;
}

function formatNumberToFloat($text) {
	$text = str_replace(" ", "", $text);
	$text = str_replace(",", ".", $text);
	return $text+0;
}

//----------------------------------------------------------------------------------------------------------------------
function getCurrentDir($path=".") {
	$dirarr = array();
	if ($dir = opendir($path)) {
		while (false !== ($entry = @readdir($dir))) {
			if (($entry!=".")&&($entry!="..")) {
				if ($path!=".") $newpath = $entry;
				else $newpath = $entry;
				$newpath = str_replace("//","/",$newpath);
				$dirarr[] = $newpath;
			}
		}
	}
	return $dirarr;
}// end func

//----------------------------------------------------------------------------------------------------------------------
function size_format($bytes="") {
	$retval = "";
	if ($bytes >=  pow(1024,5)) {
		$retval = round($bytes / pow(1024,5) * 100 ) / 100 . " PB";
	} else if ($bytes >=  pow(1024,4)) {
		$retval = round($bytes / pow(1024,4) * 100 ) / 100 . " TB";
	} else if ($bytes >=  pow(1024,3)) {
		$retval = round($bytes / pow(1024,3) * 100 ) / 100 . " GB";
	} else if ($bytes >=  pow(1024,2)) {
		$retval = round($bytes / pow(1024,2) * 100 ) / 100 . " MB";
	} else if ($bytes  >= 1024) {
		$retval = round($bytes / 1024 * 100 ) / 100 . " KB";
	} else {
		$retval = $bytes . " bytes";
	}
	return $retval;
}

function convert_to_bytes( $string ) {
	if(preg_match( '/^([0-9\.]+)[ ]*([b|k|m|g|t|p|e|z|y]*)/i', $string, $matches)) {
		if( empty( $matches[2] ) ) return $matches[1];
		$suffixes = array(
			'B' => 0,
			'K' => 1,
			'M' => 2,
			'G' => 3,
			'T' => 4,
			'P' => 5,
			'E' => 6,
			'Z' => 7,
			'Y' => 8
		);
		if(isset($suffixes[strtoupper( $matches[2])])) return round($matches[1] * pow(1024, $suffixes[strtoupper( $matches[2])]));
	}
	return false;
}

function convert_from_bytes($size){
	if($size <= 0) return '0 bytes';
	if($size == 1) return '1 byte';
	if($size < 1024) return $size . ' bytes';
	$i = 0;
	$iec = array( 'bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB' );
	while(($size / 1024) >= 1){
		$size = $size / 1024;
		++$i;
	}
	return number_format( $size, 2 ) . ' ' . $iec[$i];
}

//----------------------------------------------------------------------------------------------------------------------
/**
 * @param $id
 * @return string
 */
function getUserName($id){
	global $db;
	$str = "";
	$db->table = "core_user";
	$db->condition = "`user_id` = ". intval($id);
	$db->order = "`user_id` ASC";
	$db->limit = "";
	$rows = $db->select();
	foreach ($rows as $row){
		$str = $row['user_name'];
	}
	return stripslashes($str);
}

function getUserFullName($id){
	global $db;
	$str = "";
	$db->table = "core_user";
	$db->condition = "user_id = ".($id+0);
	$db->order = "user_id ASC";
	$db->limit = "";
	$rows = $db->select();
	foreach ($rows as $row){
		$str = $row['full_name'];
	}
	return stripslashes($str);
}

function getRoleName($id){
	global $db;
	$str = "";
	$db->table = "core_role";
	$db->condition = "`role_id` = " . intval($id);
	$db->order = "";
	$db->limit = "";
	$rows = $db->select();
	foreach ($rows as $row){
		$str = $row['name'];
	}
	return stripslashes($str);
}

/**
 * @param $name
 * @param $sum
 * @param $idno
 * @param $id
 * @param $type
 */
function showSortUser($name, $sum, $select, $id, $type, $core=1) {
    $result = '';
	if($core==1) {
        $result .= '<select class="form-control select-manager" style="width: 80%;" onchange="performSortCore(' . $id . ', this.value, \'' . $type . '\')" name="' . $name . '">';
        for ($i = 1; $i <= $sum; $i++) {
            $selected = '';
            if ($select == $i) $selected = ' selected';
            $result .= '<option value="' . $i . '"' . $selected . '>' . $i . '</option>';
        }
        $result .= '</select>';

	} else {
        $result .= '<select class="form-control select-manager ol-alert-core" style="width: 80%;" name="' . $name . '">';
        for ($i = 1; $i <= $sum; $i++) {
            $selected = '';
            if ($select == $i) $selected = ' selected';
            $result .= '<option value="' . $i . '"' . $selected . '>' . $i . '</option>';
        }
        $result .= '</select>';
	}
	return $result;
}


//----------------------------------------------------------------------------------------------------------------------
/**
 * @param $name
 * @param $sum
 * @param $idno
 * @param $width
 * @param int $style
 * @param $id
 * @param $type
 */
function showSort($name, $sum, $idno, $width, $style=1, $id, $type, $core=1) {
    $result = '';
    $e_style  = 'width: ' . $width;
    if($style==1)$e_style .= ' font-weight:bold; color: #1d92af;';
    if($core==1) {
        $result .= '<select class="form-control select-manager" style="' . $e_style . '" onchange="performSort(' . $id . ', this.value, \'' . $type . '\')" name="' . $name . '">';
        for ($i = 1; $i <= $sum; $i++) {
            $selected = '';
            if ($idno == $i) $selected = ' selected';
            $result .= '<option value="' . $i . '"' . $selected . '>' . $i . '</option>';
        }
        $result .= '</select>';

    } else {
        $result .= '<select class="form-control select-manager" disabled style="' . $e_style . '" name="' . $name . '">';
        $result .= '<option value="' . $idno . '" selected>' . $idno . '</option>';
        $result .= '</select>';
    }
    return $result;
}

//----------------------------------------------------------------------------------------------------------------------
/**
 * @return string
 */
function getTitle() {
	global $db, $slug_cat, $id_menu, $id_article;
	$slug = $slug_cat;
	$txtTitle = getConstant('title');
	$txt = "";

	if ($slug == "home") {
		$txt = $txtTitle;
	}

	if (!empty($slug)) {
		$tb = array(
			1 => 'article',
			2 => 'gallery',
			6 => 'product',
			9 => 'tour',
			10 => 'gift',
			11 => 'car',
			17 => 'project',
			18 => 'product',
		);
		$type_id = 0;

		$db->table = "category";
		$db->condition = "slug = '".$slug."'";
		$db->order = "";
		$db->limit = 1;
		$rows = $db->select();
		if($db->RowCount>0) {
			foreach($rows as $row) {
				$txt = (!empty($row['title']))? $row['title'] : $row['name'];
				$type_id = $row['type_id']+0;
			}
			$tb_name = $tb[$type_id];

			if (!empty($id_menu) && $id_menu != 0) {
				$db->table = $tb_name . "_menu";
				$db->condition = $tb_name . "_menu_id = ".$id_menu;
				$db->order = "";
				$db->limit = 1;
				$rows = $db->select();
				foreach($rows as $row) {
					$txt = (!empty($row['title']))? $row['title'] : $row['name'];
				}
			}
			if (!empty($id_article) && $id_article != 0) {
				$article_id = $id_article;
				$db->table = $tb_name;
				$db->condition = $tb_name . "_id = ".$article_id;
				$db->order = "";
				$db->limit = 1;
				$rows = $db->select();
				foreach($rows as $row) {
					if($slug=='du-an-dau-tu' || $slug=='gioi-thieu-du-an') $txt = (!empty($row['title']))? $row['title'] : $row['name'] . ' - ' . $txt;
					else $txt = (!empty($row['title']))? $row['title'] : $row['name'];
				}
			}
		}
	}

	if ($slug == "error-404") {
		$txt = "Error pages 404! - ".$txtTitle;
	}
	if ($slug == "lien-he") {
		$txt = "Liên hệ - ".$txtTitle;
	}
	if ($slug == "contact") {
		$txt = "Contact - ".$txtTitle;
	}
	if ($slug == "gio-hang") {
		$txt = "Giỏ hàng - ".$txtTitle;
	}
	if ($slug == "tim-kiem") {
		$txt = "Tìm kiếm - ".$txtTitle;
	}

	return stripslashes($txt);
}

//----------------------------------------------------------------------------------------------------------------------
/**
 * @return string
 */
function getDescription() {
	global $db, $slug_cat, $id_menu, $id_article;
	$stringObj = new String();
	$slug = $slug_cat;
	$txtDescription = getConstant('description');
	$txt = "";

	if ($slug == "home") {
		$txt = $txtDescription;
	}

	if (!empty($slug)) {
		$tb = array(
			1 => 'article',
			2 => 'gallery',
			6 => 'product',
			9 => 'tour',
			10 => 'gift',
			11 => 'car',
			17 => 'project',
			18 => 'product',
		);
		$type_id = 0;

		$db->table = "category";
		$db->condition = "slug = '".$slug."'";
		$db->order = "";
		$db->limit = 1;
		$rows = $db->select();
		if($db->RowCount>0) {
			foreach($rows as $row) {
				$txt = (!empty($row['description']))? $row['description'] : $row['name'];
				$type_id = $row['type_id']+0;
			}
			$tb_name = $tb[$type_id];

			if (!empty($id_menu) && $id_menu != 0) {
				$db->table = $tb_name . "_menu";
				$db->condition = $tb_name . "_menu_id = ".$id_menu;
				$db->order = "";
				$db->limit = 1;
				$rows = $db->select();
				foreach($rows as $row) {
					$comment = (!empty($row['comment'])) ? $row['comment'] : $row['name'];
					$txt = (!empty($row['description']))? $row['description'] : $comment;
				}
			}
			if (!empty($id_article) && $id_article != 0) {
				$article_id = $id_article;
				$db->table = $tb_name;
				$db->condition = $tb_name . "_id = ".$article_id;
				$db->order = "";
				$db->limit = 1;
				$rows = $db->select();
				foreach($rows as $row) {
					$comment = '';
					if($slug=='nha-dat-don-le' || $slug=='san-giao-dich') {
						$comment = (!empty($row['parallel_price'])) ? $row['parallel_price'] : $row['name'];
					} else {
						$comment = (!empty($row['comment'])) ? $row['comment'] : $row['name'];
					}
					
					if($slug=='du-an-dau-tu' || $slug=='gioi-thieu-du-an') $txt = (!empty($row['description'])) ? $row['description'] : $txt;
					else $txt = (!empty($row['description']))? $row['description'] : $comment;
				}
			}
		}
	}

	if ($slug == "error-404") {
		$txt = "Error pages 404!, ".$txtDescription;
	}
	if ($slug == "lien-he") {
		$txt = "Liên hệ, ".$txtDescription;
	}
	if ($slug == "contact") {
		$txt = "Contact, ".$txtDescription;
	}
	if ($slug == "gio-hang") {
		$txt = "Giỏ hàng, ".$txtDescription;
	}
	if ($slug == "tim-kiem") {
		$txt = "Tìm kiếm, ".$txtDescription;
	}

	return stripslashes($stringObj->crop($txt, 60));
}

//----------------------------------------------------------------------------------------------------------------------
/**
 * @return string
 */
function getKeywords() {
	global $db, $slug_cat, $id_menu, $id_article;
	$slug = $slug_cat;
	$txtKeywords = getConstant('keywords');
	$txt = "";

	if ($slug == "home") {
		$txt = $txtKeywords;
	}

	if (!empty($slug)) {
		$tb = array(
			1 => 'article',
			2 => 'gallery',
			6 => 'product',
			9 => 'tour',
			10 => 'gift',
			11 => 'car',
			17 => 'project',
			18 => 'product',
		);
		$type_id = 0;

		$db->table = "category";
		$db->condition = "slug = '".$slug."'";
		$db->order = "";
		$db->limit = 1;
		$rows = $db->select();
		if($db->RowCount>0) {
			foreach($rows as $row) {
				$txt = (!empty($row['keywords']))? $row['keywords'] : $row['name'];
				$type_id = $row['type_id']+0;
			}
			$tb_name = $tb[$type_id];

			if (!empty($id_menu) && $id_menu != 0) {
				$db->table = $tb_name . "_menu";
				$db->condition = $tb_name . "_menu_id = ".$id_menu;
				$db->order = "";
				$db->limit = 1;
				$rows = $db->select();
				foreach($rows as $row) {
					$txt = (!empty($row['keywords']))? $row['keywords'] : $row['name'];
				}
			}
			if (!empty($id_article) && $id_article != 0) {
				$article_id = $id_article;
				$db->table = $tb_name;
				$db->condition = $tb_name . "_id = ".$article_id;
				$db->order = "";
				$db->limit = 1;
				$rows = $db->select();
				foreach($rows as $row) {
					if($slug=='du-an-dau-tu' || $slug=='gioi-thieu-du-an') $txt = (!empty($row['keywords']))? $row['keywords'] : $row['name'] . ', ' . $txt;
					else $txt = (!empty($row['keywords']))? $row['keywords'] : $row['name'];
				}
			}
		}
	}

	if ($slug == "error-404") {
		$txt = "Error pages 404!, ".$txtKeywords;
	}
	if ($slug == "lien-he") {
		$txt = 'Liên hệ, '.$txtKeywords;
	}
	if ($slug == "contact") {
		$txt = "Contact, ".$txtKeywords;
	}
	if ($slug == "gio-hang") {
		$txt = "Giỏ hàng, ".$txtKeywords;
	}
	if ($slug == "tim-kiem") {
		$txt = "Tìm kiếm, ".$txtKeywords;
	}

	return stripslashes($txt);
}

//----------------------------------------------------------------------------------------------------------------------
/**
 * @param int $length
 * @return string
 */
function getRandomString($length = 15) {
	$validCharacters = "abcdefghijklmnopqrstuxyvwz0123456789";
	$validCharNumber = strlen($validCharacters);

	$result = "";

	for ($i = 0; $i < $length; $i++) {
		$index = mt_rand(0, $validCharNumber - 1);
		$result .= $validCharacters[$index];
	}

	return $result;
}

//----------------------------------------------------------------------------------------------------------------------
/**
 * @param $emailReply
 * @param $nameReply
 * @param $emailTo
 * @param $nameTo
 * @param $subject
 * @param $content
 * @param string $file
 * @return bool
 */
function sendMailFn($emailTo, $nameTo, $subject, $content, $file = '') {
    global $db;
    $content    =	sanitize_output($content);
    $content    =	str_replace("\n"	, "<br>"	, $content);
	$content    =	str_replace("  "	, "&nbsp; "	, $content);
	$content    =	str_replace("<script>","&lt;script&gt;", $content);
    $log_type   = 'mail';
    $log_to     = array();
    $log_status = 0;
    $result     = FALSE;

    $mail = new PHPMailer();
    $mail->IsSMTP();

    $mail->Host = getConstant("SMTP_host");
    $mail->Port = getConstant("SMTP_port");
    $mail->SMTPDebug = 0;
    // 1 = errors and messages
    // 2 = messages only
    $mail->SMTPAuth = true;
    if(getConstant("SMTP_secure") != 'none')
        $mail->SMTPSecure = getConstant("SMTP_secure");

    //--- Chọn Accout
    $email_u = $email_p = array();
    $db->table = "constant";
    $db->condition = "`type` = 1 AND `constant` LIKE '%SMTP_username%' AND `value` != ''";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {
        $email_u[] = stripslashes($row['value']);
    }
    $db->table = "constant";
    $db->condition = "`type` = 1 AND `constant` LIKE '%SMTP_password%' AND `value` != ''";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {
        $email_p[] = stripslashes($row['value']);
    }
    $k = array_rand($email_u);
    if(isset($email_u[$k]) && isset($email_p[$k])) {
        $mail->Username = $email_u[$k];
        $mail->Password = $email_p[$k];
    }

    $mail->SetFrom($mail->Username,getConstant("SMTP_mailname"));

	$mail->AddAddress($emailTo, $nameTo);
    array_push($log_to, $nameTo . " &lt;$emailTo&gt;");

	$mail->Subject = $subject;
	$mail->CharSet = "utf-8";
	$body = $content;
	$mail->Body = $body;
    if($file!='')
        $mail->AddAttachment($file);
	$mail->IsHTML(true);

    if(!$mail->Send()) {
        $result = FALSE;
        $log_status = 0;
    } else {
        $result = TRUE;
        $log_status = 1;
    }

    if(count($log_to)>0) {
        $db->table = "log";
        $data = array(
            'type'          => $db->clearText($log_type),
            'list_to'       => $db->clearText(implode('; ', $log_to)),
            'subject'       => $db->clearText($subject),
            'content'       => $db->clearText($content),
            'status'        => intval($log_status),
            'modified_time' => time()
        );
        $db->insert($data);
    }
    return $result;
}

function sendMailSMTP(array $e_reply, array $e_to, $subject, $content, $file = '') {
    global $db;
    $content    = sanitize_output($content);
    $content    = str_replace("\n"	, "<br>"	, $content);
    $content    = str_replace("  "	, "&nbsp; "	, $content);
    $content    = str_replace("<script>","&lt;script&gt;", $content);
    $log_type   = 'mail';
    $log_to     = array();
    $log_status = 0;
    $result     = FALSE;

    $mail = new PHPMailer();
    $mail->IsSMTP();

    $mail->Host = getConstant("SMTP_host");
    $mail->Port = getConstant("SMTP_port");
    $mail->SMTPDebug = 0;
    // 1 = errors and messages
    // 2 = messages only
    $mail->SMTPAuth = true;
    if(getConstant("SMTP_secure") != 'none')
        $mail->SMTPSecure = getConstant("SMTP_secure");

    //--- Chọn Accout
    $email_u = $email_p = array();
    $db->table = "constant";
    $db->condition = "`type` = 1 AND `constant` LIKE '%SMTP_username%' AND `value` != ''";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {
        $email_u[] = stripslashes($row['value']);
    }
    $db->table = "constant";
    $db->condition = "`type` = 1 AND `constant` LIKE '%SMTP_password%' AND `value` != ''";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {
        $email_p[] = stripslashes($row['value']);
    }
    $k = array_rand($email_u);
    if(isset($email_u[$k]) && isset($email_p[$k])) {
        $mail->Username = $email_u[$k];
        $mail->Password = $email_p[$k];
    }

    $mail->SetFrom($mail->Username, getConstant("SMTP_mailname"));

    if(count($e_to)>0) {
        foreach($e_to as $key => $value) {
            $mail->AddAddress($key, $value);
            array_push($log_to, $value . " &lt;$key&gt;");
        }
    }
    if(count($e_reply)>0) {
        foreach($e_reply as $key => $value) {
            $mail->AddReplyTo($key, $value);
        }
    }

    $mail->Subject = $subject;
    $mail->CharSet = "utf-8";
    $body = $content;
    $mail->Body = $body;
    if($file!='')
        $mail->AddAttachment($file);
    $mail->IsHTML(true);

    if(!$mail->Send()) {
        $result = FALSE;
        $log_status = 0;
    } else {
        $result = TRUE;
        $log_status = 1;
    }

    if(count($log_to)>0) {
        $db->table = "log";
        $data = array(
            'type'          => $db->clearText($log_type),
            'list_to'       => $db->clearText(implode('; ', $log_to)),
            'subject'       => $db->clearText($subject),
            'content'       => $db->clearText($content),
            'status'        => intval($log_status),
            'modified_time' => time()
        );
        $db->insert($data);
    }
    return $result;
}

//----------------------------------------------------------------------------------------------------------------------
/**
 * @param $currentPage
 * @param $maxPage
 * @param string $path
 */
function showPageNavigation($currentPage, $maxPage, $path = '') {
	if ($maxPage <= 1) {
		return;
	}
	$suffix = '';

	$nav = array(
		'left'	=>	3,
		'right'	=>	3,
	);
	if ($maxPage < $currentPage) {
		$currentPage = $maxPage;
	}
	$max = $nav['left'] + $nav['right'];

	if ($max >= $maxPage) {
		$start = 1;
		$end = $maxPage;
	}
	elseif ($currentPage - $nav['left'] <= 0) {
		$start = 1;
		$end = $max + 1;
	}
	elseif (($right = $maxPage - ($currentPage + $nav['right'])) <= 0) {
		$start = $maxPage - $max;
		$end = $maxPage;
	}
	else {
		$start = $currentPage - $nav['left'];
		if ($start == 2) {
			$start = 1;
		}

		$end = $start + $max;
		if ($end == $maxPage - 1) {
			++$end;
		}
	}

	$navig = '<div class="page-navigation"><ul class="pagination pagination-sm">';
	if ($currentPage >= 2) {
		if ($currentPage >= $nav['left']) {
			if ($currentPage - $nav['left'] > 2 && $max < $maxPage) {
				$navig .= '<li class="paginate_button"><a href="'.$path.'1'.$suffix.'">1</a></li>';
				$navig .= '<li class="paginate_button"><a>...</a></li>';
			}
		}
		$navig .= '<li class="paginate_button"><a href="'.$path.($currentPage - 1).$suffix.'"><i class="fa fa-step-backward"></i></a></li>';
	}

	for ($i=$start;$i<=$end;$i++) {
		if ($i == $currentPage) {
			$navig .= '<li class="paginate_button active"><a>'.$i.'</a></li>';
		}
		else {
			$pg_link = $path.$i;
			$navig .= '<li class="paginate_button"><a href="'.$pg_link.$suffix.'">'.$i.'</a></li>';
		}
	}

	if ($currentPage <= $maxPage - 1) {
		$navig .= '<li class="paginate_button"><a href="'.$path.($currentPage + 1).$suffix.'"><i class="fa fa-step-forward"></i></a></li>';

		if ($currentPage + $nav['right'] < $maxPage - 1 && $max + 1 < $maxPage) {
			$navig .= '<li class="paginate_button"><a>...</a></li>';
			$navig .= '<li class="paginate_button"><a href="'.$path.$maxPage.$suffix.'">'.$maxPage.'</a></li>';
		}
	}
	$navig .= '</ul></div>';

	echo $navig;
}

//----------------------------------------------------------------------------------------------------------------------
/** Lấy thông tin User
 * @param int $id
 * @return array
 * @throws DatabaseConnException
 */
function getInfoUser($id = 0) {
	global $db;
	$info = array();
    $date = new DateClass();
	$db->table = "core_user";
	$db->condition = "user_id = " . intval($id);
	$db->order = "";
	$db->limit = 1;
	$rows = $db->select();
	foreach($rows as $row) {
		$info[0] = stripslashes($row['full_name']);
		$info[1] = stripslashes($row['apply']);
		$info[2] = stripslashes($row['phone']);
		$info[3] = stripslashes($row['email']);
		if($row['img']=='-no-' || $row['img']=='' ) {
			$info[4] = '/uploads/user/no-avatar-' .  $row['gender'] . '.png';
			$info[4] = '<img src="' . $info[4] . '" title="' . stripslashes($row['full_name']) . '">';
		} else {
			$info[4] = '/uploads/user/sm_' . $row['img'];
			$info[4] = '<img src="' . $info[4] . '" title="' . stripslashes($row['full_name']) . '">';
		}
		$info[5] = intval($row['gender']);
		$info[6] = stripslashes($row['user_name']);
		$info[7] = groupAgency($row['agency']);
		$info[8] = intval($row['user_id']);
		if($row['img']=='-no-' || $row['img']=='' ) {
			$info[9] = '/uploads/user/lg-no-avatar-' . $row['gender'] . '.png';
			$info[9] = '<img src="' . $info[9] . '" title="' . stripslashes($row['full_name']) . '" >';
		} else {
			$info[9] = '/uploads/user/' . $row['img'];
			$info[9] = '<img src="' . $info[9] . '" title="' . stripslashes($row['full_name']) . '" >';
		}
		$info[10] = stripslashes($row['address']);
		if($row['img']=='-no-' || $row['img']=='' ) {
			$info[11] = HOME_URL . '/images/logo.jpg';
		} else {
			$info[11] = HOME_URL . '/uploads/user/sm_' . $row['img'];
		}
        $info[12] = $date->vnDate(strtotime($row['birthday']));
	}
	return $info;
}

function getInfoUser2($id = 0) {
    global $db;
    $info = array();
    $db->table = "core_user";
    $db->condition = "user_id = " . intval($id);
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select();
    foreach($rows as $row) {
        $info[0] = '<a target="_blank" href="/?ol=user&op=portal&id=' . $id . '" data-toggle="tooltip" data-placement="top" title="Portal nhân viên">' . stripslashes($row['full_name']) . '</a>';
        $info[1] = stripslashes($row['apply']);
        $info[2] = stripslashes($row['phone']);
        $info[3] = stripslashes($row['email']);
        if($row['img']=='-no-' || $row['img']=='' ) {
            $img = '/uploads/user/no-avatar-' . $row['gender'] . '.png';
            $img = '<img src="' . $img . '" title="' . stripslashes($row['full_name']) . '">';
        } else {
            $img = '/uploads/user/sm_' . $row['img'];
            $img = '<img src="' . $img . '" title="' . stripslashes($row['full_name']) . '">';
        }
        $info[4] = '<a target="_blank" href="/?ol=user&op=portal&id=' . $id . '" data-toggle="tooltip" data-placement="top" title="Portal nhân viên">' . $img . '</a>';
        $info[5] = stripslashes($row['user_name']);
    }
    return $info;
}

function getInfoUser3($id = 0) {
    global $db;
    $info = array();
    $db->table = "core_user";
    $db->condition = "user_id = " . intval($id);
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select();
    foreach($rows as $row) {
        $info[0] = stripslashes($row['full_name']);
        $info[1] = stripslashes($row['apply']);
        $info[2] = stripslashes($row['phone']);
        $info[3] = stripslashes($row['email']);
        if($row['img']=='-no-' || $row['img']=='' ) {
            $img = '/uploads/user/no-avatar-' . $row['gender'] . '.png';
            $img = '<img src="' . $img . '" title="' . stripslashes($row['full_name']) . '">';
        } else {
            $img = '/uploads/user/sm_' . $row['img'];
            $img = '<img src="' . $img . '" title="' . stripslashes($row['full_name']) . '">';
        }
        $info[4] = $img;
    }
    return $info;
}

/** Lấy quyền quản trị
 * @return array
 */
function corePrivilegeSlug($id) {
	global $db;
	$role_id = array();
	$db->table = "role_user";
	$db->join= "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_role` b ON a.`role` = b.`role_id`";
	$db->condition = "b.`is_active` = 1 AND a.`user` = $id";
	$db->order = "";
	$db->limit = "";
	$rows = $db->select("a.`role`");
	foreach($rows as $row) {
		array_push($role_id, $row['role']);
	}
	if(count($role_id)>0) $role_id = implode(',', $role_id);
	else $role_id = 0;

	$corePrivilegeSlug = array();
	$db->table = "core_privilege";
	$db->condition = "`type` = 'ol' AND `role_id` IN ($role_id)";
	$db->order = "";
	$db->limit = "";
	$rows = $db->select();
	foreach ($rows as $row) {
		$corePrivilegeSlug['ol'][] = $row['privilege_slug'];
	}
	$corePrivilegeSlug['ol'][] = 'home';
    $corePrivilegeSlug['ol'][] = 'public';
    if(!empty($id)) {
        $corePrivilegeSlug['ol'][] = 'user';
        $corePrivilegeSlug['ol'][] = 'page';
    }

	$db->table = "core_privilege";
	$db->condition = "`type` != 'ol' and `role_id` IN ($role_id)";
	$db->order = "";
	$db->limit = "";
	$rows = $db->select();
	foreach ($rows as $row) {
		$corePrivilegeSlug['op'][] = $row['privilege_slug'];
	}
	$corePrivilegeSlug['op'][] = 'main';
	$corePrivilegeSlug['op'][] = 'post';

    if(!empty($id)) {
		$corePrivilegeSlug['op'][] = 'user-profile';
        $corePrivilegeSlug['op'][] = 'view';
        $corePrivilegeSlug['op'][] = 'notify';
		$corePrivilegeSlug['op'][] = 'search';

    }
	return $corePrivilegeSlug;
}

function coreWithLimit($user, $type) {
    global $db;
    $core_choice = array();
    $role = array();
    $db->table = "role_user";
    $db->condition = "`user` = $user";
    $db->order = "";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {
        array_push($role, intval($row['role']));
    }
    if(count($role)>0) {
        $role = implode(',', $role);
        $db->table = "core_privilege";
        $db->condition = "`type` LIKE '$type' AND `role_id` IN ($role)";
        $db->order = "";
        $db->limit = "";
        $rows = $db->select();
        foreach ($rows as $row) {
            $core_choice = array_merge($core_choice, json_decode($row['privilege_slug']));
        }
    }
    return $core_choice;
}

function coreBlogCat($user) {
    global $db;
    $core_choice = array();
    $role = array();
    $db->table = "role_user";
    $db->condition = "`user` = $user";
    $db->order = "";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {
        array_push($role, intval($row['role']));
    }
    if(count($role)>0) {
        $role = implode(',', $role);
        $db->table = "core_privilege";
        $db->condition = "`type` LIKE 'blog_cat' AND `role_id` IN ($role)";
        $db->order = "";
        $db->limit = "";
        $rows = $db->select();
        foreach ($rows as $row) {
            $core_choice = array_merge($core_choice, json_decode($row['privilege_slug']));
        }
    }
    return $core_choice;
}

//----------------------------------------------------------------------------------------------------------------------
function getOgImage($slug_cat = '', $id_menu = 0, $id_article = 0) {
	global $db;
	$tb_i = 0;
	$dir_dest = ROOT_DIR . DS . 'uploads' . DS;
	$image = HOME_URL . '/images/logo.jpg';
	$tb = array(
		1 => 'article',
		2 => 'gallery',
		6 => 'product',
		9 => 'tour',
		10 => 'gift',
		11 => 'car',
		17 => 'project',
		18 => 'product'
	);

	$db->table = "category";
	$db->condition = "slug = '".$slug_cat."'";
	$db->order = "";
	$db->limit = 1;
	$rows_cat = $db->select();
	if($db->RowCount>0) {
		foreach($rows_cat as $row_cat) {
			$tb_i = $row_cat['type_id']+0;
			if(($row_cat['img']!='-no-') && glob($dir_dest . 'category' . DS . '*' . $row_cat['img'])) {
				$image = HOME_URL . '/uploads/category/' . $row_cat['img'];
			}
		}

		$tb_name = $tb[$tb_i];

		$db->table = $tb_name . "_menu";
		$db->condition = $tb_name . "_menu_id = " . $id_menu;
		$db->order = "";
		$db->limit = 1;
		$rows_menu = $db->select();
		if($db->RowCount>0) {
			foreach($rows_menu as $row_menu) {
				if(($row_menu['img']!='-no-') && glob($dir_dest . $tb_name . '_menu' . DS . '*' . $row_menu['img'])) {
					$image = HOME_URL . '/uploads/' . $tb_name . '_menu/' . $row_menu['img'];
				}
			}

			$db->table = $tb_name;
			$db->condition = $tb_name . "_id = " . $id_article;
			$db->order = "";
			$db->limit = 1;
			$rows_art = $db->select();
			if($db->RowCount>0) {
				foreach($rows_art as $row_art) {
					if($tb_i == 6 || $tb_i == 17 || $tb_i == 18) {
						$img = array();
						$list_img = "";
						$db->table = "uploads_tmp";
						$db->condition = "upload_id = " . ($row_art['upload_id']+0);
						$db->order = "";
						$db->limit = 1;
						$rows_pr = $db->select();
						foreach ($rows_pr as $row_pr){
							$list_img = $row_pr['list_img'];
						}
						$img = explode(";", $list_img);
						if(!empty($img[0]) && glob($dir_dest .'photos' . DS . '*' . $img[0])) {
							$image = HOME_URL . '/uploads/photos/' . $img[0];
						}
					} else {
						if(($row_art['img']!='-no-') && glob($dir_dest . $tb_name . DS . '*' . $row_art['img'])) {
							$image = HOME_URL . '/uploads/' . $tb_name . '/' . $row_art['img'];
						}
					}
				}
			}
		}
	}
	return $image;
}

function showRatings($qty = 0) {
	$result = '';
	while($qty>0) {
	    $result .= '★ ';
	    $qty--;
    }
	return $result;
}

function showFileBackupData(array $currentdir, $dir_dest) {
	?>
	<table class="table table-manager table-striped table-bordered table-hover">
		<thead>
		<tr>
			<th>STT</th>
			<th>Tên file</th>
			<th>Dung lương</th>
			<th>Thời gian</th>
			<th>Chức năng</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$date = new DateClass();
		$string = array();
		$timefile = time();
		for ($i=0;$i<count($currentdir);$i++) {
			$entry = $currentdir[$i];
			if (!is_dir($entry)) {
				$name = $entry;
				$string = explode("_",$name);
				$filesize = @filesize($dir_dest."/".$name);
				$timefile = str_replace(".sql.gz","",$string[1]);
				$timefile = str_replace(".gz","",$timefile);
				$timefile = str_replace(".sql","",$timefile);
				?>
				<tr>
					<td align="center"><?php echo $i+1?></td>
					<td><?php echo $string[1]?></td>
					<td align="right"><?php echo size_format($filesize)?></td>
					<td align="center"><?php echo $date->vnFull($timefile)?></td>
					<td align="center">
						<a data-toggle="tooltip" data-placement="left" title="Tải xuống" target="_blank" href="<?php echo HOME_URL . '/cronjobs/' . $name;?>"><img src="/images/download.png"></a>&nbsp;&nbsp;
						<a data-toggle="tooltip" data-placement="right" title="Xóa file" class="ol-3w-confirm" style="cursor: pointer; margin-left: 7px;" href="javacript:;" rel="<?php echo $name;?>" ><img src="/images/remove.png"></a>
					</td>
				</tr>
			<?
			}
		} ?>
		</tbody>
	</table>
	<script>
		$(".ol-3w-confirm").click(function() {
            var file = $(this).attr("rel");
			confirm("File sao lưu cơ sở dữ liệu này sẽ được xóa và không thể phục hồi lại nó.\nBạn có muốn thực hiện không?", function() {
                if(this.data == true) window.location.href = '?tick=' + file;
			});
		});
	</script>
<?
}

//----------------------------------------------------------------------------------------------------------------------
/**
 * Hiển thị thay đổi thông tin cá nhân
 */
function showInformation($id = 0) {
    $img = '';
	?>
	<input type="hidden" name="url" value="change_profile">
	<input type="hidden" name="type" value="update_info">
	<table class="table table-no-border table-hover">
		<?php
		$date = new DateClass();
		global $db;
		$db->table = "core_user";
		$db->condition = "user_id = " . $id;
		$db->order = "";
		$rows = $db->select();
		foreach($rows as $row) {
			$gender = $row['gender'];
			$birthday = $date->vnDate(strtotime($row['birthday']));
			$img = $row['img'];
			?>
			<tr>
				<td width="150px" align="right"><label class="form-lb-tp">Tên đăng nhập:</label></td>
				<td><input class="form-control" type="text" name="user_name" id="user_name" readonly value="<?php echo stripslashes($row['user_name'])?>" ></td>
			</tr>
			<tr>
				<td align="right"><label class="form-lb-tp">Nhóm nhân viên:</label></td>
				<td><input class="form-control" type="text" name="role_id" id="role_id" readonly value="<?php echo groupAdmin($row['user_id'])?>" ></td>
			</tr>
			<tr>
				<td align="right"><label class="form-lb-tp">Họ và tên:</label></td>
				<td><input class="form-control" type="text" name="full_name" id="full_name" value="<?php echo stripslashes($row['full_name'])?>" maxlength="150" ></td>
			</tr>
			<tr>
				<td align="right"><label class="form-lb-tp">Giới tính:</label></td>
				<td>
					<select class="form-control" name="gender" id="gender" style="width: 120px;">
						<option value="0" <?php echo $gender==0?"selected":""?>>Khác...</option>
						<option value="1" <?php echo $gender==1?"selected":""?>>Nam</option>
						<option value="2" <?php echo $gender==2?"selected":""?>>Nữ</option>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right"><label class="form-lb-tp">Ngày sinh:</label></td>
				<td><input class="form-control input-datetime" type="text" name="birthday" style="width: 120px;" value="<?php echo $birthday?>"></td>
			</tr>
			<tr>
				<td align="right"><label class="form-lb-tp">Email:</label></td>
				<td><input class="form-control" type="email" name="email" id="email" value="<?php echo stripslashes($row['email'])?>" maxlength="200" ></td>
			</tr>
			<tr>
				<td align="right"><label class="form-lb-tp">Số điện thoại:</label></td>
				<td><input class="form-control" type="text" name="phone" id="phone" value="<?php echo $row['phone']?>" maxlength="20"></td>
			</tr>
			<tr>
				<td align="right"><label class="form-lb-tp">Địa chỉ:</label></td>
				<td><input class="form-control" type="text" name="address" id="address" value="<?php echo stripslashes($row['address'])?>" maxlength="255"></td>
			</tr>
			<tr>
				<td align="right"><label class="form-lb-tp">Thuộc đơn vị:</label></td>
				<td><input class="form-control" type="text" name="agency" id="agency" readonly value="<?php echo groupAgency($row['agency']);?>" maxlength="255"></td>
			</tr>
			<tr>
				<td align="right"><label class="form-lb-tp">Hình đại diện:</label></td>
				<td>
					<input type="hidden" name="img" value="<?php echo $img?>" />
					<input class="form-control file file-img" type="file" name="img" data-show-upload="false" data-max-file-count="1" accept="image/*">
				</td>
			</tr>
			<tr>
				<td align="right"><label class="form-lb-tp">Trạng thái:</label></td>
				<td>
					<b><?php echo $row['is_active']+0==0?"Đóng":"Mở"?></b>
				</td>
			</tr>
			<tr>
				<td align="right"><label class="form-lb-tp">Cập nhật gần nhất:</label></td>
				<td><?php echo $date->vnDateTime($row['modified_time'])?>&nbsp;&nbsp; - &nbsp;&nbsp;<b>Thực hiện:</b> <?php echo getUserName($row['user_id_edit']);?></td>
			</tr>
		<?
		}
		?>
		<tr>
			<td align="right"><label class="form-lb-tp">Mật khẩu hiện tại:</label></td>
			<td><input class="form-control" type="password" name="passwordold" id="passwordold" autocomplete="off" maxlength="16"></td>
		</tr>
		<tr>
			<td colspan="2" class="form-ol-btn-tzc">
				<button type="submit" name="update_info" class="btn btn-primary btn-round" id="btn_change_info">Đồng ý</button> &nbsp;
				<button type="reset" class="btn btn-warning btn-round">Làm lại</button> &nbsp;
				<button type="button" class="btn btn-danger btn-round" onclick="location.href='/'">Thoát</button>
			</td>
		</tr>
	</table>
	<script>
		window.onload=use_change_profile();
		$('.input-datetime').datetimepicker({
			mask:'39/19/9999',
			lang:'vi',
			timepicker: false,
			format:'<?php echo TTH_DATE_FORMAT?>'
		});
		$('.file-img').fileinput({
			<?php if($img!='-no-' && $img!='') { ?>
			initialPreview: [
				"<img src='../uploads/user/<?php echo $img?>' class='file-preview-image' alt='<?php echo $img?>'>"
			],
			<?php } ?>
			allowedFileExtensions : ['jpg', 'png','gif']
		});
	</script>
<?
}

function groupAdmin($id) {
	global $db;
	$str = $role_id = array();
	$db->table = "role_user";
	$db->condition = "`user` = $id";
	$db->limit = "";
	$db->order = "";
	$rows = $db->select();
	foreach ($rows as $row) {
		array_push($role_id, $row['role']+0);
	}
	if(count($role_id)>0) $role_id = implode(',', $role_id);
	else $role_id = 0;
	$db->table = "core_role";
	$db->condition = "role_id IN ($role_id)";
	$db->order = "";
	$rows = $db->select();
	foreach($rows as $row) {
		array_push($str, stripslashes($row["name"]));
	}
	return implode('; ', $str);
}

function groupAgency($list) {
    global $db;
    $result = array();
    $list =  json_decode($list);
    if(count($list)>0) {
        $list = implode(',', $list);
        $db->table = "agency";
        $db->condition = "`agency_id` IN ($list)";
        $db->order = "`sort` ASC";
        $db->limit = "";
        $rows = $db->select();
        foreach($rows as $row) {
            array_push($result, stripslashes($row['name']));
        }
    }

    return implode('; ', $result);
}

/**
 * Đổi mật khẩu cá nhân
 */
function showChangePassword (){
	?>
	<input type="hidden" name="url" value="change_profile">
	<input type="hidden" name="type" value="update_pass">
	<table class="table table-no-border table-hover">
		<tr>
			<td width="150px" align="right"><label class="form-lb-tp">Mật khẩu hiện tại:</label></td>
			<td><input class="form-control" type="password" name="password2old" id="password2old" autocomplete="off" maxlength="16"></td>
		</tr>
		<tr>
			<td align="right"><label class="form-lb-tp">Mật khẩu mới:</label></td>
			<td><input class="form-control" type="password" name="password" id="password" autocomplete="off" maxlength="16"></td>
		</tr>
		<tr>
			<td align="right"><label class="form-lb-tp">Nhập lại mật khẩu:</label></td>
			<td><input class="form-control" type="password" name="rePassword" id="rePassword" autocomplete="off" maxlength="16"></td>
		</tr>
		<tr>
			<td colspan="2" class="form-ol-btn-tzc">
				<button type="submit" name="update_pass" class="btn btn-primary btn-round" id="btn_change_pass">Đồng ý</button> &nbsp;
				<button type="reset" class="btn btn-warning btn-round">Làm lại</button> &nbsp;
				<button type="button" class="btn btn-danger btn-round" onclick="location.href='/'">Thoát</button>
			</td>
		</tr>
	</table>
	<script>
		window.onload=use_change_password();
	</script>
<?
}

function selectUserSingle($parent, $choice, $required = '') {
    global $db;
    $result = '';
    if($parent==0)  {
        $result .= '<select class="form-control selectpicker" name="user" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn nhân viên..." ' . $required . '>';

        $db->table = "core_user";
        $db->condition = "`is_active` = 1 AND `is_show` = 1 AND `agency` LIKE '[]'";
        $db->order = "`full_name` ASC";
        $db->limit = "";
        $rows = $db->select("`user_id`, `full_name`, `gender`, `img`");
        if($db->RowCount>0) {
            $result .= '<optgroup label="0 - ROOT">';
            foreach($rows as $row) {
                $avatar = '';
                if($row['img']=='-no-' || $row['img']=='' ) {
                    $avatar = '/uploads/user/no-avatar-' .  $row['gender'] . '.png';
                } else {
                    $avatar = '/uploads/user/sm_' . $row['img'];
                }
                $avatar = ' data-thumbnail="' . $avatar . '"';

                $selected = '';
                if ($row["user_id"]==$choice) $selected = ' selected';
                $result .= '<option' . $avatar . ' value="' . $row["user_id"] . '"' . $selected . '>' . stripslashes($row["full_name"]) . '</option>';
            }
            $result .= '</optgroup>';
        }
    }

    $db->table = "agency";
    $db->condition = "`is_active` = 1 AND `parent` = $parent";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows_ag = $db->select("`agency_id`, `symbol`, `name`");
    foreach ($rows_ag as $row_ag) {
        $code = '"' . intval($row_ag['agency_id']) . '"';
        $db->table = "core_user";
        $db->condition = "`is_active` = 1 AND `is_show` = 1 AND `agency` LIKE '%$code%'";
        $db->order = "`full_name` ASC";
        $db->limit = "";
        $rows = $db->select("`user_id`, `full_name`, `gender`, `img`");
        if($db->RowCount>0) {
            $result .= '<optgroup label="' . stripslashes($row_ag['symbol']) . ' - ' . stripslashes($row_ag['name']) . '">';
            foreach($rows as $row) {
                $avatar = '';
                if($row['img']=='-no-' || $row['img']=='' ) {
                    $avatar = '/uploads/user/no-avatar-' .  $row['gender'] . '.png';
                } else {
                    $avatar = '/uploads/user/sm_' . $row['img'];
                }
                $avatar = ' data-thumbnail="' . $avatar . '"';

                $selected = '';
                if ($row["user_id"]==$choice) $selected = ' selected';
                $result .= '<option' . $avatar . ' value="' . $row["user_id"] . '"' . $selected . '>' . stripslashes($row["full_name"]) . '</option>';
            }
            $result .= '</optgroup>';
        }

        $result .= selectUserSingle($row_ag['agency_id'], $choice);
    }

    if($parent==0) $result .= '</select>';
    return $result;
}

function selectMapTypeSingle($choice, $required = '') {
	global $db;
	$result = '';
	$result .= '<select class="form-control selectpicker" name="mapTypeId" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn icon..." ' . $required . '>';

	$db->table = 'maps_type';
	$db->condition = '`is_active` = 1';
	$db->order = '`title` ASC';
	$db->limit = '';
	$rows = $db->select('`maps_type_id`, `icon`, `title`');

	if($db->RowCount>0) {
		foreach($rows as $row) {
			$avatar = '';
			$selected = '';

			if ($row["maps_type_id"]==$choice) $selected = ' selected';

			if($row['icon']=='-no-' || $row['icon']=='' ) {
				$avatar = '/uploads/maps/no-icon.png';
			} else $avatar = '/uploads/maps/' . $row['icon'];

			$avatar = ' data-thumbnail="' . $avatar . '"';

			$result .= '<option' . $avatar . ' value="' . $row["maps_type_id"] . '"' . $selected . '>' . stripslashes($row["title"]) . '</option>';
		}
	}

	$result .= '</select>';

	return $result;
}

function selectShipTypeSingle($choice, $required = '') {
	global $db;
	$result = '';
	$result .= '<select class="form-control selectpicker" name="shipTypeId" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn loại tàu..." ' . $required . '>';

	$db->table = 'ship_type';
	$db->condition = '`isActive` = 1';
	$db->order = '`order` ASC';
	$db->limit = '';
	$rows = $db->select('`shipTypeId`, `name`');

	if($db->RowCount>0) {
		foreach($rows as $row) {
			$selected = '';

			if ($row["shipTypeId"]==$choice) $selected = ' selected';

			$result .= '<option value="' . $row["shipTypeId"] . '"' . $selected . '>' . stripslashes($row["name"]) . '</option>';
		}
	}

	$result .= '</select>';

	return $result;
}

function selectCountrySingle($choice, $required = '') {
	global $db;
	$result = '';
	$result .= '<select class="form-control selectpicker" name="countryId" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn quốc gia..." ' . $required . '>';

	$db->table = 'country';
	$db->condition = '`isActive` = 1';
	$db->order = '`order` ASC';
	$db->limit = '';
	$rows = $db->select('`countryId`, `name`');

	if($db->RowCount>0) {
		foreach($rows as $row) {
			$selected = '';

			if ($row["countryId"]==$choice) $selected = ' selected';

			$result .= '<option value="' . $row["countryId"] . '"' . $selected . '>' . stripslashes($row["name"]) . '</option>';
		}
	}

	$result .= '</select>';

	return $result;
}

function selectShipSingle($choice, $required = '') {
	global $db;
	$result = '';
	$result .= '<select class="form-control selectpicker" name="shipId" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn tàu..." ' . $required . '>';

	$db->table = 'ship';
	$db->condition = '`isActive` = 1';
	$db->order = '`name` ASC';
	$db->limit = '';
	$rows = $db->select('`shipId`, `name`');

	if($db->RowCount>0) {
		foreach($rows as $row) {
			$selected = '';

			if ($row["shipId"]==$choice) $selected = ' selected';

			$result .= '<option value="' . $row["shipId"] . '"' . $selected . '>' . stripslashes($row["name"]) . '</option>';
		}
	}

	$result .= '</select>';

	return $result;
}

function selectShipWeaponSingle($choice, $required = '') {
	global $db;
	$result = '';
	$result .= '<select class="form-control selectpicker" name="shipWeaponId" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn vũ khí tàu..." ' . $required . '>';

	$db->table = 'ship_weapon';
	$db->condition = '`isActive` = 1';
	$db->order = '`order` ASC';
	$db->limit = '';
	$rows = $db->select('`shipWeaponId`, `name`');

	if($db->RowCount>0) {
		foreach($rows as $row) {
			$selected = '';

			if ($row["shipWeaponId"]==$choice) $selected = ' selected';

			$result .= '<option value="' . $row["shipWeaponId"] . '"' . $selected . '>' . stripslashes($row["name"]) . '</option>';
		}
	}

	$result .= '</select>';

	return $result;
}

function selectFacebookGroupTarget($parent, $choice, $required = '') {
	
	global $db;

	$result = '';
	if($parent==0)  {
		$avatar = ' data-thumbnail="' . '/uploads/user/no-avatar-' .  '1' . '.png' . '"';
		$db->table = "facebook_target_group";
		$db->condition = "`is_active` = 1";
		$db->order = "`group_name` ASC";
		$db->limit = "";
		$rows_target = $db->select("`target_group_id`,`group_name`");
		$result .= '<select class="form-control selectpicker" name="user" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn nhóm đối tượng..." ' . $required . '>';

		if ($db->RowCount>0) {
			foreach($rows_target as $row) {
				$value = $row["group_name"];
				$selected = '';
				if ($row["group_name"]==$choice) $selected = ' selected';
				$result .= '<option' . $avatar . ' value ="' . $value . '"' .  $selected . '>' . $value .'</option>';
		}
	}
	$result .= '</select>';

	}
	

    return $result;
}

function selectUserMultiple($parent, array $choice, $required = '') {
    global $db;
    $result = '';
    if($parent==0)  {
        $result .= '<select class="form-control selectpicker" name="user[]" multiple data-live-search="true" data-selected-text-format="count" data-live-search-placeholder="Tìm..." title="Chọn nhân viên..." ' . $required . '>';

        $db->table = "core_user";
        $db->condition = "`is_active` = 1 AND `is_show` = 1 AND `agency` LIKE '[]'";
        $db->order = "`full_name` ASC";
        $db->limit = "";
        $rows = $db->select("`user_id`, `full_name`, `gender`, `img`");
        if($db->RowCount>0) {
            $result .= '<optgroup label="0 - ROOT">';
            foreach($rows as $row) {
                $avatar = '';
                if($row['img']=='-no-' || $row['img']=='' ) {
                    $avatar = '/uploads/user/no-avatar-' .  $row['gender'] . '.png';
                } else {
                    $avatar = '/uploads/user/sm_' . $row['img'];
                }
                $avatar = ' data-thumbnail="' . $avatar . '"';

                $selected = '';
                if (in_array($row["user_id"], $choice)) $selected = ' selected';
                $result .= '<option' . $avatar . ' value="' . $row["user_id"] . '"' . $selected . '>' . stripslashes($row["full_name"]) . '</option>';
            }
            $result .= '</optgroup>';
        }
    }

    $db->table = "agency";
    $db->condition = "`is_active` = 1 AND `parent` = $parent";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows_ag = $db->select("`agency_id`, `symbol`, `name`");
    foreach ($rows_ag as $row_ag) {
        $code = '"' . intval($row_ag['agency_id']) . '"';
        $db->table = "core_user";
        $db->condition = "`is_active` = 1 AND `is_show` = 1 AND `agency` LIKE '%$code%'";
        $db->order = "`full_name` ASC";
        $db->limit = "";
        $rows = $db->select("`user_id`, `full_name`, `gender`, `img`");
        if($db->RowCount>0) {
            $result .= '<optgroup label="' . stripslashes($row_ag['symbol']) . ' - ' . stripslashes($row_ag['name']) . '">';
            foreach($rows as $row) {
                $avatar = '';
                if($row['img']=='-no-' || $row['img']=='' ) {
                    $avatar = '/uploads/user/no-avatar-' .  $row['gender'] . '.png';
                } else {
                    $avatar = '/uploads/user/sm_' . $row['img'];
                }
                $avatar = ' data-thumbnail="' . $avatar . '"';

                $selected = '';
                if (in_array($row["user_id"], $choice)) $selected = ' selected';
                $result .= '<option' . $avatar . ' value="' . $row["user_id"] . '"' . $selected . '>' . stripslashes($row["full_name"]) . '</option>';
            }
            $result .= '</optgroup>';
        }

        $result .= selectUserMultiple($row_ag['agency_id'], $choice);
    }

    if($parent==0) $result .= '</select>';
    return $result;
}

function selectAgencySingle($choice, $required = '') {
    global $db;
    $result = '';
    $result .= '<select class="form-control selectpicker" name="agency" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn đơn vị..." ' . $required . '>';
    $result .= loadSingleAgency($db, 0, 0, $choice);
    $result .= '</select>';

    return $result;
}
function loadSingleAgency($db, $level, $parent, $choice){
    $result = '';
    $db->table = "agency";
    $db->condition = "`is_active` = 1 AND `parent` = $parent";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {
        $space = '&nbsp; ';
        for($i=0; $i<$level; $i++) {
            $space = $space . '&nbsp; ';
        }
        $selected = '';
        if($row['agency_id']==$choice) $selected = ' selected';
        $result .= '<option value="' . $row['agency_id'] . '"' . $selected . '>' . $space . '&rarr; ' . stripslashes($row['name']) . '</option>';
        $result .= loadSingleAgency($db, $level+1, $row['agency_id'], $choice);
    }
    return $result;
}

function selectAgencyMultiple(array $choice, $required = '') {
    global $db;
    $result = '';
    $result .= '<select id="agency" class="form-control selectpicker" name="list[]" multiple data-live-search="true" data-selected-text-format="count" data-live-search-placeholder="Tìm..." title="Chọn đơn vị..." ' . $required . '>';
    $result .= loadMultipleAgency($db, 0, 0, $choice);
    $result .= '</select>';

    return $result;
}
function loadMultipleAgency($db, $level, $parent, array $choice){
    $result = '';
    $db->table = "agency";
    $db->condition = "`is_active` = 1 AND `parent` = $parent";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {
        $space = '&nbsp; ';
        for($i=0; $i<$level; $i++) {
            $space = $space . '&nbsp; ';
        }
        $selected = '';
        if(in_array($row['agency_id'], $choice)) $selected = ' selected';
        $result .= '<option value="' . $row['agency_id'] . '"' . $selected . '>' . $space . '&rarr; ' . stripslashes($row['name']) . '</option>';
        $result .= loadMultipleAgency($db, $level+1, $row['agency_id'], $choice);
    }
    return $result;
}

/* Lấy Đơn vị chi nhánh chính Parent = 0  */
function getAgencyParent0($id) {
	global $db;
	$agency_id  = 0;
	$parent     = 999;
	$result     = 0;
	$db->table = "core_user";
	$db->condition = "`user_id` =" . ($id+0);
	$db->order = "";
	$db->limit = 1;
	$rows = $db->select("agency");
	foreach ($rows as $row) {
		$agency_id = $row['agency'] + 0;
	}
	$parent = $agency_id;

	while($parent != 0) {
		$db->table = "agency";
		$db->condition = "agency_id = " . ($parent);
		$db->order = "";
		$db->limit = 1;
		$rows = $db->select();
		if ($db->RowCount > 0) {
			foreach ($rows as $row) {
				$parent = ($row['parent'] + 0);
				$result = ($row['agency_id'] + 0);
			}
		} else $parent = 0;
	}
	return $result;
}

/* Lấy dãy ID Đơn vị của chính nó và các đơn vị con từ USER. */
function getAgencyToUser($id) {
	global $db;
	$agency = $result = array(-1);
	$db->table = "core_user";
	$db->condition = "`user_id` = $id";
	$db->order = "";
	$db->limit = 1;
	$rows = $db->select("agency");
	if($db->RowCount>0) {
        foreach ($rows as $row) {
            $agency = json_decode($row['agency']);
        }
        $result = $agency;

        foreach($agency as $val) {
            $element    = TRUE;
            $agency     = $val;
            while($element == TRUE) {
                $db->table = "agency";
                $db->condition = "`parent` IN ($agency)";
                $db->order = "";
                $db->limit = "";
                $rows = $db->select();
                if($db->RowCount>0) {
                    $agency = '';
                    $i = 0;
                    foreach ($rows as $row) {
                        if($i==0) $agency = intval($row['agency_id']);
                        else $agency .= ',' . intval($row['agency_id']);
                        array_push($result, intval($row['agency_id']));
                        $i++;
                    }
                } else $element = FALSE;
            }

        }
    }
	return array_keys(array_flip($result));
}

/* Lấy dẫy ID Đơn vị của chính nó và các đơn vị con từ AGENCY. */
function getAgencyElementPlus($id) {
	global $db;
	$agency_id  = $id;
	$element    = TRUE;
	$result     = 0;
	$result = $agency_id;
	while($element == TRUE) {
		$db->table = "agency";
		$db->condition = "`parent` IN (" . $agency_id . ")";
		$db->order = "";
		$db->limit = "";
		$rows = $db->select("`agency_id`");
		if($db->RowCount>0) {
			$agency_id = '';
			$i = 0;
			foreach ($rows as $row) {
				if($i==0) $agency_id = $row['agency_id'];
				else $agency_id .= ',' . $row['agency_id'];
				$result .= ',' . $row['agency_id'];
				$i++;
			}
		} else $element = FALSE;
	}
	return $result;
}

function getAgencyUserJobPlus($id) {
	global $db;
	$agency_id  = $id;
	$element    = TRUE;
	$result     = 0;
	$result = 'a' . $agency_id;
	$code = '"' . $agency_id . '"';
	$db->table = "core_user";
	$db->condition = "`agency` LIKE '$code'";
	$db->order = "";
	$db->limit = "";
	$rows_u = $db->select();
	foreach ($rows_u as $row_u) {
		$result .= ',u' . $row['user_id'];
	}
	while($element == TRUE) {
		$db->table = "agency";
		$db->condition = "`parent` IN (" . $agency_id . ")";
		$db->order = "";
		$db->limit = "";
		$rows = $db->select("`agency_id`");
		if($db->RowCount>0) {
			$agency_id = '';
			$i = 0;
			foreach ($rows as $row) {
				if($i==0) $agency_id = $row['agency_id'];
				else $agency_id .= ',' . $row['agency_id'];
				$result .= ',a' . $row['agency_id'];
				$i++;
				
				$code = '"' . $row['agency_id'] . '"';
				$db->table = "core_user";
				$db->condition = "`agency` LIKE '$code'";
				$db->order = "";
				$db->limit = "";
				$rows_u = $db->select();
				foreach ($rows_u as $row_u) {
					$result .= ',u' . $row['user_id'];
				}				
			}
		} else $element = FALSE;
	}
	return $result;
}

/* Lấy dẫy ID Đơn vị được phân quyền quản lý. */
function getAgencyToCore(array $core, $type = '/calendar_list;/') {
	$id = $result = array();
	$core = preg_grep($type, $core);
	$core = array_keys(array_flip($core));
	foreach ($core as $value) {
		$item = intval(substr(stristr($value, ';'), 1));
		array_push($id, $item);
	}

	foreach ($id as $value) {
		$item = getAgencyElementPlus($value);
		$item = explode(',', $item);
		$result = array_merge($item, $result);
	}
	$result = array_keys(array_flip($result));
	return $result;
}

/* Lấy dữ liệu table olala3w_agency */
function getTableAgency($id, $type='symbol') {
	global $db;
	$db->table = "agency";
	$db->condition = "`agency_id` = " . ($id+0);
	$db->order = "";
	$db->limit = 1;
	$rows = $db->select();
	foreach ($rows as $row) {
		return stripslashes($row[$type]);
	}
	if($type=='symbol' && $id==0) {
		return 'ROOT';
	} elseif($type=='name' && $id==0) {
		return 'TỔNG CÔNG TY';
	} else return '(không rõ)';
}

/* Lấy dữ liệu table olala3w_core_role */
function getTableCoreRole($id, $type='name') {
	global $db;
	$db->table = "core_role";
	$db->condition = "`role_id` = " . intval($id);
	$db->order = "";
	$db->limit = 1;
	$rows = $db->select();
	foreach ($rows as $row) {
		return stripslashes($row[$type]);
	}
	return '(không rõ)';
}

function getListUserNotify() {
	global $db;
	$result = array();

	$db->table = "core_privilege";
	$db->condition = "`privilege_slug` LIKE 'calendar_notify'";
	$db->order = "";
	$db->limit = "";
	$rows = $db->select();
	foreach($rows as $row) {
		$db->table = "role_user";
		$db->condition = "`role` = " . ($row['role_id']+0);
		$db->order = "";
		$db->limit = "";
		$rows2 = $db->select();
		foreach ($rows2 as $row2) {
			array_push($result, $row2['user']+0);
		}
	}

	$result = array_keys(array_flip($result));
	return $result;
}

function insertNotify($id, array $personnel, $user, $type = 1) {
	date_default_timezone_set(TTH_TIMEZONE);
	global $db;
	$current_time = time();

	$db->table = "notify";
	$data = array(
			'calendar' => $id,
			'type' => $type,
			'created_time' => $current_time,
			'user_id' => $user
	);
	$db->insert($data);
	$notify_id = $db->LastInsertID;

	$list_notify = array();
	$list_notify = array_merge($personnel, getListUserNotify());
	array_push($list_notify, $user);
	$list_notify = array_keys(array_flip($list_notify));
	foreach ($list_notify as $value) {
		if($value>0) {
			$onoff = 0;
			$db->table = "core_user";
			$db->condition = "`user_id` = $value";
			$db->order = "";
			$db->limit = 1;
			$rows_onoff = $db->select();
			foreach($rows_onoff as $row_onoff) {
				$onoff = $row_onoff['btn_notify1']+0;
			}

			if ($value == $user) {
				$db->table = "notify_user";
				$data = array(
					'notify' => $notify_id,
					'user_id' => $value,
					'status' => 1,
					'modified_time' => $current_time
				);
				$db->insert($data);
			} elseif($onoff==1) {
				$db->table = "notify_user";
				$data = array(
					'notify' => $notify_id,
					'user_id' => $value,
					'modified_time' => $current_time
				);
				$db->insert($data);
			} else {
				$db->table = "notify_user";
				$data = array(
					'notify' => $notify_id,
					'user_id' => $value,
					'status' => 1,
					'modified_time' => $current_time
				);
				$db->insert($data);
			}
		}
	}
	return true;
}

function convertTimeAgo($time) {
	date_default_timezone_set(TTH_TIMEZONE);
	$result = '';
	$time = time() - $time;
	if($time>0) {
		if($time<60) $result = $time . ' giây trước';
		elseif($time<3600) $result = round($time/60) . ' phút trước';
		elseif($time<86400) $result = round($time/3600) . ' giờ trước';
        elseif($time<172800) $result = 'hôm qua';
		elseif($time<604800) $result = round($time/86400) . ' ngày trước';
		elseif($time<2592000) $result = round($time/604800) . ' tuần trước';
		elseif($time<31536000) $result = round($time/2592000) . ' tháng trước';
		else $result = round($time/31536000) . ' năm trước';
	} else {
		$result = 'mới xong';
	}

	return $result;
}
function convertTimeDayAgo($time, $t1 = '', $t2 = '', $day = 10) {
    date_default_timezone_set(TTH_TIMEZONE);
    $date = new DateClass();
    if((time() - $time)>(86400*$day)) return $t1 . $date->vnDate($time);
    else return $t2 . convertTimeAgo($time);
}

function countLogWeek($user) {
	global $db;
	date_default_timezone_set(TTH_TIMEZONE);
	$week_start = date("Y-m-d", strtotime('monday this week'));
	$week_end = date("Y-m-d", strtotime('sunday this week'));
	$sum = 0;
	$db->table = "log";
	$db->condition = "`user_id` = '$user' AND `date` >= '$week_start' AND `date` <= '$week_end'";
	$db->order = "";
	$db->limit = "";
	$rows = $db->select();
	if($db->RowCount > 0) {
		foreach($rows as $row) {
			$sum = $sum + $row['count'];
		}
	}
	return formatNumberVN($sum);
}

function countRating($choice) {
    global $db;
    $db->table = "rating_item";
    $db->condition = "`rating` = $choice AND `is_active` = 1";
    $db->order = "";
    $db->limit = "";
    $db->select();
    return $db->RowCount;
}

function starsMark($id, $choice, $code=0, $to=0) {
    $result = '';

    $result .= '<div class="stars" data-id="' . $id .  '" data-role="' . $code . '" data-mark="' . $choice . '">';
    for($i=5; $i>=1; $i--) {
        $checked = '';
        if($i==$choice) $checked = ' checked';
        $result .= '<input class="star checkbox" id="star' . $to . '-' . $i . $id . '" type="radio" value="' . $i . '" name="mark[r' . $to . $id .  ']"' . $checked . '><label class="star" for="star' . $to . '-'  . $i . $id . '"></label>';
    }
    $result .= '</div>';

    return $result;
}
function starsMark2($id, $choice, $code=0, $to=0) {
    $result = '';

    $result .= '<div class="stars" data-id="' . $id .  '" data-role="' . $code . '" data-mark="' . $choice . '" data-to="' . $to . '">';
    for($i=5; $i>=1; $i--) {
        $checked = '';
        if($i==$choice) $checked = ' checked';
        $result .= '<input class="star checkbox" id="star' . $to . '-' . $i . $id . '" type="radio" value="' . $i . '" name="mark[r' . $to . $id .  ']"' . $checked . '><label class="star" for="star' . $to . '-'  . $i . $id . '"></label>';
    }
    $result .= '</div>';

    return $result;
}

function starsMarkShow($choice) {
    $result = '<div class="stars">';
    for($i=5; $i>=1; $i--) {
        $active = '';
        if($i==$choice) $active = ' active';
        $result .= '<label class="star' . $active . '"></label>';
    }
    $result .= '</div>';
    return $result;
}

function showMark($choice) {
    $result = '';
    if($choice>4)
        $result .= '<label class="kpi-mark mark-good">' . $choice . '</label>';
    elseif($choice<2)
        $result .= '<label class="kpi-mark mark-bad">' . $choice . '</label>';
    else
        $result .= '<label class="kpi-mark">' . $choice . '</label>';
    return $result;
}

function listCashBookDataTable($choice) {
    global $db;
    $query      = "";
    $core_cash  = coreCashBook($_SESSION["user_id"]);
    if(count($core_cash)>0 && !in_array(-1,$core_cash)) {
        $list = implode(',', $core_cash);
        $query = " AND `cash_book_id` IN ($list)";
    } elseif(count($core_cash)==0) {
        $query = " AND `cash_book_id` = 0";
    }

    $result = '<select name="cash_book[]" data-column="1" class="form-control filter selectpicker" multiple data-live-search="true" data-selected-text-format="count" data-live-search-placeholder="Tìm..." title="Chọn sổ quỹ..." required>';
    $result .= '<option value="">Chọn sổ quỹ...</option>';
    $db->table = "parents";
    $db->condition = "`type` LIKE 'cash' AND `is_active` = 1";
    $db->order = "`created_time` DESC";
    $db->limit = "";
    $rows_p = $db->select();
    foreach ($rows_p as $row_p) {
        $db->table = "cash_book";
        $db->condition = "`is_active` = 1 AND `parent` = " . intval($row_p['parents_id']) . $query;
        $db->order = "`created_time` DESC";
        $db->limit = "";
        $rows = $db->select();
        if($db->RowCount>0) {
            $result .= '<optgroup label="' . stripslashes($row_p['title']) . '">';
            foreach ($rows as $row) {
                $selected = '';
                if (intval($row['cash_book_id']) == $choice) $selected = ' selected';
                $result .= '<option value="' . intval($row['cash_book_id']) . '"' . $selected . '>' . stripslashes($row['name']) . '</option>';
            }
            $result .= '</optgroup>';
        }

    }
    $db->table = "cash_book";
    $db->condition = "`is_active` = 1 AND `parent` = 0" . $query;
    $db->order = "`created_time` DESC";
    $db->limit = "";
    $rows = $db->select();
    if($db->RowCount>0) {
        foreach ($rows as $row) {
            $selected = '';
            if (intval($row['cash_book_id']) == $choice) $selected = ' selected';
            $result .= '<option value="' . intval($row['cash_book_id']) . '"' . $selected . '>' . stripslashes($row['name']) . '</option>';
        }
    }
    $result .= '</select>';

    return $result;
}

function totalLikeComment($type, $id) {
    global $db;
    $total = 0;

    $db->table = "like";
    $db->condition = "`type` LIKE '$type' AND `id` = $id";
    $db->order = "";
    $db->limit = "";
    $db->select();
    $total += $db->RowCount;

    $db->table = "comment";
    $db->condition = "`type` LIKE '$type' AND `is_active` = 1 AND `id` = $id";
    $db->order = "";
    $db->limit = "";
    $db->select();
    $total += $db->RowCount;

    return $total;
}

function getTitleParent($choice) {
    global $db;
    $result = '';

    $db->table = "parents";
    $db->condition = "`is_active` = 1 AND `parents_id` = $choice";
    $db->order = "`created_time` DESC";
    $db->limit = "";
    $rows = $db->select();
    if($db->RowCount>0) {
        foreach($rows as $row) {
            $result = stripslashes($row['title']);
        }
    }
    return $result;
}

function getSlugBlogParent0($id) {
    global $db;
    $parent     = $id;
    $result     = '';

    while($parent != 0) {
        $db->table = "blog";
        $db->condition = "`blog_id` = $parent";
        $db->order = "";
        $db->limit = 1;
        $rows = $db->select("`blog_id`, `parent`, `slug`");
        if ($db->RowCount > 0) {
            foreach ($rows as $row) {
                $parent = intval($row['parent']);
                $result = stripslashes($row['slug']);
            }
        } else $parent = 0;
    }
    return $result;
}

function getBlogElementPlus($id) {
    global $db;
    $element    = TRUE;
    $result     = $id;
    while($element == TRUE) {
        $db->table = "blog";
        $db->condition = "`parent` IN ($id)";
        $db->order = "";
        $db->limit = "";
        $rows = $db->select();
        if($db->RowCount>0) {
            $id = '';
            foreach ($rows as $row) {
                if($id=='') $id = intval($row['blog_id']);
                else $id .= ',' . intval($row['blog_id']);
                $result .= ',' . intval($row['blog_id']);
            }
        } else $element = FALSE;
    }

    return $result;
}

function getBlogSlug($id) {
    global $db;
    $result = '';

    $db->table = "blog";
    $db->condition = "`blog_id` IN ($id)";
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select();
    if($db->RowCount>0) {
        foreach ($rows as $row) {
            $result = stripslashes($row['slug']);
        }
    }

    return $result;
}

function getBlogTags($choice, $slug) {
    global $db;
    $parent     = $choice;
    $result     = '';

    while($parent != 0) {
        $db->table = "blog";
        $db->condition = "`blog_id` = $parent";
        $db->order = "";
        $db->limit = 1;
        $rows = $db->select();
        if ($db->RowCount > 0) {
            foreach ($rows as $row) {
                $parent = intval($row['parent']);
                if($parent==0)
                    $result .= '<a href="' . HOME_URL_LANG . '/' . $slug . '" title="">#' . stripslashes($row['name']) . '</a>';
                else
                    $result .= '<a href="' . HOME_URL_LANG . '/' . $slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">#' . stripslashes($row['name']) . '</a>';
            }
        } else $parent = 0;
    }
    return $result;
}
function getTagsUser($choice) {
    global $db;
    $result = '';
    $user   = array(0);
    $user   = json_decode($choice);
    if(count($user)>0) {
        $user = implode(', ', $user);
        $db->table = "core_user";
        $db->condition = "`is_active` = 1 AND `is_show` = 1 AND `user_id` IN ($user)";
        $db->order = "";
        $db->limit = 1;
        $rows = $db->select();
        if ($db->RowCount > 0) {
            foreach ($rows as $row) {
                $result .= '<a href="javascript:;">@' . stripslashes($row['full_name']) . '</a>';
            }
        }
    }

    return $result;
}

function countKpiItem($rating, $to) {
    global $db;
    $db->table = "kpi_item";
    $db->condition = "`is_active` = 1 AND `rating_item` = $rating AND `kpi` IN (SELECT `kpi_id` FROM `" . TTH_DATA_PREFIX . "kpi` WHERE `is_active` = 1 AND `list_to` = $to)";
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select("COUNT(`kpi_item_id`) AS `count`");
    $total = 0;
    foreach($rows as $row) {
        $total = $row['count'];
    }
    return $total;
}

function queryCron($data, $type = 'insert', $id = 0) {
    global $db;

    if($type== 'insert') {
        $db->table = "cron";
        $db->insertMultiple($data);
    } else {
        $db->table = "cron";
        $db->condition = "`type` LIKE '$type' AND `id` = $id";
        $db->update($data);
    }
}

function kpiLoadRate($type, $to, $user, $portal = 0) {
    global $db;
    $result = '';

    if($type==1) {
        $db->table = "agency";
        $db->condition = "`is_active` = 1 AND `agency_id` = $to";
        $db->order = "`sort` ASC";
        $db->limit = 1;
        $db->select("`agency_id`");
        if($db->RowCount>0) {
            $k_mark = $k_code = array();
            $kpi = $update = 0;

            $db->table = "kpi";
            $db->condition = "`type` = 1 AND `is_active` = 1 AND `list_to` = $to AND `user_id` = $user AND `created_date` >= '" . date('Y-m-d', strtotime('-7 days')) . "'";
            $db->order = "`created_date` DESC";
            $db->limit = 1;
            $rows = $db->select("`kpi_id`, `done`");
            foreach ($rows as $row) {
                $kpi = intval($row['kpi_id']);
                $update = intval($row['done']);
                $db->table = "kpi_item";
                $db->condition = "`is_active` = 1 AND `kpi` = $kpi";
                $db->order = "";
                $db->limit = "";
                $rows_i = $db->select("`kpi_item_id`, `rating_item`, `mark`");
                foreach ($rows_i as $row_i) {
                    $k_mark[$row_i['rating_item']] = intval($row_i['mark']);
                    $k_code[$row_i['rating_item']] = intval($row_i['kpi_item_id']);
                }
            }

            $role = array();
            $db->table = "role_user";
            $db->condition = "`user` = $user";
            $db->limit = "";
            $db->order = "";
            $rows = $db->select("`role`");
            foreach ($rows as $row) {
                array_push($role, $row['role']);
            }

            $list = '"' . $to . '"';
            $db->table = "rating";
            $db->condition = "`type` = 1 AND `is_active` = 1 AND `list_to` LIKE '%$list%'";
            $db->order = "`sort` ASC, `coefficient` DESC, `rating_id` ASC";
            $db->limit = "";
            $rows = $db->select("`rating_id`, `name`, `approach`");
            if($db->RowCount>0) {
                $result .= '<input type="hidden" name="to" value="' . $to . '">';
                $result .= '<input type="hidden" name="type" value="1">';
                $result .= '<input type="hidden" name="kpi" value="' . $kpi . '">';
                $i = 0;
                foreach($rows as $row) {
                    $i++;
                    $result .= '<tr class="b-500"><td width="50px" align="center">' . $i . '</td><td colspan="3">' . stripslashes($row['name']) . '</td></tr>';

                    $db->table = "rating_item";
                    $db->condition = "`is_active` = 1 AND `rating` = " . intval($row['rating_id']);
                    $db->order = "`coefficient` DESC, `name` ASC";
                    $db->limit = "";
                    $rows_i = $db->select("`rating_item_id`, `name`, `coefficient`");
                    $approach = json_decode($row['approach']);
                    $ado = array_intersect($approach, $role);
                    $j = 0;
                    foreach ($rows_i as $row_i) {
                        $r = $class_i = '';
                        $value_m = isset($k_mark[$row_i['rating_item_id']]) ? intval($k_mark[$row_i['rating_item_id']]) : 0;
                        $value_c = isset($k_code[$row_i['rating_item_id']]) ? intval($k_code[$row_i['rating_item_id']]) : 0;
                        //---
                        if (intval($row_i['coefficient']) < 0) $class_i = ' class="kpi-red"';
                        if ($value_m > 0 || (empty($ado) && count($approach)>0)) $r = '<button class="btn-kpi-dt" data-id="' . $row_i['rating_item_id'] . '">&nbsp;</button>';
                        else $r = $i . '.' . $j;

                        $result .= '<tr><td' . $class_i . ' width="50px" align="center">' . $r . '</td>';
                        $result .= '<td>' . stripslashes($row_i['name']) . '</td>';

                        $stars = '';
                        if($update==0)
                            $stars = starsMark($row_i['rating_item_id'], $value_m, $value_c, $to);
                        else
                            $stars = starsMarkShow($value_m);

                        $count = countKpiItem($row_i['rating_item_id'], $to);
                        if($count>0) $count = '(' . formatNumberVN($count) . ' đánh giá)';
                        else $count = '';
                        if(empty($ado) && count($approach)>0) {
                            if(intval($row_i['coefficient'])>0) $result .= '<td width="110px" align="right">' . $count . '</td><td width="110px" align="center">&nbsp;</td>';
                            elseif(intval($row_i['coefficient'])<0) $result .= '<td width="110px" align="center">&nbsp;</td><td width="110px" align="left">' . $count . '</td>';
                        } else {
                            if(intval($row_i['coefficient'])>0) $result .= '<td width="110px" align="right">' . $count . '</td><td width="110px" align="center">' . $stars . '</td>';
                            elseif(intval($row_i['coefficient'])<0) $result .= '<td width="110px" align="center">' . $stars . '</td><td width="110px" align="left">' . $count . '</td>';
                        }

                        $result .= '</tr>';
                    }
                }
                if($update==0 && $portal==0) $result .= '<tr><td colspan="4" class="form-ol-btn-tzc"><button type="submit" class="btn btn-primary btn-round" disabled name="update">Lưu lại</button> &nbsp; <button type="button" class="btn btn-danger btn-round" onclick="location.href=\'./\';">Thoát</button></td></tr>';
            }
        }
    } elseif($type==2) {
        $db->table = "core_user";
        $db->condition = "`is_active` = 1 AND `is_show` = 1 AND `user_id` = $to";
        $db->order = "`sort` ASC";
        $db->limit = 1;
        $rs = $db->select("`gender`");
        if($db->RowCount>0) {
            $k_mark = $k_code = array();
            $kpi = $update = $gender = 0;
            foreach($rs as $r) {
                $gender = intval($r['gender']);
            }

            $db->table = "kpi";
            $db->condition = "`type` = 2 AND `is_active` = 1 AND `list_to` = $to AND `user_id` = $user AND `created_date` >= '" . date('Y-m-d', strtotime('-7 days')) . "'";
            $db->order = "`created_date` DESC";
            $db->limit = 1;
            $rows = $db->select("`kpi_id`, `done`");
            foreach ($rows as $row) {
                $kpi = intval($row['kpi_id']);
                $update = intval($row['done']);
                $db->table = "kpi_item";
                $db->condition = "`is_active` = 1 AND `kpi` = $kpi";
                $db->order = "";
                $db->limit = "";
                $rows_i = $db->select("`kpi_item_id`, `rating_item`, `mark`");
                foreach ($rows_i as $row_i) {
                    $k_mark[$row_i['rating_item']] = intval($row_i['mark']);
                    $k_code[$row_i['rating_item']] = intval($row_i['kpi_item_id']);
                }
            }

            $rating = array(0);
            $db->table = "role_user";
            $db->condition = "`user` = $to";
            $db->order = "";
            $db->limit = "";
            $rows_u = $db->select("`role`");
            foreach($rows_u as $row_u) {
                $list = '"' . $row_u['role'] . '"';
                $db->table = "rating";
                $db->condition = "`type` = 2 AND `is_active` = 1 AND `list_to` LIKE '%$list%'";
                $db->order = "`sort` ASC";
                $db->limit = "";
                $rows = $db->select("`rating_id`");
                if($db->RowCount>0) {
                    foreach($rows as $row) {
                        array_push($rating, $row['rating_id']);
                    }
                }
            }

            $rating = array_keys(array_flip($rating));
            $rating = implode(',', $rating);

            $role = array();
            $db->table = "role_user";
            $db->condition = "`user` = $user";
            $db->limit = "";
            $db->order = "";
            $rows = $db->select("`role`");
            foreach ($rows as $row) {
                array_push($role, $row['role']);
            }

            $db->table = "rating";
            $db->condition = "`type` = 2 AND `is_active` = 1 AND `rating_id` IN ($rating) AND `gender` IN (0, $gender)";
            $db->order = "`sort` ASC, `coefficient` DESC, `rating_id` ASC";
            $db->limit = "";
            $rows = $db->select("`rating_id`, `name`, `approach`");
            if($db->RowCount>0) {
                $result .= '<input type="hidden" name="to" value="' . $to . '">';
                $result .= '<input type="hidden" name="type" value="2">';
                $result .= '<input type="hidden" name="kpi" value="' . $kpi . '">';
                $i = 0;
                foreach($rows as $row) {
                    $i++;
                    $result .= '<tr class="b-500"><td width="50px" align="center">' . $i . '</td><td colspan="3">' . stripslashes($row['name']) . '</td></tr>';

                    $db->table = "rating_item";
                    $db->condition = "`is_active` = 1 AND `rating` = " . intval($row['rating_id']) . " AND `gender` IN (0, $gender)";
                    $db->order = "`coefficient` DESC, `name` ASC";
                    $db->limit = "";
                    $rows_i = $db->select("`rating_item_id`, `name`, `coefficient`");
                    $approach = json_decode($row['approach']);
                    $ado = array_intersect($approach, $role);
                    $j = 0;
                    foreach ($rows_i as $row_i) {
                        $j++;
                        $r = $class_i = '';
                        $value_m = isset($k_mark[$row_i['rating_item_id']]) ? intval($k_mark[$row_i['rating_item_id']]) : 0;
                        $value_c = isset($k_code[$row_i['rating_item_id']]) ? intval($k_code[$row_i['rating_item_id']]) : 0;
                        //---
                        if (intval($row_i['coefficient']) < 0) $class_i = ' class="kpi-red"';
                        if ($value_m > 0 || (empty($ado) && count($approach)>0)) $r = '<button class="btn-kpi-dt" data-id="' . $row_i['rating_item_id'] . '">&nbsp;</button>';
                        else $r = $i . '.' . $j;

                        $result .= '<tr><td' . $class_i . ' width="50px" align="center">' . $r . '</td>';
                        $result .= '<td>' . stripslashes($row_i['name']) . '</td>';

                        $stars = '';
                        if($update==0)
                            $stars = starsMark($row_i['rating_item_id'], $value_m, $value_c, $to);
                        else
                            $stars = starsMarkShow($value_m);

                        $count = countKpiItem($row_i['rating_item_id'], $to);
                        if($count>0) $count = '(' . formatNumberVN($count) . ' đánh giá)';
                        else $count = '';
                        if(empty($ado) && count($approach)>0) {
                            if(intval($row_i['coefficient'])>0) $result .= '<td width="110px" align="right">' . $count . '</td><td width="110px" align="center">&nbsp;</td>';
                            elseif(intval($row_i['coefficient'])<0) $result .= '<td width="110px" align="center">&nbsp;</td><td width="110px" align="left">' . $count . '</td>';
                        } else {
                            if(intval($row_i['coefficient'])>0) $result .= '<td width="110px" align="right">' . $count . '</td><td width="110px" align="center">' . $stars . '</td>';
                            elseif(intval($row_i['coefficient'])<0) $result .= '<td width="110px" align="center">' . $stars . '</td><td width="110px" align="left">' . $count . '</td>';
                        }

                        $result .= '</tr>';
                    }
                }
                if($update==0 && $portal==0) $result .= '<tr><td colspan="4" class="form-ol-btn-tzc"><button type="submit" class="btn btn-primary btn-round" disabled name="update">Lưu lại</button> &nbsp; <button type="button" class="btn btn-danger btn-round" onclick="location.href=\'/\'">Thoát</button></td></tr>';
            }
        }
    }

    return $result;
}

function selectWarning($choice, $required = '') {
	$selectedYes = '';
	$selectedNo = '';

	if ($choice == 1) $selectedYes = ' selected';
	else $selectedNo = ' selected';

	$result = '<select class="form-control selectpicker" name="isWarning"' . $required . '>';
	$result .= '<option value=1' . $selectedYes . '>Có</option>';
	$result .= '<option value=0' . $selectedNo . '>Không</option>';
	$result .= '</select>';

	return $result;
}

function getWarning($status)
{
  if ($status == 1) return 'Có';
  return '-';
}
/// convertCooodirate
function  convertToGeometry($lat,$long)
{
	$resultLat = explode("°", $lat);
	$resultLong = explode("°", $long);
	$directionLat = substr($lat, -1);
	$directionLong = substr($long, -1);
	$latitude  = floatval($resultLat[0]) + floatval($resultLat[1]) / 60;
	if($directionLat =='S'){
		$latitude = 0 - $latitude;
	}
	$longitude = floatval($resultLong[0]) + floatval($resultLong[1]) / 60;
	if($directionLong =='W')
	{
		$longitude = - $longitude;
	}
	$data   = array($longitude, $latitude);
	return $data;
}
////
function selectSingleLocal($choice)
{
  global $db;
  $result = '';

  $result .= '<select name="localId" class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn địa bàn...">';
  $db->table = "local";
  $db->condition = "`is_active` = 1";
  $db->order = "`sort` ASC";
  $db->limit = "";
  $rows = $db->select();
  if ($db->RowCount > 0) {
    foreach ($rows as $row) {
      $selected = '';
      if ($row["local_id"] == $choice) $selected = ' selected';

      $result .= '<option value="' . intval($row["local_id"]) . '"' . $selected . '>' . stripslashes($row["title"]) . '</option>';
    }
  }
  $result .= '</select>';
  return $result;
}

function selectMultipleFbTargetId(array $choice)
{
    global $db;
    $result = '';

    $result .= '<select style="width: 50px !important; min-width: 50px; max-width: 50px;" name="fbTargetId[]" class="form-control selectpicker" multiple data-live-search="true" data-selected-text-format="count" data-live-search-placeholder="Tìm..." title="Chọn facebook...">';
    $db->table = "fb_target";
    $db->condition = "`isActive` = 1";
    $db->order = "`name` ASC";
    $db->limit = "";
    $rows = $db->select('`fbTargetId`, `name`');
    if ($db->RowCount > 0) {
        foreach ($rows as $row) {
            $selected = '';
            if (in_array($row['fbTargetId'], $choice)) $selected = ' selected';

            $result .= '<option value="' . intval($row["fbTargetId"]) . '"' . $selected . '>' . stripslashes($row["name"]) . '</option>';
        }
    }
    $result .= '</select>';
    return $result;
}

function listFbInGroup($lists) {
	global $db;
	$result = '<div class="bootstrap-tagsinput">';
	for ($i =0; $i< count($lists); $i++) {
			$db->table = "fb_target";
			$db->order = "`name` ASC";
			$db->limit = "1";
			$db->condition = "`isActive` = 1 AND `fbTargetId` = $lists[$i]";
			$row = $db->select();
			$facebookName = stripslashes($row[0]['name']);
			$facebookLink = stripslashes($row[0]['link']);
			if($db->RowCount>0) {
					$result .= '<a href ="' . $facebookLink .'"><span class="tag label label-info">'. $facebookName .'</span></a>';
					if ($i != count($lists) - 1) $result .= ', ';
			}
	}
	$result .= '</div>';

	return $result;
}
