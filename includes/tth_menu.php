<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

$mmenu = array();
$mmenu['home'] = array(
  'link'  => '/home',
  'title' => 'Báo cáo Biển Đông',
  'icon'  => 'fa-globe'
);

$mmenu['field-situation'] = array(
  'link'  => '/field-situation',
  'title' => 'Tình hình trên thực địa',
  'icon'  => 'fa-ship'
);

$mmenu['opinion'] = array(
  'link'  => '/opinion',
  'title' => 'Quan điểm, lập trường Biển Đông',
  'icon'  => 'fa-flag'
);

$mmenu['tracking'] = array(
  'link'  => '/tracking',
  'title' => 'Dữ liệu',
  'icon'  => 'fa-dot-circle-o',
  'sub'   => array(
    array('link'  => '/tracking-local', 'title' => 'Khu vực'),
    array(
      'link'  => '/tracking-ship',
      'title' => 'Tàu tuyền trên Biển Đông',
      'sub'   => array(
        array(
          'link'  => '/ship',
          'title' => 'Đối tượng tàu thuyền'
        ),
        array(
          'link' => '/ship-type',
          'title' => 'Phân loại tàu thuyền',
        ),
        array(
          'link' => '/ship-weapon',
          'title' => 'Vũ khí trên tàu'
        ),
      ),
    ),

  )
);

$mmenu['month-report'] = array(
  'link'  => '/month-report',
  'title' => 'Báo cáo theo tháng',
  'icon'  => 'fa-sticky-note'
);



?>
<aside>
  <div id="sidebar" class="nav-collapse <?php echo $hide_left_bar; ?>">
    <!-- sidebar menu start-->
    <div class="leftside-navigation">
      <ul class="sidebar-menu" id="nav-accordion">
        <?php
        foreach ($mmenu as $key => $value) {
          if (in_array(substr($value['link'], 1), $corePrivilegeSlug['ol'])) {
            $active = '';
            if ($tth[TTH_PATH] == substr($value['link'], 1)) $active = ' class="active"';
            if (empty($value['sub'])) {
              echo '<li><a' . $active . ' href="' . HOME_URL_LANG . $value['link'] . '" title="' . $value['title'] . '"><i class="fa ' . $value['icon'] . '"></i><span>' . $value['title'] . '</span></a></li>';
            } else {
              echo '<li class="sub-menu">';
              echo '<a' . $active . ' href="javascript:;" title="' . $value['title'] . '"><i class="fa ' . $value['icon'] . '"></i><span>' . $value['title'] . '</span></a>';
              echo '<ul class="sub">';
              foreach ($value['sub'] as $k => $v) {
                if (in_array(substr($v['link'], 1), $corePrivilegeSlug['op'])) {
                  if (empty($v['sub'])) {
                    $active = '';
                    if (($tth[TTH_PATH] == substr($value['link'], 1)) && ($tth[TTH_PATH_OP] == substr($v['link'], 1))) $active = ' class="active"';
                    echo '<li' . $active . '><a href="' . HOME_URL_LANG . $value['link'] . $v['link'] . '" title="' . $v['title'] . '">' . $v['title'] . '</a></li>';
                  } else {
                    $active = '';
                    if (($tth[TTH_PATH] == substr($value['link'], 1)) && in_array('/' . $tth[TTH_PATH_OP], array_column($v['sub'], 'link'))) $active = ' class="active"';
                    echo '<li class="sub-menu">';
                    echo '<a' . $active . ' href="javascript:;" title="' . $v['title'] . '">' . $v['title'] . '</a>';
                    echo '<ul class="sub">';
                    foreach ($v['sub'] as $kk => $vv) {
                      $active = '';
                      if (($tth[TTH_PATH] == substr($value['link'], 1)) && ($tth[TTH_PATH_OP] == substr($vv['link'], 1))) $active = ' class="active"';
                      echo '<li' . $active . '><a href="' . HOME_URL_LANG . $value['link'] . $vv['link'] . '" title="' . $vv['title'] . '">' . $vv['title'] . '</a></li>';
                    }
                    echo '</ul>';
                    echo '</li>';
                  }
                }
              }
              echo '</ul>';
              echo '</li>';
            }
          }
        }

        ?>
      </ul>
    </div>
    <!-- sidebar menu end-->
  </div>
</aside>