<?php
class DateClass
{
	public function __construct() {
		date_default_timezone_set(TTH_TIMEZONE);
	}

	public function vnTime($time) {
		if($time>0) return self::time($time);
		else return '';
	}
	public function vnDate($time) {
		if($time>0) return self::date($time);
		else return '';
	}
	public function vnDateTime($time) {
		if($time>0) return self::dateTime($time);
		else return '';
	}
	public function vnOther($time, $_Phare) {
		if($time>0) {
			$out_date = date($_Phare, $time);
			return $out_date;
		} else return '';
	}
	public function vnFull($time) {
		if($time>0) {
			$out_date = date(TTH_FULL_DATE_FORMAT, $time);
			$str_search = array(
					"Mon",
					"Tue",
					"Wed",
					"Thu",
					"Fri",
					"Sat",
					"Sun",
					"AM",
					"PM",
					":"
			);
			$str_replace = array(
					"Thứ Hai",
					"Thứ Ba",
					"Thứ Tư",
					"Thứ Năm",
					"Thứ Sáu",
					"Thứ Bảy",
					"Chủ Nhật",
					" phút, sáng",
					" phút, chiều",
					":"
			);
			return str_replace($str_search, $str_replace, $out_date);
		} else return '';
	}
    public function vnFullDay($time) {
        if($time>0) {
            $out_date = date('D, ' . TTH_DATE_FORMAT, $time);
            $str_search = array(
                "Mon",
                "Tue",
                "Wed",
                "Thu",
                "Fri",
                "Sat",
                "Sun"
            );
            $str_replace = array(
                "Thứ Hai",
                "Thứ Ba",
                "Thứ Tư",
                "Thứ Năm",
                "Thứ Sáu",
                "Thứ Bảy",
                "Chủ Nhật"
            );
            return str_replace($str_search, $str_replace, $out_date);
        } else return '';
    }
	public function dateTimeOther($time, $_Phare) {
		if($time>0) {
			$out_date = date($_Phare, $time);
			$str_search = array(
					"Mon",
					"Tue",
					"Wed",
					"Thu",
					"Fri",
					"Sat",
					"Sun",
					"AM",
					"PM",
					":"
			);
			$str_replace = array(
					"Thứ hai",
					"Thứ ba",
					"Thứ tư",
					"Thứ năm",
					"Thứ sáu",
					"Thứ bảy",
					"Chủ nhật",
					" phút, sáng",
					" phút, chiều",
					":"
			);
			return str_replace($str_search, $str_replace, $out_date);
		} else return '';
	}

	public function time($time) {
		$out_date = date(TTH_TIME_FORMAT, $time);
		return $out_date;
	}
	public function date($time) {
		$out_date = date(TTH_DATE_FORMAT, $time);
		return $out_date;
	}
	public function dateTime($time) {
		$out_date = date(TTH_DATETIME_FORMAT, $time);
		return $out_date;
	}

	public function dmYtoYmd($datetime) {
		$datetime = str_replace('/', '-', $datetime);
		$result = strtotime($datetime);
		if($result>0) return date('Y-m-d H:i:s', $result);
		else return '';
	}

	public function convertYmdTodmY($date) {
		return date('d/m/Y',strtotime($date));
	}

	public function dmYtoYmd2($date) {
		$date = str_replace('/', '-', $date);
		$result = strtotime($date);
		if($result>0) return date('Y-m-d', $result);
		else return '';
	}
}