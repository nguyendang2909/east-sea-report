<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['jobs']['link'] . '">' . $mmenu['jobs']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">' . $mmenu['jobs']['sub'][0]['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "jobs.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
$date = new DateClass();
if($typeFunc=='add'){
    $list   = isset($_POST['list']) ? $_POST['list'] : array();
    $list   = array_keys(array_flip($list));
    $begin 	= isset($_POST['begin']) ? trim($_POST['begin']) : $date->vnDateTime(time());
    $end 	= isset($_POST['end']) ? trim($_POST['end']) : $date->vnDateTime(time());
    if(count($list)==0) $error = '<span class="show-error">Vui lòng chọn người nhận việc.</span>';
	else {
		$file_max_size	= FILE_MAX_SIZE;
		$dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'jobs' . DS;
		
		$f_name = array();
        if(empty(array_sum($_FILES['files']['error']))) {
            $images = array();
            foreach ($_FILES['files'] as $k => $l) {
                foreach ($l as $i => $v) {
                    if (!array_key_exists($i, $images))
                        $images[$i] = array();
                    $images[$i][$k] = $v;
                }
            }
            foreach ($images as $image) {
                $filename = substr($image['name'], 0, strrpos($image['name'], '.'));
                $fileUp = new Upload($image);
                $fileUp->file_max_size = $file_max_size;
                $fileUp->file_new_name_body = $filename;
                $fileUp->Process($dir_dest);
                if($fileUp->processed) {
                    array_push($f_name, $fileUp->file_dst_name);
                    $OK = true;
                } else {
                    $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
                }
            }

        } else {
            $OK = true;
        }
		
		$begin 	= $date->dmYtoYmd($begin);
        $end 	= $date->dmYtoYmd($end);

        if(strtotime($begin) > strtotime($end))
            $end = date('Y-m-d H:i:s', strtotime($begin) + 43200);

        $f_name = json_encode($f_name, JSON_UNESCAPED_UNICODE);
        if($OK) {			
			$db->table = "jobs";
			$data = array(
				'title'         => $db->clearText($title),
				'list_to'       => $db->clearText(json_encode($list)),
				'level'         => intval($level),
				'begin'         => $db->clearText($begin),
				'end'           => $db->clearText($end),
				'files'         => $db->clearText($f_name),
				'note'          => $db->clearText($note),
				'created_time'  => time(),
				'modified_time' => time(),
				'user_id'       => $_SESSION["user_id"]
			);
			$db->insert($data);
			
            loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['jobs']['link'] . $mmenu['jobs']['sub'][2]['link']);
		}
		$OK = true;
	}
}
else {
	$title 		= "";
	$list		= array();
	$level		= 0;
	$begin		= $date->vnDateTime(time());
	$end		= "";
	$files		= array();
	$note		= "";
}
if(!$OK) jobs(HOME_URL_LANG . $mmenu['jobs']['link'] . '/job-add', "add", 0, $title, $list, $level, $begin, $end, $files, $note, $error);