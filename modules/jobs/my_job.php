<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['jobs']['link'] . '">' . $mmenu['jobs']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">' . $mmenu['jobs']['sub'][1]['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---
?>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="fa fa-pencil-square-o fa-fw"></i> Lịch công việc của bạn &nbsp;
                <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-eye"></a>
                <a href="javascript:;" class="fa fa-compress"></a>
                <a href="javascript:;" class="fa fa-times"></a>
            </span>
            </div>
            <div class="panel-body">
                <div id="calendar" class="has-toolbar"></div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/js/fullcalendar/fullcalendar.min.css">
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/moment.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/fullcalendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/fullcalendar/locale-all.js"></script>
<script type="text/javascript">
    $(function() {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            disableDragging: true,
            editable: false,
            navLinks: true,
            views: {
                month: {
                    eventLimit: true
                },
                agenda: {
                    eventLimit: false
                }
            },
            scrollTime: "07:00:00",
            locale: 'vi',
            events: function(start, end, timezone, callback) {
                $.ajax({
                    url: '/action.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        url: 'my_job',
                        type: 'load',
                        start: start.unix(),
                        end: end.unix()
                    },
                    async: false,
                    success: function(s) {
                        callback(s);
                    }
                });
            },
            eventClick: function(event) {
                if (event.id) {
                    open_jobs('open', event.id);
                    return false;
                }
            },
            eventRender: function(event, eventElement) {
                if (event.icon) {
                    eventElement.find(".fc-title").prepend('<i class="fa ' + event.icon + '"></i> ');
                }
            },
            axisFormat: 'HH:mm',
            timeFormat: 'H(:mm)'
        });
    });
</script>