<?php
include_once(_F_TEMPLATES . DS . "kgm_attt_report.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['report']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['report']['link'] . '/kgm-attt-report' . '">' . $mmenu['report']['sub'][0]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Sửa</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'add') {
  $date = new DateClass();

  // Validate data
  if (
    empty($newsSummary)
    || empty($newsContent)
    || empty($newsSource)
    || empty($reportDate)
  ) $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
  else {
    $file_max_size  = FILE_MAX_SIZE;
    $dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'network' . DS;

    $file_name      = 'os' . time() . '_' . md5(microtime(true));
    $file_size      = $_FILES['files']['size'];
    if ($file_size > 0) {
      $fileUp = new Upload($_FILES['files']);
      $fileUp->file_max_size = $file_max_size;
      $fileUp->file_new_name_body = $file_name;
      $fileUp->Process($dir_dest);
      if ($fileUp->processed) {
        $file_name = $fileUp->file_dst_name;
        $OK = true;
      } else {
        $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
      }
    } else {
      $OK = true;
      $file_name = '-no-';
    }

    $reportDate = $date->dmYtoYmd($reportDate);

    if ($OK) {
      $db->table = "kgm_attt_reports";
      $data = array(
        'newsSummary' => $db->clearText($newsSummary),
        'newsContent' => $db->clearText($newsContent),
        'newsRecommendation' => $db->clearText($newsRecommendation),
        'newsSource' => $db->clearText($newsSource),
        'categoryId' => $categoryId,
        'reportDate' => $reportDate,
        'note' => $db->clearText($note),
        'files' => $db->clearText($file_name),
        'createdBy'       =>  $_SESSION["user_id"],
        'createdAt'  => date("Y-m-d H:i:s"),
        'updatedAt' => date("Y-m-d H:i:s"),
        'updatedBy' =>  $_SESSION["user_id"],
      );
      $db->insert($data);

      loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['report']['link'] . '/kgm-attt-report');
    }
    $OK = true;
  }
} else {
  $newsSummary = "";
  $newsContent = "";
  $newsRecommendation = "";
  $newsSource = "";
  $categoryId = "";
  $reportDate = "";
  $note = "";
  $files = "";
}
if (!$OK) kgmAtttReport(HOME_URL_LANG . $mmenu['report']['link'] . '/kgm-attt-report-add', "add", 0, $newsSummary, $newsContent, $newsRecommendation, $newsSource, $categoryId, $reportDate, $note, $files, $error);
