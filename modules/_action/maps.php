<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//

if($account["id"]>0) {
    $type       	= isset($_POST['type']) ? trim($_POST['type']) : '-no-';
    $f_type     	= isset($_POST['f_type']) ? trim($_POST['f_type']) : '';
    $f_coordinates	= isset($_POST['f_coordinates']) ? trim($_POST['f_coordinates']) : '';
    $title       	= isset($_POST['title']) ? trim($_POST['title']) : '';
    $m_type       	= isset($_POST['m_type']) ? intval($_POST['m_type']) : 0;
    $note       	= isset($_POST['note']) ? trim($_POST['note']) : '';
	$imgurl         = isset($_POST['imgurl']) ? trim($_POST['imgurl']) : '';
	$iswarning         = isset($_POST['warning']) ? 1 : 0;
	$warningnote         = isset($_POST['warningnote']) ? trim($_POST['warningnote']) : '';

	$OK         	= false;
	$msg        	= '';
	$file 			= isset($_FILES['file']) ? $_FILES['file'] : null;
	
    if($type=='add' && in_array("maps-gis-add", $corePrivilegeSlug['op'])) {
		if(empty($title)) {
			$msg = 'Vui lòng nhập đầy đủ dữ liệu vị trí.';
		} else {
			if($file["name"] != ''){
				$target_dir = "uploads/maps/doc/";
				$target_file = $target_dir.basename($file["name"]);
				$msg= $target_file;
				if(move_uploaded_file($file["tmp_name"], $target_file)){
					$msg ="Upload successed!";
					$msg = realpath($target_file);
				}else{
					//$msg = "Error Uploaded!";
				}
			}
			$db->table = "maps";
			$data = array(
			    'type'          => intval($m_type),
                'title'         => $db->clearText($title),
                'f_type'        => $db->clearText($f_type),
                'f_coordinates' => $db->clearText($f_coordinates),
                'created_time'  => time(),
                'modified_time' => time(),
                'user_id'       => intval($account["id"]),
                'note'          => $note,
				'imgurl'        => $imgurl,
				'warning'		=> $iswarning,
				'warningnote'		=> $warningnote,
				'attached'		=> basename($target_file)

            );
			$db->insert($data);
			if($db->LastInsertID>0) {
                $OK = true;
            } else
                $msg += 'Lỗi dữ liệu, vui lòng thực hiện lại.';
		}
    }
	
    echo json_encode(array("process" => $OK, "msg" => $msg));
	
} else die(http_response_code(404));