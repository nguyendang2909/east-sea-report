<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $key    = isset($_POST['key']) ? trim($_POST['key']) : '';
    $date   = new DateClass();
    //
	if($type=='load') {
        $requestData = $_REQUEST;
        $rows_all = $distance = array();

        $db->table = "device";
        $db->condition = "`is_active` = 1 AND `title` LIKE '%" . $db->clearText($key) . "%'";
        $db->order = "`created_time` DESC";
        $db->limit = "";
        $rows = $db->select();
        $rows_all = array_merge($rows_all, $rows);

        $db->table = "device_item";
        $db->condition = "`is_active` = 1 AND `title` LIKE '%" . $db->clearText($key) . "%'";
        $db->order = "`created_time` DESC";
        $db->limit = "";
        $rows = $db->select();
        $rows_all = array_merge($rows_all, $rows);

        $db->table = "software";
        $db->condition = "`is_active` = 1 AND `title` LIKE '%" . $db->clearText($key) . "%'";
        $db->order = "`created_time` DESC";
        $db->limit = "";
        $rows = $db->select();
        $rows_all = array_merge($rows_all, $rows);

        $db->table = "software_item";
        $db->condition = "`is_active` = 1 AND `title` LIKE '%" . $db->clearText($key) . "%'";
        $db->order = "`created_time` DESC";
        $db->limit = "";
        $rows = $db->select();
        $rows_all = array_merge($rows_all, $rows);

        $db->table = "network_profile";
        $db->condition = "`is_active` = 1 AND `title` LIKE '%" . $db->clearText($key) . "%'";
        $db->order = "`created_time` DESC";
        $db->limit = "";
        $rows = $db->select();
        $rows_all = array_merge($rows_all, $rows);

        $db->table = "network_ip";
        $db->condition = "`is_active` = 1 AND `ip` LIKE '%" . $db->clearText($key) . "%'";
        $db->order = "`created_time` DESC";
        $db->limit = "";
        $rows = $db->select();
        $rows_all = array_merge($rows_all, $rows);

        $db->table = "agency";
        $db->condition = "`is_active` = 1 AND `name` LIKE '%" . $db->clearText($key) . "%'";
        $db->order = "`created_time` DESC";
        $db->limit = "";
        $rows = $db->select();
        $rows_all = array_merge($rows_all, $rows);

        $db->table = "core_user";
        $db->condition = "`is_active` = 1 AND `is_show` = 1 AND `full_name` LIKE '%" . $db->clearText($key) . "%'";
        $db->order = "`full_name` ASC";
        $db->limit = "";
        $rows = $db->select();
        $rows_all = array_merge($rows_all, $rows);


        foreach ($rows_all as $key => $row) {
            $distance[$key] = $row['created_time'];
        }
        array_multisort($distance, SORT_DESC, $rows_all);

        $totalData = $totalFiltered = count($rows_all);

        $b = intval($requestData['start']);
        if($totalData>intval($requestData['length'])) {
            $e = intval($requestData['start'] + 1) * intval($requestData['length']);
        } else
            $e = $totalData;

        $data = array();
        if($totalData>0) {
            for($i=$b; $i<=$e; $i++) {
                if(isset($rows_all[$i]['device_id'])) {
                    $nestedData =   array();
                    $nestedData[]   = $i + 1;
                    $nestedData[]   = 'Nhóm phần cứng';
                    $nestedData[]   = stripslashes($rows_all[$i]['title']);
                    $tool = '';
                    if(in_array("device-category-edit", $corePrivilegeSlug['op'])) {
                        $tool .= '<a href="' . HOME_URL_LANG . '/network/device-category-edit?id=' . intval($rows_all[$i]['device_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a>';
                    }
                    $nestedData[] = $tool;
                    $data[] = $nestedData;
                } elseif(isset($rows_all[$i]['device_item_id'])) {
                    $nestedData =   array();
                    $nestedData[]   = $i + 1;
                    $nestedData[]   = 'Thiết bị phần cứng';
                    $nestedData[]   = stripslashes($rows_all[$i]['title']);
                    $tool = '';
                    if(in_array("device-item-edit", $corePrivilegeSlug['op'])) {
                        $tool .= '<a href="' . HOME_URL_LANG . '/network/device-item-id?id=' . intval($rows_all[$i]['device_item_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a>';
                    }
                    $nestedData[] = $tool;
                    $data[] = $nestedData;
                } elseif(isset($rows_all[$i]['software_id'])) {
                    $nestedData =   array();
                    $nestedData[]   = $i + 1;
                    $nestedData[]   = 'Nhóm phần mềm';
                    $nestedData[]   = stripslashes($rows_all[$i]['title']);
                    $tool = '';
                    if(in_array("software-category-edit", $corePrivilegeSlug['op'])) {
                        $tool .= '<a href="' . HOME_URL_LANG . '/network/software-category-edit?id=' . intval($rows_all[$i]['software_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a>';
                    }
                    $nestedData[] = $tool;
                    $data[] = $nestedData;
                } elseif(isset($rows_all[$i]['software_item_id'])) {
                    $nestedData =   array();
                    $nestedData[]   = $i + 1;
                    $nestedData[]   = 'Phần mềm ứn dụng';
                    $nestedData[]   = stripslashes($rows_all[$i]['title']);
                    $tool = '';
                    if(in_array("software-item-edit", $corePrivilegeSlug['op'])) {
                        $tool .= '<a href="' . HOME_URL_LANG . '/network/software-item-id?id=' . intval($rows_all[$i]['software_item_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a>';
                    }
                    $nestedData[] = $tool;
                    $data[] = $nestedData;
                } elseif(isset($rows_all[$i]['network_profile_id'])) {
                    $nestedData =   array();
                    $nestedData[]   = $i + 1;
                    $nestedData[]   = 'Hồ sơ mạng';
                    $nestedData[]   = stripslashes($rows_all[$i]['title']);
                    $tool = '';
                    if(in_array("network-profile-edit", $corePrivilegeSlug['op'])) {
                        $tool .= '<a href="' . HOME_URL_LANG . '/network/network-profile-edit?id=' . intval($rows_all[$i]['network_profile_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a>';
                    }
                    $nestedData[] = $tool;
                    $data[] = $nestedData;
                } elseif(isset($rows_all[$i]['network_ip_id'])) {
                    $nestedData =   array();
                    $nestedData[]   = $i + 1;
                    $nestedData[]   = 'Địa chỉ IP';
                    $nestedData[]   = stripslashes($rows_all[$i]['ip']);
                    $tool = '';
                    if(in_array("network-ip-edit", $corePrivilegeSlug['op'])) {
                        $tool .= '<a href="' . HOME_URL_LANG . '/network/network-ip-edit?id=' . intval($rows_all[$i]['network_ip_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a>';
                    }
                    $nestedData[] = $tool;
                    $data[] = $nestedData;
					
                } elseif(isset($rows_all[$i]['agency_id'])) {
                    $nestedData =   array();
                    $nestedData[]   = $i + 1;
                    $nestedData[]   = 'Đơn vị';
                    $nestedData[]   = stripslashes($rows_all[$i]['name']);
                    $tool = '';
                    if(in_array("agency-edit", $corePrivilegeSlug['op'])) {
                        $tool .= '<a href="' . HOME_URL_LANG . '/agency/agency-edit?id=' . intval($rows_all[$i]['agency_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a>';
                    }
                    $nestedData[] = $tool;
                    $data[] = $nestedData;
                } elseif(isset($rows_all[$i]['user_id'])) {
                    $nestedData =   array();
                    $nestedData[]   = $i + 1;
                    $nestedData[]   = 'Nhân sự';
                    $nestedData[]   = stripslashes($rows_all[$i]['full_name']);
                    $tool = '';
                    if(in_array("user-edit", $corePrivilegeSlug['op'])) {
                        $tool .= '<a href="' . HOME_URL_LANG . '/account/user-edit?id=' . intval($rows_all[$i]['user_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a>';
                    }
                    $nestedData[] = $tool;
                    $data[] = $nestedData;
                }
            }
        }

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);