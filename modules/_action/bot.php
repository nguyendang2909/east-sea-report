<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0 && isset($_GET['type'])) {
  $type   = isset($_GET['type']) ? $_GET['type'] : '-no-';
  $date   = new DateClass();

  if ($type == 'load') {
    $requestData = $_REQUEST;
    $columns = array(
      0 => 'a.`botId`',
      1 => 'a.`name`',
      2 => 'a.`address`',
      3 => 'c.`title`',
      4 => 'a.`investor`',
      5 => 'a.`transferStatus`',
      6 => 'a.`isWarning`',
    );

    $query = "a.`isActive` = 1 AND b.`is_active` = 1";

    // Tim
    if (!empty($requestData['search']['value'])) {
      $query .= " AND CONCAT(a.`name`, a.`address`, a.`investor`, a.`transferStatus`, a.`note`) LIKE '%" . trim($db->clearText($requestData['search']['value'])) . "%'";
    }
    // Tim ten
    if (!empty($requestData['columns'][1]['search']['value'])) {
      $query .= " AND a.`name` LIKE '%" . trim($db->clearText($requestData['columns'][1]['search']['value'])) . "%'";
    }
    // Tim address
    if (!empty($requestData['columns'][2]['search']['value'])) {
      $query .= " AND a.`address` LIKE '%" . trim($db->clearText($requestData['columns'][2]['search']['value'])) . "%'";
    }
    // Tim local
    if (!empty($requestData['columns'][3]['search']['value'])) {
      $query .= " AND c.`title` LIKE '%" . trim($db->clearText($requestData['columns'][3]['search']['value'])) . "%'";
    }
    // Tim investor
    if (!empty($requestData['columns'][4]['search']['value'])) {
      $query .= " AND a.`investor` LIKE '%" . trim($db->clearText($requestData['columns'][4]['search']['value'])) . "%'";
    }
    // Tim transfer status
    if (!empty($requestData['columns'][5]['search']['value'])) {
      $query .= " AND a.`transferStatus` LIKE '%" . trim($db->clearText($requestData['columns'][5]['search']['value'])) . "%'";
    }

    // Tim kiem va Count
    $db->table = "bots";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`
    LEFT JOIN `" . TTH_DATA_PREFIX . "local` c ON a.`localId` = c.`local_id`";
    $db->condition = $query;
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select("COUNT(*) AS `count`");
    $totalData = $db->RowCount;
    foreach ($rows as $row) {
      $totalData = $row['count'];
    }
    $totalFiltered = $totalData;

    // Danh sach record
    $data = array();
    $db->table = "bots";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`
                   LEFT JOIN `" . TTH_DATA_PREFIX . "local` c ON a.`localId` = c.`local_id`";
    $db->condition = $query;
    $db->order = $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
    // $db->order = '';
    $db->limit = $requestData['start'] . " ," . $requestData['length'];
    $rows = $db->select("a.`botId`, a.`name`, a.`address`, a.`investor`, a.`transferStatus`, a.`isWarning`, a.`file`, a.`note`, c.`title` AS `local`");

    $i = $requestData['start'];
    foreach ($rows as $row) {
      $i++;

      $file = '';
      if ($row['file'] !=  '' && file_exists(ROOT_DIR . DS . 'uploads' . DS . 'tracking' . DS . $row['file'])) {
        $file = '<a target="_blank" href="' . HOME_URL . '/uploads/tracking/' . stripslashes($row['file']) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống">
          <i class="fa fa-paperclip fa-lg"></i>
        </a>';
      }

      $nestedData =   array();
      $nestedData['no'] = $i;
      $nestedData['name'] = stripslashes($row['name']);
      $nestedData['address'] = stripslashes($row['address']);
      $nestedData['local'] = stripslashes($row['local']);
      $nestedData['investor'] = stripslashes($row['investor']);
      $nestedData['transferStatus'] = stripslashes($row['transferStatus']);
      $nestedData['isWarning'] = getWarning(stripslashes($row['isWarning']));
      $nestedData['note'] = stripslashes($row['note']);
      $nestedData['file'] = $file;

      // Show/hide nut chinh sua, xoa record
      $tool = '';
      if (in_array("bot-edit", $corePrivilegeSlug['op'])) {
        $tool .= '<a href="' . HOME_URL_LANG . '/tracking/bot-edit?id=' . intval($row['botId']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
      }

      if (in_array("bot;delete", $corePrivilegeSlug['op'])) {
        $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['botId']) . '"></label>';
      }
      $nestedData['tool'] = $tool;

      $data[] = $nestedData;
    }

    $json_data = array(
      "draw"            => intval($requestData['draw']),
      "recordsTotal"    => intval($totalData),
      "recordsFiltered" => intval($totalFiltered),
      "data"            => $data
    );

    echo json_encode($json_data);
  }
} else echo json_encode(false);
