<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0) {
  $type   = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
  if ($type == 'load') {
    $features = array();

    $db->table = 'reactive';
    $db->join = 'a LEFT JOIN olala3w_maps_type c ON a.`mapTypeId` = c.`maps_type_id`';
    $db->condition = 'a.`isActive` = 1  AND a.`latitude` != "" AND a.`longitude` != ""';
    $db->order = '';
    $db->limit = '';
    $rows = $db->select('
    a.`reactiveId`,
    a.`name`,
    a.`hometown`,
    a.`address`,
    a.`birthdayYear`,
    a.`religion`,
    a.`fbTargetId`,
    a.`contact`,
    a.`isWarning`,
    a.`latitude`,
    a.`longitude`,
    a.`note`,
    c.`icon`,
    c.`color`
    ');

    if ($db->RowCount > 0) {
      foreach ($rows as $row) {
        $coordinates = array();

        $data = convertToGeometry($row['latitude'], $row['longitude']);
        $databyradius  = array($row['latitude'], $row['longitude']);
        $ftype = "Point";

        if (count($data) == 2 && $ftype == 'Point') {
          $coordinates = $data;
          $coordinatesbyradius = $databyradius;
        }

        $geometry = array(
          'type'          => $ftype,
          'coordinates'   => $coordinates,
          'coordinatesbyradius'   => $coordinatesbyradius
        );

        $fbTargetsId = json_decode($row['fbTargetId']);
        $fbReports = array();

        foreach ($fbTargetsId as $fbTargetId) {
          $db->table = 'fb_target';
          $db->join = 'a LEFT JOIN olala3w_fb_report b ON a.`fbTargetId` = b.`fbTargetId`';
          $db->condition = 'a.`isActive` = 1 AND a.`fbTargetId` = ' . $fbTargetId;
          $db->order = '';
          $db->limit = 1;
          $fbReportRow = $db->select('
            a.`fbTargetId`,
            a.`name`,
            a.`type`,
            a.`link`,
            b.`fbReportId`,
            b.`content`,
            b.`react`,
            b.`share`,
            b.`comment`,
            b.`dateTime`,
            b.`image`,
            b.`link`
          ');
          if ($db->RowCount > 0) array_push($fbReports, $fbReportRow);
        }

        $properties = array(
          'id' => $row['reactiveId'],
          'type' => 'reactive',
          'name' => $row['name'],
          'hometown' => $row['name'],
          'address' => $row['address'],
          'birthdayYear' => $row['birthdayYear'],
          'religion' => $row['religion'],
          'contact' => $row['contact'],
          'isWarning' => $row['isWarning'],
          'icon' => $row['icon'],
          'color' => $row['color'],
          'coordinate'         => $coordinates,
          'coordinatebyradius'        => $coordinatesbyradius,
          'fb' => $fbReports,
        );

        $child = array(
          'type'          => 'Feature',
          'geometry'      => $geometry,
          'properties'    => $properties
        );
        $features[] = $child;
      }
    }

    $geo = array(
      'type'      => 'FeatureCollection',
      'features'  => $features
    );
  }
  echo json_encode($geo);
} else echo json_encode(false);
