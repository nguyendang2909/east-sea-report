<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0 && isset($_GET['type'])) {
  $type   = isset($_GET['type']) ? $_GET['type'] : '-no-';
  $date   = new DateClass();

  if ($type == 'load') {
    $requestData = $_REQUEST;
    $columns = array(
      0 => 'a.`name`',
      1 => 'a.`name`',
      2 => 'c.`title`',
      3 => 'a.`carrier`',
      4 => 'a.`employeeCount`',
      5 => 'a.`area`',
      6 => 'a.`investor`',
      7 => 'a.`phoneNumber`',
      8 => 'a.`email`',
      9 => 'a.`website`',
      10 => 'a.`name`',
      11 => 'a.`name`',
    );

    $query = "a.`isActive` = 1";

    // Tim
    if (!empty($requestData['search']['value'])) {
      $query .= " AND CONCAT(a.`name`, a.`carrier`, a.`employeeCount`, a.`area`, a.`investor`, a.`phoneNumber`, a.`email`, a.`website`,  a.`note`) LIKE '%" . trim($db->clearText($requestData['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][1]['search']['value'])) {
      $query .= " AND " . $columns[1] . " LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
    }

    if (!empty($requestData['columns'][2]['search']['value'])) {
      $query .= " AND " . $columns[2] . " LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
    }

    if (!empty($requestData['columns'][3]['search']['value'])) {
      $query .= " AND " . $columns[3] . " LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
    }

    if (!empty($requestData['columns'][4]['search']['value'])) {
      $query .= " AND " . $columns[4] . " LIKE '%" . $db->clearText($requestData['columns'][4]['search']['value']) . "%'";
    }

    if (!empty($requestData['columns'][5]['search']['value'])) {
      $query .= " AND " . $columns[5] . " LIKE '%" . $db->clearText($requestData['columns'][5]['search']['value']) . "%'";
    }

    if (!empty($requestData['columns'][6]['search']['value'])) {
      $query .= " AND " . $columns[6] . " LIKE '%" . $db->clearText($requestData['columns'][6]['search']['value']) . "%'";
    }
    // Tim SDT
    if (!empty($requestData['columns'][7]['search']['value'])) {
      $query .= " AND " . $columns[7] . " LIKE '%" . $db->clearText($requestData['columns'][7]['search']['value']) . "%'";
    }
    // Tim email
    if (!empty($requestData['columns'][8]['search']['value'])) {
      $query .= " AND " . $columns[8] . " LIKE '%" . $db->clearText($requestData['columns'][8]['search']['value']) . "%'";
    }
    // Tim website
    if (!empty($requestData['columns'][9]['search']['value'])) {
      $query .= " AND " . $columns[9] . " LIKE '%" . $db->clearText($requestData['columns'][9]['search']['value']) . "%'";
    }

    // Tim kiem va Count
    $db->table = "industrial_zone";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`
                   LEFT JOIN `" . TTH_DATA_PREFIX . "local` c ON a.`localId` = c.`local_id`";
    $db->condition = $query;
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select("COUNT(*) AS `count`");
    $totalData = $db->RowCount;
    foreach ($rows as $row) {
      $totalData = $row['count'];
    }
    $totalFiltered = $totalData;

    // Danh sach record
    $data = array();
    $db->table = "industrial_zone";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`
                   LEFT JOIN `" . TTH_DATA_PREFIX . "local` c ON a.`localId` = c.`local_id`";
    $db->order = $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
    $db->limit = $requestData['start'] . " ," . $requestData['length'];
    $db->limit = '';
    $rows = $db->select("
      a.`industrialZoneId`,
      a.`name`,
      a.`address`,
      a.`area`,
      a.`carrier`,
      a.`employeeCount`,
      a.`yearOfEstablishment`,
      a.`buildingDensity`,
      a.`investor`,
      a.`phoneNumber`,
      a.`email`,
      a.`website`,
      a.`isWarning`,
      a.`file`,
      a.`note`,
      c.`title` AS `local`");

    $i = $requestData['start'];
    foreach ($rows as $row) {
      $i++;

      $file = '';
      if ($row['file'] !=  '' && file_exists(ROOT_DIR . DS . 'uploads' . DS . 'tracking' . DS . $row['file'])) {
        $file = '<a target="_blank" href="' . HOME_URL . '/uploads/tracking/' . stripslashes($row['file']) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống">
          <i class="fa fa-paperclip fa-lg"></i>
        </a>';
      }

      $nestedData =   array();
      $nestedData['no'] = $i;
      $nestedData['name'] = stripslashes($row['name']);
      $nestedData['local'] = stripslashes($row['local']);
      $nestedData['area'] = stripslashes($row['area']) == 0 ? '' : stripslashes($row['area']);
      $nestedData['carrier'] = stripslashes($row['carrier']);
      $nestedData['employeeCount'] = stripslashes($row['employeeCount']);
      $nestedData['yearOfEstablishment'] = stripslashes($row['yearOfEstablishment']) == 0 ? '' : stripslashes($row['yearOfEstablishment']);
      $nestedData['buildingDensity'] = stripslashes($row['buildingDensity']);
      $nestedData['investor'] = stripslashes($row['investor']);
      $nestedData['phoneNumber'] = stripslashes($row['phoneNumber']);
      $nestedData['email'] = stripslashes($row['email']);
      $nestedData['website'] = stripslashes($row['website']);
      $nestedData['isWarning'] = getWarning(stripslashes($row['isWarning']));
      $nestedData['note'] = stripslashes($row['note']);
      $nestedData['file'] = $file;

      // Show/hide nut chinh sua, xoa record
      $tool = '';
      if (in_array("industrial-zone-edit", $corePrivilegeSlug['op'])) {
        $tool .= '<a href="' . HOME_URL_LANG . '/tracking/industrial-zone-edit?id=' . intval($row['industrialZoneId']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
      }

      if (in_array("industrial-zone;delete", $corePrivilegeSlug['op'])) {
        $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['industrialZoneId']) . '"></label>';
      }
      $nestedData['tool'] = $tool;

      $data[] = $nestedData;
    }

    $json_data = array(
      "draw"            => intval($requestData['draw']),
      "recordsTotal"    => intval($totalData),
      "recordsFiltered" => intval($totalFiltered),
      "data"            => $data
    );

    echo json_encode($json_data);
  }
} else echo json_encode(false);
