<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type       = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date       = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => '`' . TTH_DATA_PREFIX . 'blog_post`.`created_time`',
			1 => '`' . TTH_DATA_PREFIX . 'blog`.`name`',
            2 => '`' . TTH_DATA_PREFIX . 'blog_post`.`img`',
            3 => '`' . TTH_DATA_PREFIX . 'blog_post`.`name`',
            4 => '`' . TTH_DATA_PREFIX . 'blog_post`.`hot`',
            5 => '`' . TTH_DATA_PREFIX . 'blog_post`.`created_time`',
            6 => '`' . TTH_DATA_PREFIX . 'core_user`.`full_name`'
		);

		$query = "`" . TTH_DATA_PREFIX . "blog_post`.`is_active` = 1 AND `" . TTH_DATA_PREFIX . "blog`.`is_active` = 1";

		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(`" . TTH_DATA_PREFIX . "blog`.`name`, `" . TTH_DATA_PREFIX . "blog_post`.`name`, `" . TTH_DATA_PREFIX . "core_user`.`full_name`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}

		if( !empty($requestData['columns'][1]['search']['value']) ) {
			$query .= " AND `" . TTH_DATA_PREFIX . "blog`.`name` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][2]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "blog_post`.`img` LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][3]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "blog_post`.`name` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][4]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "blog_post`.`hot` = " . (intval($requestData['columns'][4]['search']['value'])-1);
        }
        if (!empty($requestData['columns'][5]['search']['value'])) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][5]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND `" . TTH_DATA_PREFIX . "blog_post`.`created_time` >= $d1 AND `" . TTH_DATA_PREFIX . "blog_post`.`created_time` <= $d2";
        }
        if( !empty($requestData['columns'][6]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "core_user`.`full_name` LIKE '%" . $db->clearText($requestData['columns'][6]['search']['value']) . "%'";
        }

		$db->table = "blog_post";
        $db->join = "LEFT JOIN `" . TTH_DATA_PREFIX . "blog` ON `" . TTH_DATA_PREFIX . "blog`.`blog_id` = `" . TTH_DATA_PREFIX . "blog_post`.`blog` LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` ON `" . TTH_DATA_PREFIX . "core_user`.`user_id` = `" . TTH_DATA_PREFIX . "blog_post`.`user_id`";
        $db->condition = $query;
		$db->order = "";
        $db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach($rows as $row) {
            $totalData = $row['count'];
        }
        $totalFiltered = $totalData;

		$db->table = "blog_post";
        $db->join = "LEFT JOIN `" . TTH_DATA_PREFIX . "blog` ON `" . TTH_DATA_PREFIX . "blog`.`blog_id` = `" . TTH_DATA_PREFIX . "blog_post`.`blog` LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` ON `" . TTH_DATA_PREFIX . "core_user`.`user_id` = `" . TTH_DATA_PREFIX . "blog_post`.`user_id`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("`blog_post_id`, `" . TTH_DATA_PREFIX . "blog`.`name` AS `name1`, `" . TTH_DATA_PREFIX . "blog_post`.`img` AS `img`, `" . TTH_DATA_PREFIX . "blog_post`.`name` AS `name2`, `" . TTH_DATA_PREFIX . "blog_post`.`hot` AS `hot`, `" . TTH_DATA_PREFIX . "blog_post`.`created_time` AS `created_time`, `" . TTH_DATA_PREFIX . "blog_post`.`user_id` AS `user_id`, `" . TTH_DATA_PREFIX . "core_user`.`full_name` AS `full_name`");
		$data = array();
		$i = $requestData['start'];
		foreach($rows as $row) {
			$i++;
			$nestedData =   array();

			$img = '';
			if($row['img']=='-no-') $img = '<img data-toggle="tooltip" data-placement="top" title="Không có hình" src="/images/error.png">';
			else $img = '<img class="btn-popover" data-img="' . HOME_URL_LANG . '/uploads/blog/' . stripslashes($row['img']) . '" title="' . stripslashes($row['name2']) . '" src="/images/OK.png">';
            $active = $hot = '';
            if(in_array("blog-post-edit", $corePrivilegeSlug['op'])) {
                $hot = (intval($row['hot']) == 0) ?
                    '<div class="btn-event-close" data-toggle="tooltip" data-placement="top" title="Mở" onclick="edit_status($(this), ' . intval($row["blog_post_id"]) . ', \'hot\', \'blog_post\');" rel="1">0</div>'
                    :
                    '<div class="btn-event-open" data-toggle="tooltip" data-placement="top" title="Đóng" onclick="edit_status($(this), ' . intval($row["blog_post_id"]) . ', \'hot\', \'blog_post\');" rel="0">1</div>';
            } else {
                $hot = (intval($row['hot']) == 0) ?
                    '<div class="btn-event-close ol-alert-core" data-toggle="tooltip" data-placement="top" title="Mở" rel="1">0</div>'
                    :
                    '<div class="btn-event-open ol-alert-core" data-toggle="tooltip" data-placement="top" title="Đóng" rel="0">1</div>';
            }

			$nestedData[] = $i;
			$nestedData[] = stripslashes($row['name1']);
            $nestedData[] = $img;
            $nestedData[] = stripslashes($row['name2']);
            $nestedData[] = $hot;
            $nestedData[] = $date->vnDateTime($row['created_time']);
            $nestedData[] = stripslashes($row['full_name']);
            $nestedData[] = '<a href="' . HOME_URL_LANG . '/blog/blog-post-edit?id=' . intval($row['blog_post_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; <label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Chọn xoá" class="ol-checkbox-js" name="tick[]" value="' . intval($row['blog_post_id']) . '"></label>';

			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);