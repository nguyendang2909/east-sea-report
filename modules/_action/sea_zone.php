<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0 && isset($_GET['type'])) {
  $type   = isset($_GET['type']) ? $_GET['type'] : '-no-';
  $date   = new DateClass();

  if ($type == 'load') {
    $requestData = $_REQUEST;
    $columns = array(
      0 => '`zoneId`',
      1 => '`name`',
    );

    $query = "`isActive` = 1";

    // Tim
    if (!empty($requestData['search']['value'])) {
      $query .= " AND CONCAT(`name`) LIKE '%" . trim($db->clearText($requestData['search']['value'])) . "%'";
    }
    // Tim ten
    if (!empty($requestData['columns'][1]['search']['value'])) {
      $query .= " AND `name` LIKE '%" . trim($db->clearText($requestData['columns'][1]['search']['value'])) . "%'";
    }

    // Tim kiem va Count
    $db->table = "sea_zone";
    $db->join = "";
    $db->condition = $query;
    $db->order = "";
    $db->limit = "";
    $rows = $db->select("COUNT(*) AS `count`");
    $totalData = $db->RowCount;
    foreach ($rows as $row) {
      $totalData = $row['count'];
    }
    $totalFiltered = $totalData;

    // Danh sach record
    $data = array();
    $db->table = "sea_zone";
    $db->join = "";
    $db->condition = $query;
    $db->order = $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
    // $db->order = '';
    $db->limit = $requestData['start'] . " ," . $requestData['length'];
    $rows = $db->select("`name`");

    $i = 0;
    foreach ($rows as $row) {
      $i++;


      $nestedData =   array();
      $nestedData['no'] = $i;
      $nestedData['name'] = stripslashes($row['name']);

      // Show/hide nut chinh sua, xoa record
      $tool = '';
      if (in_array("sea-zone-edit", $corePrivilegeSlug['op'])) {
        $tool .= '<a href="' . HOME_URL_LANG . '/tracking/sea-zone-edit?id=' . intval($row['botId']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
      }

      if (in_array("sea-zone;delete", $corePrivilegeSlug['op'])) {
        $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['zoneId']) . '"></label>';
      }
      $nestedData['tool'] = $tool;

      $data[] = $nestedData;
    }

    $json_data = array(
      "draw"            => intval($requestData['draw']),
      "recordsTotal"    => intval($totalData),
      "recordsFiltered" => intval($totalFiltered),
      "data"            => $data
    );

    echo json_encode($json_data);
  }
} else echo json_encode(false);
