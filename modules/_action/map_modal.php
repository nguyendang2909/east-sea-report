<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//

if($account["id"]>0) {
    $type       = isset($_POST['type']) ? trim($_POST['type']) : '-no-';
    $f_type     = isset($_POST['f_type']) ? trim($_POST['f_type']) : '';
	$url       = isset($_POST['url']) ? trim($_POST['url']) : '-no-';
    $f_latlong  = isset($_POST['f_latlong']) ? trim($_POST['f_latlong']) : '';
    $result = '';
    if($type=='add' && in_array("maps-gis-add", $corePrivilegeSlug['op'])) {
        $result .= '<div class="modal-dialog"><div class="modal-content">';
		$result .= '<form id="_maps" class="form-ol-3w" method="post">';
		$result .= '<input type="hidden" name="f_type" value="' . $f_type . '">';
		$result .= '<input type="hidden" name="url" value="' . $url . '">';
		$result .= '<input type="hidden" name="f_coordinates" value="' . htmlspecialchars($f_latlong) . '">';
		$result .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Địa điểm</h4></div>';
		$result .= '<div class="modal-body"><div class="form-responsive report-top"><table class="table table-no-border table-hover">';
		$result .= '<tr><td width="80px" align="right"><label class="form-lb-tp">Tên vị trí:</label></td><td><input class="form-control" type="text" name="title" value="" maxlength="250" required></td></tr>';
		$result .= '<tr><td align="right"><label class="form-lb-tp">Loại vị trí:</label></td><td>' . selectMapsType(0, 'required') . '<label for="type" class="error"></label></td></tr>';
		$result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td><td><textarea class="form-control" rows="3" name="note"></textarea></td></tr>';
		$result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Trạng thái</label></td><td><label><input type="checkbox" name="warning" value="warning" >Cảnh báo</label>   <input type="text" class="form-control" name="warningnote"></td></tr>';
		$result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Đính kèm:</label></td><td><textarea disabled=\'disabled\' class="form-control" name="attached"></textarea></td></tr>';
		$result .= '</table></div></div>';
		$result .= '<div class="modal-footer"><input id="fileupload" type="file" name="file" oninput="document.getElementsByName(\'attached\')[0].innerText = document.getElementById(\'fileupload\').value;" style="display:none"/><button type="button" class="btn btn-primary btn-round" name="upload" onclick="document.getElementById(\'fileupload\').click();">Đính kèm</button><button type="submit" class="btn btn-primary btn-round" name="add">Thêm địa điểm</button><button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Thoát</button></div>';
		$result .= ' </form>';
		$result .= '</div></div>';
    }
	else if($type == 'loadMapType'){
		$choice = 0;
		$required = '';
		$result = '<select id="map-type-picker" class="" name="m_type" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn loại vị trí..." ' . $required . '>';
		
		$db->table = "maps_type";
		$db->condition = "`is_active` = 1";
		$db->order = "`title` ASC";
		$db->limit = "";
		$rows = $db->select("`maps_type_id`, `icon`, `title`");
		if($db->RowCount>0) {
			foreach($rows as $row) {
				$icon = '';
				if($row['icon']=='-no-' || $row['icon']=='' ) {
					$icon = HOME_URL_LANG . '/images/icon.png';
				} else {
					$icon = HOME_URL_LANG . '/uploads/maps/' . stripslashes($row['icon']);
				}
				$icon = ' data-thumbnail="' . $icon . '"';

				$selected = '';
				if ($row["maps_type_id"] == $choice) $selected = ' selected';
				$result .= '<option' . $icon . ' value="' . $row["maps_type_id"] . '"' . $selected . '>' . stripslashes($row["title"]) . '</option>';
			}
		}

		$result .= '</select>';
	}
	
    echo $result;
	
} else echo '-Error-';

function option(){
	return '<option value="nong">Nóng</option>'.
	'<option value="vuaphai">Vừa phải</option>'.
	'<option value="binhthuong">Bình thường</option>';
	
}
function selectMapsType($choice, $required = '') {
    global $db;

    $result = '<select class="form-control selectpicker" name="m_type" style="float:left" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn loại vị trí..." ' . $required . '>';

    $db->table = "maps_type";
    $db->condition = "`is_active` = 1";
    $db->order = "`title` ASC";
    $db->limit = "";
    $rows = $db->select("`maps_type_id`, `icon`, `title`");
    if($db->RowCount>0) {
        foreach($rows as $row) {
            $icon = '';
            if($row['icon']=='-no-' || $row['icon']=='' ) {
                $icon = HOME_URL_LANG . '/images/icon.png';
            } else {
                $icon = HOME_URL_LANG . '/uploads/maps/' . stripslashes($row['icon']);
            }
            $icon = ' data-thumbnail="' . $icon . '"';

            $selected = '';
            if ($row["maps_type_id"] == $choice) $selected = ' selected';
            $result .= '<option' . $icon . ' value="' . $row["maps_type_id"] . '"' . $selected . '>' . stripslashes($row["title"]) . '</option>';
        }
    }

    $result .= '</select>';
    return $result;
}
