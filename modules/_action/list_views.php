<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if(isset($_POST['id'])) {
	$id     = intval($_POST['id']);
	$list   = array();

	$db->table = "views";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`user_id` = b.`user_id`";
    $db->condition = "a.`type` LIKE 'blog' AND a.`id` = $id";
    $db->order = "a.`created_time` DESC";
    $db->limit = "";
	$rows = $db->select("a.`user_id`, b.`full_name`");
    foreach ($rows as $row) {
        array_push($list, stripslashes($row['full_name']));
    }

    echo implode(', ', $list);

} else echo '-Error-';