<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type       = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $software   = isset($_POST['software']) ? intval($_POST['software']) : 0;
    $date       = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => 'a.`software_item_id`',
			1 => 'b.`title`',
            2 => 'a.`title`',
            3 => 'a.`perform`',
            4 => 'a.`owner`',
            5 => 'd.`name`',
            6 => 'e.`full_name`',
            7 => 'a.`files`',
            8 => 'a.`modified_time`',
            9 => 'c.`full_name`'
		);

		$query = "a.`is_active` = 1 AND b.`is_active` = 1";
        if($software>0) {
            $query = "a.`software` = $software";
        }

		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(b.`title`, a.`title`, a.`perform`, a.`owner`, d.`name`, e.`full_name`, a.`files`, c.`full_name`, a.`history`, a.`note`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][1]['search']['value']) ) {
            $query .= " AND b.`title` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][2]['search']['value']) ) {
            $query .= " AND a.`title` LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][3]['search']['value']) ) {
            $query .= " AND a.`perform` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][4]['search']['value']) ) {
            $query .= " AND a.`owner` LIKE '%" . $db->clearText($requestData['columns'][4]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][5]['search']['value']) ) {
            $query .= " AND d.`name` LIKE '%" . $db->clearText($requestData['columns'][5]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][6]['search']['value']) ) {
            $query .= " AND e.`full_name` LIKE '%" . $db->clearText($requestData['columns'][6]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][7]['search']['value']) ) {
            $query .= " AND a.`files` LIKE '%" . $db->clearText($requestData['columns'][7]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][8]['search']['value']) ) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][8]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND a.`modified_time` >= $d1 AND a.`modified_time` <= $d2";
        }
        if( !empty($requestData['columns'][9]['search']['value']) ) {
            $query .= " AND c.`full_name` LIKE '%" . $db->clearText($requestData['columns'][9]['search']['value']) . "%'";
        }

        $db->table = "software_item";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "software` b ON b.`software_id` = a.`software` LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` c ON c.`user_id` = a.`user_id` LEFT JOIN `" . TTH_DATA_PREFIX . "agency` d ON d.`agency_id` = a.`agency` LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` e ON e.`user_id` = a.`user`";
        $db->condition = $query;
		$db->order = "";
        $db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach($rows as $row) {
            $totalData = $row['count'];
        }
        $totalFiltered = $totalData;
		
        $data = array();
        //---
		$db->table = "software_item";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "software` b ON b.`software_id` = a.`software` LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` c ON c.`user_id` = a.`user_id` LEFT JOIN `" . TTH_DATA_PREFIX . "agency` d ON d.`agency_id` = a.`agency` LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` e ON e.`user_id` = a.`user`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("a.`software_item_id`, b.`title` AS `title1`, a.`title` AS `title2`, a.`perform`, a.`owner`, d.`name` AS `name1`, e.`full_name` AS `name2`, a.`files`, a.`modified_time`, c.`full_name`");
		$i = $requestData['start'];
		foreach($rows as $row) {
			$i++;
			
			$file = '';
			if(file_exists(ROOT_DIR  . DS . 'uploads' . DS . 'software' . DS . $row['files']) && ($row['files']!='')) {
				$file = '<a target="_blank" href="' . HOME_URL . '/uploads/software/' . stripslashes($row['files']) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống"><i class="fa fa-paperclip fa-lg"></i></a>';
			}
			
			$nestedData =   array();
			$nestedData[] = $row['software_item_id'];
			$nestedData[] = stripslashes($row['title1']);
			$nestedData[] = stripslashes($row['title2']);
			$nestedData[] = stripslashes($row['perform']);
            $nestedData[] = stripslashes($row['owner']);
            $nestedData[] = stripslashes($row['name1']);
            $nestedData[] = stripslashes($row['name2']);
            $nestedData[] = $file;
            $nestedData[] = $date->vnDateTime($row['modified_time']);
            $nestedData[] = stripslashes($row['full_name']);

			$tool = '';
            if(in_array("software-item-edit", $corePrivilegeSlug['op'])) {
                $tool .= '<a href="' . HOME_URL_LANG . '/network/software-item-edit?id=' . intval($row['software_item_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
            }

            if(in_array("software-item;delete", $corePrivilegeSlug['op'])) {
                $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['software_item_id']) . '"></label>';
            }
            $nestedData[] = $tool;

			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);