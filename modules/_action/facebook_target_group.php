<?php
if (!defined('TTH_SYSTEM')) {
    die('Please stop!');
}

//

if ($account["id"] > 0 && isset($_POST['type'])) {
    $type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date   = new DateClass();
    //
    if ($type == 'load') {
        $requestData = $_REQUEST;
        $columns = array(
            0 => 'a.`name`',
            1 => 'a.`name`',
            2 => 'a.`age`',
            3 => 'a.`address`',
            4 => 'a.`religion`',
            5 => 'a.`faction`',
        );

        $query = "a.`is_active` = 1";

        if (!empty($requestData['search']['value'])) {
            $query .= " AND CONCAT(a.`name`, a.`age`, a.`address`, a.`religion`, a.`faction`, a.`social`, a.`note`, b.`full_name`, a.`facebook_type`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
        }
        if (!empty($requestData['columns'][1]['search']['value'])) {
            $query .= " AND a.`name` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
        }
        if (!empty($requestData['columns'][2]['search']['value'])) {
            $query .= " AND a.`age` = " . formatNumberToInt($requestData['columns'][2]['search']['value']);
        }
        if (!empty($requestData['columns'][3]['search']['value'])) {
            $query .= " AND a.`address` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
        }
        if (!empty($requestData['columns'][4]['search']['value'])) {
            $query .= " AND a.`religion` LIKE '%" . $db->clearText($requestData['columns'][4]['search']['value']) . "%'";
        }
        if (!empty($requestData['columns'][5]['search']['value'])) {
            $query .= " AND a.`faction` LIKE '%" . $db->clearText($requestData['columns'][5]['search']['value']) . "%'";
        }
        if (!empty($requestData['columns'][6]['search']['value'])) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][6]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND a.`modified_time` >= $d1 AND a.`modified_time` <= $d2";
        }
        if (!empty($requestData['columns'][7]['search']['value'])) {
            $query .= " AND b.`full_name` LIKE '%" . $db->clearText($requestData['columns'][7]['search']['value']) . "%'";
        }
        if (!empty($requestData['columns'][8]['search']['value'])) {
            $query .= " AND a.`address` LIKE '%" . $db->clearText($requestData['columns'][8]['search']['value']) . "%'";
        }



        $db->table = "facebook_target_group";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`user_id` = b.`user_id`";
        $db->condition = $query;
        $db->order = "";
        $db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach ($rows as $row) {
            $totalData = $row['count'];
        }
        $totalFiltered = $totalData;

        $data = array();
        //---
        $db->table = "facebook_target_group";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`user_id` = b.`user_id`";
        $db->condition = $query;
        $db->order = $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
        $db->limit = $requestData['start'] . " ," . $requestData['length'];
        $rows = $db->select('
            a.`subject_id`,
            a.`name`,
            a.`faction`,
            b.`full_name`,
            a.`tracking_facebook_target`,
            a.`tracking_status`,
            a.`scan_status`
            ');
        $i = $requestData['start'];
        foreach ($rows as $row) {
            $i++;

            $nestedData             =   array();
            $nestedData['no']         = $i;
            $nestedData['name']     = stripslashes($row['name']);
            $nestedData['age']         = doubleval($row['age']);
            $nestedData['address']  = stripslashes($row['address']);
            $nestedData['religion'] = stripslashes($row['religion']);
            $nestedData['faction']  = stripslashes($row['faction']);
            $nestedData['time']     = $date->vnDateTime($row['modified_time']);
            $nestedData['user']     = stripslashes($row['full_name']);
            $nestedData['facebookType']     = stripslashes($row['facebook_type']);
            $nestedData['social']     = exportDataForm($row);
            $nestedData['tracking_facebook_target']     = listFacebookInFacebookTargetGroup(json_decode($row['tracking_facebook_target']));
            $nestedData['tracking_status']     = trackingGroupStatus(stripslashes($row['tracking_status']), stripslashes($row['scan_status']));

            $tool = '';
            if (in_array("facebook-target-group-edit", $corePrivilegeSlug['op'])) {
                $tool .= '<a href="' . HOME_URL_LANG . '/tracking/facebook-target-group-edit?id=' . intval($row['subject_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
            }

            if (in_array("facebook-target-group;delete", $corePrivilegeSlug['op'])) {
                $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['subject_id']) . '"></label>';
            }

            $nestedData['tool']     = $tool;

            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"            => intval($requestData['draw']),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }
} else echo json_encode(false);

function listFacebookInFacebookTargetGroup($lists)
{
    global $db;
    $result = '<div class="bootstrap-tagsinput">';
    for ($i = 0; $i < count($lists); $i++) {
        $db->table = "fb_target";
        $db->order = "`name` ASC";
        $db->limit = "1";
        $db->condition = "`isActive` = 1 AND `fbTargetId` = $lists[$i]";
        $row = $db->select();
        $facebookName = stripslashes($row[0]['name']);
        $facebookLink = stripslashes($row[0]['link']);
        if ($db->RowCount > 0) {
            $result .= '<a href ="' . $facebookLink . '"><span class="tag label label-info">' . $facebookName . '</span></a>';
            if ($i != count($lists) - 1) $result .= ', ';
        }
    }
    $result .= '</div>';

    return $result;
}

function trackingGroupStatus($trackingStatus, $scanStatus)
{
    if ($trackingStatus == 1) {
        if ($scanStatus == 1) return 'Đang quét';
        return 'Đã chọn quét';
    }

    return 'Dừng quét';
}
function listTrackingFacebookTargetForm(array $choice)
{
    global $db;
    $data = implode("','", $choice);
    $result = '';

    $db->table = "fb_target";
    $db->condition = "`isActive` = 1 AND `fbTargetId` IN ('" . $data . "')";
    $db->order = "`name` ASC";
    $db->limit = "";
    $rows = $db->select();
    if ($db->RowCount > 0) {
        foreach ($rows as $row) {
            $result .= '<option value="' . intval($row["fbTargetId"]) . '">' . stripslashes($row["name"]) . ' (' . $row["link"] . ')</option>';
        }
    }
    return $result;
}

function exportDataForm($row)
{
    $groupTargetId = stripslashes($row['subject_id']);
    $dateTime = new DateTime();
    $dateTime->add(DateInterval::createFromDateString('yesterday'));
    $yesterday = $dateTime->format('Y-m-d\TH:i:s');

    $date   = new DateClass();
    $today = $date->vnOther(time(), 'Y-m-d\TH:i:s');

    $result = '<button type="button" class="btn btn-success btn-round ol-confirm" data-toggle="modal" data-target="#' . $row['subject_id'] . '" data-whatever="@mdo">Tải về</button>

    <div class="modal fade" id="' . $row['subject_id'] . '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Xuất báo cáo nhóm đối tượng ' . stripslashes($row['name']) . '</h5>
            
          </div>
          <div class="modal-body">
            <form action = "/print/facebook-target-group">
            <input type="hidden" id="custId" name="id" value="' . stripslashes($row['subject_id']) . '">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Từ ngày:</label>
                <input class="form-control" type="datetime-local" value="' . $yesterday . '" id="example-datetime-local-input" name="startdate">

                <label for="message-text" class="col-form-label">Đến ngày:</label>
                <input class="form-control" type="datetime-local" value="' . $today . '" id="example-datetime-local-input" name="enddate">

                <label for="message-text" class="col-form-label">Keywords (ngăn cách nhau bởi dấu , hoặc ;):</label>
                <input class="form-control" type="text" name="keywords">

                <label for="message-text" class="col-form-label">Đối tượng trinh sát</label>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="trackingTargetRadio" id="trackingTargetRadio1' . $groupTargetId . '" value="allTrackingTargets" checked>
                    <label class="form-check-label" for="trackingTargetRadio1' . $groupTargetId . '">Tất cả</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="trackingTargetRadio" id="trackingTargetRadio2' . $groupTargetId . '" value="oneTrackingTarget">
                    <label class="form-check-label" for="trackingTargetRadio2' . $groupTargetId . '">Đối tượng cụ thể</label>
                </div>


                <select class="form-control" name="selectTrackingTarget" id="selectTrackingTarget' . $groupTargetId . '" disabled>
                    ' . listTrackingFacebookTargetForm(json_decode($row['tracking_facebook_target'])) . '
                </select>
              </div>
              <div class="modal-footer">
              <button type="submit" class="btn btn-success btn-round ol-confirm">Xuất báo cáo</button>
            <button type="button" class="btn btn-light btn-round ol-confirm" data-dismiss="modal">Đóng</button>
            
          </div>
              
            </form>
          </div>
          
        </div>
      </div>
    </div>
    <script>
    function disableSelectTrackingTarget' . $groupTargetId . '() {
        document.querySelector("#selectTrackingTarget' . $groupTargetId . '").disabled = true;

    }
    function enableSelectTrackingTarget' . $groupTargetId . '() {
        document.querySelector("#selectTrackingTarget' . $groupTargetId . '").disabled = false;
    }

    $("input[type=radio][id=trackingTargetRadio1' . $groupTargetId . ']").change(function() {
        disableSelectTrackingTarget' . $groupTargetId . '();
    });

    $("input[type=radio][id=trackingTargetRadio2' . $groupTargetId . ']").change(function() {
        enableSelectTrackingTarget' . $groupTargetId . '();
    });
    </script>
    ';
    // <a href ="../print/facebook-target-group?id='. stripslashes($row['subject_id']) . '"><input type="button" class="btn btn-success btn-round ol-confirm" value="Tải về" name="delete"></a>
    return $result;
};
