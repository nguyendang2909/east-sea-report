<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0 && isset($_GET['type'])) {
  $type   = isset($_GET['type']) ? $_GET['type'] : '-no-';
  $date   = new DateClass();

  if ($type == 'load') {
    $requestData = $_REQUEST;
    $columns = array(
      0 => 'a.`time`',
      1 => 'a.`time`',
      2 => 'c.`title`',
      3 => 'd.`name`',
      4 => 'e.`name`',
      5 => 'a.`purpose`',
      6 => 'a.`note`',
      7 => 'c.`title`',
    );

    $query = "a.`isActive` = 1 AND c.`is_active` = 1 AND d.`isActive` = 1 AND e.`isActive` = 1";

    // Tim
    if (!empty($requestData['search']['value'])) {
      $query .= " AND CONCAT(`purpose`) LIKE '%" . trim($db->clearText($requestData['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][2]['search']['value'])) {
      $query .= " AND " . $columns[2] . " LIKE '%" . trim($db->clearText($requestData['columns'][2]['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][3]['search']['value'])) {
      $query .= " AND " . $columns[3] . " LIKE '%" . trim($db->clearText($requestData['columns'][3]['search']['value'])) . "%'";
    }
    if (!empty($requestData['columns'][4]['search']['value'])) {
      $query .= " AND " . $columns[4] . " LIKE '%" . trim($db->clearText($requestData['columns'][4]['search']['value'])) . "%'";
    }
    if (!empty($requestData['columns'][5]['search']['value'])) {
      $query .= " AND " . $columns[5] . " LIKE '%" . trim($db->clearText($requestData['columns'][5]['search']['value'])) . "%'";
    }
    if (!empty($requestData['columns'][6]['search']['value'])) {
      $query .= " AND " . $columns[6] . " LIKE '%" . trim($db->clearText($requestData['columns'][6]['search']['value'])) . "%'";
    }

    // Tim kiem va Count
    $db->table = "field_situation";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`
                   LEFT JOIN `" . TTH_DATA_PREFIX . "local` c ON a.`localId` = c.`local_id`
                   LEFT JOIN `" . TTH_DATA_PREFIX . "country` d ON a.`countryId` = d.`countryId`
                   LEFT JOIN `" . TTH_DATA_PREFIX . "ship` e ON a.`shipId` = e.`shipId`
                   ";
    $db->condition = $query;
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select("COUNT(*) AS `count`");
    $totalData = $db->RowCount;
    foreach ($rows as $row) {
      $totalData = $row['count'];
    }
    $totalFiltered = $totalData;

    // Danh sach record
    $data = array();
    $db->table = "field_situation";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`
                   LEFT JOIN `" . TTH_DATA_PREFIX . "local` c ON a.`localId` = c.`local_id`
                   LEFT JOIN `" . TTH_DATA_PREFIX . "country` d ON a.`countryId` = d.`countryId`
                   LEFT JOIN `" . TTH_DATA_PREFIX . "ship` e ON a.`shipId` = e.`shipId`
                   ";
    $db->condition = $query;
    $db->order = $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
    $db->limit = $requestData['start'] . " ," . $requestData['length'];
    $rows = $db->select("
      a.`fieldSituationId`,
      a.`time`,
      a.`purpose`,
      a.`note`,
      c.`title` as `local`,
      d.`file` AS `countryIcon`,
      d.`name` as `country`,
      e.`name` as `ship`,
      a.`file`
    ");

    $i = $requestData['start'];
    foreach ($rows as $row) {
      $i++;

      $file = '';

      if ($row['file'] !=  '' && file_exists(ROOT_DIR . DS . 'uploads' . DS . 'field-situation' . DS . $row['file'])) {
        $file = '<a target="_blank" href="' . HOME_URL . '/uploads/field-situation/' . stripslashes($row['file']) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống">
          <i class="fa fa-paperclip fa-lg"></i>
        </a>';
      }

      $nestedData =   array();
      $nestedData['no'] = $i;
      $nestedData['time'] = $date->convertYmdTodmY($row['time']);
      $nestedData['local'] = stripslashes($row['local']);
      $nestedData['country'] = $row['countryIcon'] != '' ? '<img src="/uploads/country/' . stripslashes($row['countryIcon']) . '" / style= {width="20px" height="20px"} alt="' . stripslashes($row['country'])  . '" title="' . stripslashes($row['country']) . '">' : '-';
      $nestedData['ship'] = stripslashes($row['ship']);
      $nestedData['purpose'] = stripslashes($row['purpose']);
      $nestedData['note'] = stripslashes($row['note']);
      $nestedData['file'] = $file;

      // Show/hide nut chinh sua, xoa record
      $tool = '';
      if (in_array("field-situation-edit", $corePrivilegeSlug['op'])) {
        $tool .= '<a href="' . HOME_URL_LANG . '/field-situation/field-situation-edit?id=' . intval($row['fieldSituationId']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
      }

      if (in_array("field-situation;delete", $corePrivilegeSlug['op'])) {
        $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['fieldSituationId']) . '"></label>';
      }
      $nestedData['tool'] = $tool;

      $data[] = $nestedData;
    }

    $json_data = array(
      "draw"            => intval($requestData['draw']),
      "recordsTotal"    => intval($totalData),
      "recordsFiltered" => intval($totalFiltered),
      "data"            => $data
    );

    echo json_encode($json_data);
  }
} else echo json_encode(false);
