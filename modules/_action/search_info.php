<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0) {
  $type   = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
  if ($type == 'load') {
    $data = array();

    $db->table = "ship";
    $db->join = "
			a LEFT JOIN
			(SELECT b1.`zone`, b1.`shipUpdateTime`, b1.`shipLatitude`, b1.`shipLongitude`, b1.`shipHeadingCourse`, b1.`shipSpeed`, b1.`isViolateTerritorial`, b1.`shipId`, b1.`comment`, b1.`isActive` FROM olala3w_ship_report b1
				JOIN (SELECT shipId, MAX(updatedAt) AS updatedAt FROM olala3w_ship_report GROUP BY shipId) b2 ON b1.shipId = b2.shipId AND b1.updatedAt = b2.updatedAt) b
			ON a.`shipId` = b.`shipId` LEFT JOIN olala3w_maps_type c ON a.`mapTypeId` = c.`maps_type_id`";
    $db->condition = 'a.`isActive` = 1 AND b.`isActive`=1  AND b.`shipLatitude` != "" AND b.`shipLongitude` != ""';
    $db->order = "`b`.`zone` ASC";
    $db->limit = '';
    $shipRows = $db->select("a.`shipId`, a.`mmsi`, a.`name`");

    foreach($shipRows as $shipRow) {
      $child = array(
        'field' => $shipRow['name'],
        // 'fieldMmsi' => $shipRow['mmsi'],
        'type' => 'ship',
        'id' => $shipRow['shipId']
      );

      $data[] = $child;

      $child = array(
        'field' => $shipRow['mmsi'],
        'type' => 'ship',
        'id' => $shipRow['shipId']
      );

      $data[] = $child;
    }

    $db->table = 'bots';
    $db->condition = '`isActive` = 1 AND `longitude` != "" AND `latitude` != ""';
    $db->order = '';
    $db->limit = '';
    $botRows = $db->select();

    foreach($botRows as $botRow) {
      $child = array(
        'field' => $botRow['name'],
        'type' => 'bot',
        'id' => $botRow['botId'],
      );

      $data[] = $child;
    }

    $db->table = 'parishs';
    $db->condition = '`isActive` = 1 AND `longitude` != "" AND `latitude` != ""';
    $db->order = '';
    $db->limit = '';
    $parishRows = $db->select();

    foreach($parishRows as $parishRow) {
      $child = array(
        'field' => $parishRow['name'],
        'type' => 'parish',
        'id' => $parishRow['parishId'],
      );

      $data[] = $child;
    }

    $db->table = 'websites';
    $db->condition = '`isActive` = 1 AND `longitude` != "" AND `latitude` != ""';
    $db->order = '';
    $db->limit = '';
    $websiteRows = $db->select();

    foreach($websiteRows as $websiteRow) {
      $child = array(
        'field' => $websiteRow['name'],
        'type' => 'website',
        'id' => $websiteRow['websiteId'],
      );

      $data[] = $child;
    }

    $db->table = 'industrial_zone';
    $db->condition = '`isActive` = 1 AND `longitude` != "" AND `latitude` != ""';
    $db->order = '';
    $db->limit = '';
    $industrialRows = $db->select();

    foreach($industrialRows as $industrialRow) {
      $child = array(
        'field' => $industrialRow['name'],
        'type' => 'industrial_zone',
        'id' => $industrialRow['industrialZoneId'],
      );

      $data[] = $child;
    }

    $db->table = 'enterprises';
    $db->condition = '`isActive` = 1 AND `longitude` != "" AND `latitude` != ""';
    $db->order = '';
    $db->limit = '';
    $enterpriseRows = $db->select();

    foreach($enterpriseRows as $enterpriseRow) {
      $child = array(
        'field' => $enterpriseRow['name'],
        'type' => 'enterprise',
        'id' => $enterpriseRow['enterpriseId'],
      );

      $data[] = $child;
    }

    $db->table = 'reactive';
    // $db->join = 'a LEFT JOIN olala3w_maps_type c ON a.`mapTypeId` = c.`maps_type_id`';
    // $db->condition = 'a.`isActive` = 1  AND a.`latitude` != "" AND a.`longitude` != ""';
    $db->condition = '`isActive` = 1 AND `longitude` != "" AND `latitude` != ""';
    $db->order = '';
    $db->limit = '';
    $reactiveRows = $db->select();

    foreach($reactiveRows as $reactiveRow) {
      $child = array(
        'field' => $reactiveRow['name'],
        'type' => 'reactive',
        'id' => $reactiveRow['reactiveId'],
      );

      $data[] = $child;
    }
  }
  echo json_encode($data);
} else echo json_encode(false);
