<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0) {
  $type   = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
  $zoneName   = isset($_REQUEST['zoneName']) ? trim($_REQUEST['zoneName']) : '';
  $lat = isset($_REQUEST['lat']) ? trim($_REQUEST['lat']) : '';
  $long = isset($_REQUEST['long']) ? trim($_REQUEST['long']) : '';
  $zoneId = isset($_REQUEST['zoneId']) ? intval($_REQUEST['zoneId']) : 0;

  if ($type == "insertSeaZone") {

    $db->table = "sea_zone ";
    $data = array(
      'name' => $db->clearText($zoneName),
    );
    $db->insert($data);

    $db->table = "sea_zone";
    $db->condition = "`name` ='". $zoneName . "'";
    $db->order = '';
    $db->limit = '';
    $rows = $db->select('`zoneId`');

    if ($db->RowCount > 0) {
      foreach ($rows as $row) {
        $child = array(
          'zoneId'        => $row['zoneId'],
        );
        $features[] = $child;
      }
    }
    $geo = array(
      'features'  => $features
    );
  } else if($type == "insertSeaZoneReport")
  {
    $db->table = "sea_zone_report";
    $data = array(
      'zoneId' => intval($zoneId),
      'latitude' => $db->clearText($lat),
      'longtitude' => $db->clearText($long),
    );
    $db->insert($data);
  }
  echo json_encode($geo);
} else echo json_encode(false);
