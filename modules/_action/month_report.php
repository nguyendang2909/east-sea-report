<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0 && isset($_GET['type'])) {
  $type   = isset($_GET['type']) ? $_GET['type'] : '-no-';
  $date   = new DateClass();

  if ($type == 'load') {
    $requestData = $_REQUEST;
    $columns = array(
      0 => 'a.`year`',
      1 => 'a.`month`',
      2 => 'a.`name`',
      3 => 'e.`name`',
      4 => 'a.`note`',
    );

    $query = "a.`isActive` = 1";

    // // Tim
    // if (!empty($requestData['search']['value'])) {
    //   $query .= " AND CONCAT(`purpose`) LIKE '%" . trim($db->clearText($requestData['search']['value'])) . "%'";
    // }

    // if (!empty($requestData['columns'][2]['search']['value'])) {
    //   $query .= " AND " . $columns[2] . " LIKE '%" . trim($db->clearText($requestData['columns'][2]['search']['value'])) . "%'";
    // }

    // if (!empty($requestData['columns'][3]['search']['value'])) {
    //   $query .= " AND " . $columns[3] . " LIKE '%" . trim($db->clearText($requestData['columns'][3]['search']['value'])) . "%'";
    // }
    // if (!empty($requestData['columns'][4]['search']['value'])) {
    //   $query .= " AND " . $columns[4] . " LIKE '%" . trim($db->clearText($requestData['columns'][4]['search']['value'])) . "%'";
    // }
    // if (!empty($requestData['columns'][5]['search']['value'])) {
    //   $query .= " AND " . $columns[5] . " LIKE '%" . trim($db->clearText($requestData['columns'][5]['search']['value'])) . "%'";
    // }
    // if (!empty($requestData['columns'][6]['search']['value'])) {
    //   $query .= " AND " . $columns[6] . " LIKE '%" . trim($db->clearText($requestData['columns'][6]['search']['value'])) . "%'";
    // }

    // Tim kiem va Count
    $db->table = "month_report";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`";
    $db->condition = $query;
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select("COUNT(*) AS `count`");
    $totalData = $db->RowCount;
    foreach ($rows as $row) {
      $totalData = $row['count'];
    }
    $totalFiltered = $totalData;

    // Danh sach record
    $data = array();
    $db->table = "month_report";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`";
    $db->condition = $query;
    $db->order = $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
    $db->limit = $requestData['start'] . " ," . $requestData['length'];
    $rows = $db->select("
      a.`monthReportId`,
      a.`year`,
      a.`month`,
      a.`note`,
      a.`isUpdate`
    ");

    $i = $requestData['start'];
    foreach ($rows as $row) {
      $i++;

      $file = '';
      $upload = '';

      $filePath = ROOT_DIR . DS . 'uploads' . DS . 'month-report' . DS . intval($row['year']). '-' . intval($row['month']) . '.docx';

      if (in_array("month-report-edit", $corePrivilegeSlug['op'])) {
        $upload .= '<a href="' . HOME_URL_LANG . '/month-report/month-report-edit?id=' . intval($row['monthReportId']) . '"><i class="fa fa-upload"></i></a> &nbsp; &nbsp;';
      }

      if (file_exists($filePath)) {
        $file = '<a target="_blank" href="' . HOME_URL . '/uploads/month-report/' . intval($row['year']). '-' . intval($row['month']) . '.docx" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống">
          <i class="fa fa-paperclip fa-lg"></i>
          </a>';
        $file .= '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp';
        $file .= '<a target="_blank" href="' . HOME_URL_LANG . ':3000/month-report?month='. intval($row['month']) .'&&year=' . intval($row['year'])  . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống">
          Xuất lại
          </a>';
      } else {
        $file .= '<a target="_blank" href="' . HOME_URL_LANG . ':3000/month-report?month='. intval($row['month']) .'&&year=' . intval($row['year'])  . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống">
          Xuất báo cáo
          </a>';
      }

      $nestedData = array();
      $nestedData['no'] = $i;
      $nestedData['month'] = intval($row['month']) . '/' . intval($row['year']);
      $nestedData['note'] = stripslashes($row['note']);
      $nestedData['file'] = $file;
      $nestedData['upload'] = $upload;


      // Show/hide nut chinh sua, xoa record
      $tool = '';
      // if (in_array("month-report-edit", $corePrivilegeSlug['op'])) {
      //   $tool .= '<a href="' . HOME_URL_LANG . '/month-report/month-report-edit?id=' . intval($row['monthReportId']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
      // }

      if (in_array("month-report;delete", $corePrivilegeSlug['op'])) {
        $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['monthReportId']) . '"></label>';
      }
      $nestedData['tool'] = $tool;

      $data[] = $nestedData;
    } 

    $json_data = array(
      "draw"            => intval($requestData['draw']),
      "recordsTotal"    => intval($totalData),
      "recordsFiltered" => intval($totalFiltered),
      "data"            => $data
    );

    echo json_encode($json_data);
  }
} else echo json_encode(false);
