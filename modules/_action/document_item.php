<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $document 	= isset($_POST['document']) ? intval($_POST['document']) : 0;
    $date   = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => '`' . TTH_DATA_PREFIX . 'document_item`.`created_time`',
			1 => '`' . TTH_DATA_PREFIX . 'document`.`title`',
            2 => '`' . TTH_DATA_PREFIX . 'document_item`.`code`',
            3 => '`' . TTH_DATA_PREFIX . 'document_item`.`title`',
            4 => '`' . TTH_DATA_PREFIX . 'document_item`.`comment`',
            5 => '`' . TTH_DATA_PREFIX . 'document_item`.`file`',
            6 => '`' . TTH_DATA_PREFIX . 'document_item`.`modified_time`',
            7 => '`' . TTH_DATA_PREFIX . 'core_user`.`full_name`'
		);

		$query = "`" . TTH_DATA_PREFIX . "document_item`.`is_active` = 1 AND `" . TTH_DATA_PREFIX . "document`.`is_active` = 1";
        if($document>0) {
            $query = "`" . TTH_DATA_PREFIX . "document_item`.`document` = $document";
        }

		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(`" . TTH_DATA_PREFIX . "document`.`title`, `" . TTH_DATA_PREFIX . "document`.`comment`, `" . TTH_DATA_PREFIX . "document_item`.`content`, `" . TTH_DATA_PREFIX . "document_item`.`name`, `" . TTH_DATA_PREFIX . "document_item`.`store_name`, `" . TTH_DATA_PREFIX . "core_user`.`full_name`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][1]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "document`.`title` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][2]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "document_item`.`code` LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][3]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "document_item`.`title` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][4]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "document_item`.`comment` LIKE '%" . $db->clearText($requestData['columns'][4]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][5]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "document_item`.`file` LIKE '%" . $db->clearText($requestData['columns'][5]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][6]['search']['value']) ) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][6]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND `" . TTH_DATA_PREFIX . "document_item`.`modified_time` >= $d1 AND `" . TTH_DATA_PREFIX . "document_item`.`modified_time` <= $d2";
        }
        if( !empty($requestData['columns'][7]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "core_user`.`full_name` LIKE '%" . $db->clearText($requestData['columns'][7]['search']['value']) . "%'";
        }

		$db->table = "document_item";
        $db->join = "LEFT JOIN `" . TTH_DATA_PREFIX . "document` ON `" . TTH_DATA_PREFIX . "document`.`document_id` = `" . TTH_DATA_PREFIX . "document_item`.`document` LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` ON `" . TTH_DATA_PREFIX . "core_user`.`user_id` = `" . TTH_DATA_PREFIX . "document_item`.`user_id`";
        $db->condition = $query;
		$db->order = "";
		$db->limit = "";
        $rows = $db->select();
		$totalData = $db->RowCount;
		$totalFiltered = $totalData;
		
        $data = array();
        //---
		$db->table = "document_item";
        $db->join = "LEFT JOIN `" . TTH_DATA_PREFIX . "document` ON `" . TTH_DATA_PREFIX . "document`.`document_id` = `" . TTH_DATA_PREFIX . "document_item`.`document` LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` ON `" . TTH_DATA_PREFIX . "core_user`.`user_id` = `" . TTH_DATA_PREFIX . "document_item`.`user_id`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("`document_item_id`, `" . TTH_DATA_PREFIX . "document`.`title` AS `title`, `" . TTH_DATA_PREFIX . "document_item`.`code` AS `code`, `" . TTH_DATA_PREFIX . "document_item`.`title` AS `title2`, `" . TTH_DATA_PREFIX . "document_item`.`comment` AS `comment`, `" . TTH_DATA_PREFIX . "document_item`.`file` AS `file`, `" . TTH_DATA_PREFIX . "document_item`.`modified_time` AS `modified_time`, `" . TTH_DATA_PREFIX . "core_user`.`full_name` AS `full_name`");
		$i = $requestData['start'];
		foreach($rows as $row) {
			$i++;
			
			$file = '';
			if(file_exists(ROOT_DIR  . DS . 'uploads' . DS . 'residence' . DS . $row['file']) && ($row['file']!='')) {
				$file = '<a target="_blank" href="' . HOME_URL . '/uploads/residence/' . stripslashes($row['file']) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống"><i class="fa fa-paperclip fa-lg"></i></a>';
			}
			
			$nestedData =   array();
			$nestedData[] = $i;
			$nestedData[] = stripslashes($row['title']);
			$nestedData[] = stripslashes($row['code']);
            $nestedData[] = stripslashes($row['title2']);
            $nestedData[] = stripslashes($row['comment']);
            $nestedData[] = $file;
            $nestedData[] = $date->vnDate($row['modified_time']);
            $nestedData[] = stripslashes($row['full_name']);

			$nestedData[] = '<a href="/?ol=document&op=document_item_edit&id=' . intval($row['document_item_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="images/edit.png"></a> &nbsp; <label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Chọn xoá" class="ol-checkbox-js" name="tick[]" value="' . intval($row['document_item_id']) . '"></label>';
			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);