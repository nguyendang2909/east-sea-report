<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0 && isset($_POST['type'])) {
  $type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
  $date   = new DateClass();

  if ($type == 'load') {
    $requestData = $_REQUEST;
    $columns = array(
      0 => 'a.`shipWeaponId`',
      1 => 'a.`name`',
      2 => 'a.`order`',
      3 => 'a.`feature`',
      4 => 'a.`weight`',
      5 => 'a.`name`',
      6 => 'a.`updatedAt`',
      7 => 'b.`updatedBy`',
      8 => 'a.`name`',
    );

    // Tim cac active record (chua bi xoa)
    $query = "a.`isActive` = 1 AND b.`is_active` = 1";

    if (!empty($requestData['search']['value'])) {
      $query .= " AND CONCAT(a.`name`, a.`order`, a.`feature`, a.`weight`, a.`note`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
    }

    // Tim ten
    if (!empty($requestData['columns'][1]['search']['value'])) {
      $query .= " AND a.`name` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
    }

    // Tim ten
    if (!empty($requestData['columns'][1]['search']['value'])) {
      $query .= " AND a.`order` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
    }

    // Tim tinh nang
    if (!empty($requestData['columns'][3]['search']['value'])) {
      $query .= " AND a.`feature` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
    }

    // Tim can nang
    if (!empty($requestData['columns'][4]['search']['value'])) {
      $query .= " AND a.`weight` LIKE '%" . $db->clearText($requestData['columns'][4]['search']['value']) . "%'";
    }

    // Tim ngay cap nhat
    if (!empty($requestData['columns'][6]['search']['value'])) {
      $searchDate = $date->dmYtoYmd($requestData['columns'][6]['search']['value']);
      $query .= ' AND DATE(a.`updatedAt`) = DATE("' . $searchDate . '")';
    }

    // Tim nguoi cap nhat
    if (!empty($requestData['columns'][7]['search']['value'])) {
      $query .= " AND b.`full_name` LIKE '%" . $db->clearText($requestData['columns'][7]['search']['value']) . "%'";
    }

    // Tim kiem va Count
    $db->table = "ship_weapon";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`";
    $db->condition = $query;
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select("COUNT(*) AS `count`");
    $totalData = $db->RowCount;
    foreach ($rows as $row) {
      $totalData = $row['count'];
    }
    $totalFiltered = $totalData;

    // Danh sach record
    $data = array();
    $db->table = "ship_weapon";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`";
    $db->condition = $query;
    $db->order = $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
    $db->limit = $requestData['start'] . " ," . $requestData['length'];
    $rows = $db->select("a.`shipWeaponId`, a.`name`, a.`order`, a.`feature`, a.`weight` , a.`file`, a.`note`, a.`updatedAt`, b.`full_name` AS `updatedBy`");

    $i = $requestData['start'];
    foreach ($rows as $row) {
      $i++;

      $file = '';
      if ($row['file'] !=  '' && file_exists(ROOT_DIR . DS . 'uploads' . DS . 'tracking' . DS . $row['file'])) {
        $file = '<a target="_blank" href="' . HOME_URL . '/uploads/tracking/' . stripslashes($row['file']) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống">
          <i class="fa fa-paperclip fa-lg"></i>
        </a>';
      }

      $nestedData =   array();
      $nestedData['no'] = $i;
      $nestedData['name'] = stripslashes($row['name']);
      $nestedData['order'] = intval($row['order']);
      $nestedData['feature'] = stripslashes($row['feature']);
      $nestedData['weight'] = getWeight($row['weight']);
      $nestedData['file'] = $file;
      $nestedData['note'] = stripslashes($row['note']);
      $nestedData['updatedAt'] = $date->convertYmdTodmY($row['updatedAt']);
      $nestedData['updatedBy'] = stripslashes($row['updatedBy']);

      // Show/hide nut chinh sua, xoa record
      $tool = '';
      if (in_array("ship-weapon-edit", $corePrivilegeSlug['op'])) {
        $tool .= '<a href="' . HOME_URL_LANG . '/tracking/ship-weapon-edit?id=' . intval($row['shipWeaponId']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
      }

      if (in_array("ship-weapon;delete", $corePrivilegeSlug['op'])) {
        $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['shipWeaponId']) . '"></label>';
      }
      $nestedData['tool'] = $tool;

      $data[] = $nestedData;
    }

    $json_data = array(
      "draw"            => intval($requestData['draw']),
      "recordsTotal"    => intval($totalData),
      "recordsFiltered" => intval($totalFiltered),
      "data"            => $data
    );

    echo json_encode($json_data);
  }
} else echo json_encode(false);

function getWeight($weight)
{
  if (floatval($weight) == 0) return "-";
  return floatval($weight);
}
