<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0 && isset($_POST['type'])) {
  $type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
  $date   = new DateClass();

  if ($type == 'load') {
    $requestData = $_REQUEST;
    $columns = array(
      0 => 'a.`name`',
      1 => 'a.`name`',
      2 => 'a.`place`',
      3 => 'a.`url`',
      4 => 'a.`ip`',
      5 => 'a.`owner`',
    );

    // Tim cac active record (chua bi xoa)
    $query = "a.`isActive` = 1 AND b.`is_active` = 1";

    // Tim kiem
    if (!empty($requestData['search']['value'])) {
      $query .= " AND CONCAT(a.`name`, a.`url`, a.`ip`, a.`place`, a.`owner`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
    }
    // Tim ten website
    if (!empty($requestData['columns'][1]['search']['value'])) {
      $query .= " AND a.`name` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
    }
    // Tim dia ban
    if (!empty($requestData['columns'][2]['search']['value'])) {
      $query .= " AND a.`place` LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
    }
    // Tim url
    if (!empty($requestData['columns'][3]['search']['value'])) {
      $query .= " AND a.`url` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
    }
    // Tim ip
    if (!empty($requestData['columns'][4]['search']['value'])) {
      $query .= " AND a.`ip` LIKE '%" . $db->clearText($requestData['columns'][4]['search']['value']) . "%'";
    }
    // Tim chu so huu
    if (!empty($requestData['columns'][5]['search']['value'])) {
      $query .= " AND a.`owner` LIKE '%" . $db->clearText($requestData['columns'][5]['search']['value']) . "%'";
    }

    // Tim kiem va Count
    $db->table = "websites";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`";
    $db->condition = $query;
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select("COUNT(*) AS `count`");
    $totalData = $db->RowCount;
    foreach ($rows as $row) {
      $totalData = $row['count'];
    }
    $totalFiltered = $totalData;

    // Danh sach website
    $data = array();
    $db->table = "websites";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`";
    $db->condition = $query;
    $db->order = $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
    $db->limit = $requestData['start'] . " ," . $requestData['length'];
    $rows = $db->select("a.`websiteId`, a.`name`, a.`url`, a.`ip`, a.`place`, a.`mapTypeId`, a.`owner`, a.`isWarning`, a.`file`");

    $i = $requestData['start'];
    foreach ($rows as $row) {
      $i++;

      $file = '';
      if ($row['file'] !=  '' && file_exists(ROOT_DIR . DS . 'uploads' . DS . 'network' . DS . $row['file'])) {
        $file = '<a target="_blank" href="' . HOME_URL . '/uploads/network/' . stripslashes($row['file']) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống">
          <i class="fa fa-paperclip fa-lg"></i>
        </a>';
      }

      $nestedData = array();
      $nestedData['no'] = $i;
      $nestedData['name'] = stripslashes($row['name']);
      $nestedData['url'] = stripslashes($row['url']);
      $nestedData['ip'] = stripslashes($row['ip']);
      $nestedData['place'] = stripslashes($row['place']);
      $nestedData['owner'] = stripslashes($row['owner']);
      $nestedData['mapTypeId'] = stripslashes($row['mapTypeId']);
      $nestedData['isWarning'] = getWarning(stripslashes($row['isWarning']));
      $nestedData['file'] = $file;

      // Show/hide nut chinh sua, xoa record
      $tool = '';
      if (in_array("website-edit", $corePrivilegeSlug['op'])) {
        $tool .= '<a href="' . HOME_URL_LANG . '/network/website-edit?id=' . intval($row['websiteId']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
      }

      if (in_array("website;delete", $corePrivilegeSlug['op'])) {
        $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['websiteId']) . '"></label>';
      }
      $nestedData['tool'] = $tool;

      $data[] = $nestedData;
    }

    $json_data = array(
      "draw"            => intval($requestData['draw']),
      "recordsTotal"    => intval($totalData),
      $typeFunc,
      $websiteId,
      $name,
      $websiteUrl,
      $place,
      $latitude,
      $longitude,
      $mapTypeId,
      "recordsFiltered" => intval($totalFiltered),
      "data"            => $data
    );

    echo json_encode($json_data);
  }
} else echo json_encode(false);
