<?php
include_once(_F_TEMPLATES . DS . "industrial_zone.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . '/industrial-zone' . '">Khu công nghiệp</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">Sửa</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

// Get record to edit from db
$industrialZoneId = isset($_GET['id']) ? intval($_GET['id']) : intval($industrialZoneId);
$db->table      = "industrial_zone";
$db->condition  = "`industrialZoneId` = $industrialZoneId AND `isActive` = 1";
$db->order      = "";
$db->limit      = 1;
$rows           = $db->select();
if ($db->RowCount == 0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['tracking']['link'] . '/industrial-zone');

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'edit') {
  // Validate data
  if (empty($name)) $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
  else {
    $fileMaxSize  = FILE_MAX_SIZE;
    $dirDest       = ROOT_DIR . DS . 'uploads' . DS . 'tracking' . DS;

    $fileName      = 'os' . time() . '_' . md5(microtime(true));
    $fileSize      = $_FILES['file']['size'];
    if ($fileSize > 0) {
      $fileUp = new Upload($_FILES['file']);
      $fileUp->file_max_size = $fileMaxSize;
      $fileUp->file_new_name_body = $fileName;
      $fileUp->Process($dirDest);
      if ($fileUp->processed) {
        $fileName = $fileUp->file_dst_name;
        $OK = true;
      } else {
        $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
      }
    } else {
      $OK = true;
      foreach ($rows as $row) {
        $fileName = stripslashes($row['file']);
      }
    }

    if ($OK) {
      $db->table = "industrial_zone";
      $data = array(
        'name' => $db->clearText($name),
        'address' => $db->clearText($address),
        'localId' => intval($localId),
        'carrier' => $db->clearText($carrier),
        'employeeCount' => $db->clearText($employeeCount),
        'yearOfEstablishment' => intval($yearOfEstablishment),
        'area' => floatval($area),
        'buildingDensity' => floatval($buildingDensity),
        'investor' => $db->clearText($investor),
        'phoneNumber' => $db->clearText($phoneNumber),
        'email' => $db->clearText($email),
        'website' => $db->clearText($website),
        'isWarning' => intval($isWarning),
        'mapTypeId' => intval($mapTypeId),
        'latitude'  => $db->clearText($latitude),
        'longitude' => $db->clearText($longitude),
        'file' => $db->clearText($fileName),
        'note' => $db->clearText($note),
        'updatedAt' => date("Y-m-d H:i:s"),
        'updatedBy' =>  $_SESSION["user_id"],
      );
      $db->condition = "`industrialZoneId` = $industrialZoneId";
      $db->update($data);

      loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . '/industrial-zone');
    }

    $OK = true;
  }
} else {
  foreach ($rows as $row) {
    $name = $row['name'];
    $address = $row['address'];
    $localId = $row['localId'];
    $carrier = $row['carrier'];
    $employeeCount = $row['employeeCount'];
    $yearOfEstablishment = $row['yearOfEstablishment'];
    $area = $row['area'];
    $buildingDensity = $row['buildingDensity'];
    $investor = $row['investor'];
    $phoneNumber = $row['phoneNumber'];
    $email = $row['email'];
    $website = $row['website'];
    $isWarning = $row['isWarning'];
    $mapTypeId = $row['mapTypeId'];
    $latitude = $row['latitude'];
    $longitude = $row['longitude'];
    $file = $row['file'];
    $note = $row['note'];
  }
}
if (!$OK) postIndustrialZone(
  HOME_URL_LANG . $mmenu['tracking']['link'] . '/industrial-zone-edit',
  "edit",
  $industrialZoneId,
  $name,
  $address,
  $localId,
  $carrier,
  $employeeCount,
  $yearOfEstablishment,
  $area,
  $buildingDensity,
  $investor,
  $phoneNumber,
  $email,
  $website,
  $isWarning,
  $mapTypeId,
  $latitude,
  $longitude,
  $file,
  $note,
  $error
);
