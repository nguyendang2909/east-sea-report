<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//

$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][3]['link'] . '">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][3]['link'] . '">' . $mmenu['tracking']['sub'][3]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm trang</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "target_facebook.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='add'){
    $date = new DateClass();
    if(empty($name)) $error = '<span class="show-error">Vui lòng nhập tên trang.</span>';
	else {
        $db->table = "target_facebook";
        $data = array(
            'name'          => $db->clearText($name),
            'link'          => $db->clearText($link),
            'user'          => $db->clearText($user),
            'type'       => $db->clearText($faction),
            'note'          => $db->clearText($note),
            'created_time'  => time(),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->insert($data);
        echo($user);
		loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][6]['sub'][0]['link']);
		$OK = true;
	}
}
else {
    $name       = "";
    $link       = "";
    $user       = "";
    $faction    = "";
    $note       = "";
}
if(!$OK) targetFacebook(HOME_URL_LANG . $mmenu['tracking']['link'] . '/target_facebook_add', "add", 0, $name, $link, $user, $faction, $note, $error);