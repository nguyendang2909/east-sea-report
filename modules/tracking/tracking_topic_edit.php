<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][2]['link'] . '">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][2]['link'] . '">' . $mmenu['tracking']['sub'][2]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa chủ đề</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

$topic_id       = isset($_GET['id']) ? intval($_GET['id']) : intval($topic_id);
$db->table 		= "topic";
$db->condition 	= "`topic_id` = $topic_id";
$db->order 		= "";
$db->limit 		= 1;
$rows 			= $db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][2]['link']);

include_once (_F_TEMPLATES . DS . "tracking_topic.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='edit'){
    $date = new DateClass();
    if(empty($content)) $error = '<span class="show-error">Vui lòng nhập nội dung chủ đề.</span>';
    else {
		$db->table = "topic";
		$data = array(
            'content'       => $db->clearText($content),
            'archives'      => $db->clearText($archives),
            'created_date'  => $date->dmYtoYmd2($created_date),
            'note'          => $db->clearText($note),
            'keywords'      => $db->clearText($keywords),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
		);
		$db->condition = "`topic_id` = $topic_id";
		$db->update($data);
		loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][2]['link']);
		$OK = true;
	}
}
else {
	foreach($rows as $row) {
        $content        = $row['content'];
        $archives       = $row['archives'];
        $created_date   = $date->vnDate(strtotime($row['created_date']));
        $note           = $row['note'];
        $keywords       = $row['keywords'];
	}
}
if(!$OK) trackingTopic(HOME_URL_LANG . $mmenu['tracking']['link'] . '/tracking-topic-edit', "edit", $topic_id, $content, $archives, $created_date, $note, $keywords, $error);