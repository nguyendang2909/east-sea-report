<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][2]['link'] . '">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][2]['link'] . '">' . $mmenu['tracking']['sub'][2]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm chủ đề</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "tracking_topic.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='add'){
    $date = new DateClass();
    if(empty($content)) $error = '<span class="show-error">Vui lòng nhập nội dung chủ đề.</span>';
	else {
		$file_max_size	= FILE_MAX_SIZE;
		$dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'topic' . DS;
		
		$file_name      = 'os' . time() . '_' . md5(microtime(true));

        $file_size      = $_FILES['files']['size'];
        if ($file_size > 0) {
            $fileUp = new Upload($_FILES['files']);
            $fileUp->file_max_size = $file_max_size;
            $fileUp->file_new_name_body = $file_name;
            $fileUp->Process($dir_dest);
            if($fileUp->processed) {
                $file_name = $fileUp->file_dst_name;
                $OK = true;
            } else {
				$error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
            }
        } else {
            $OK = true;
            $file_name = '-no-';
        }
		if($OK){
			$db->table = "topic";
			$data = array(
				'content'       => $db->clearText($content),
				'archives'      => $db->clearText($archives),
				'created_date'  => $date->dmYtoYmd2($created_date),
				'note'          => $db->clearText($note),
				'keywords'      => $db->clearText($keywords),
				'created_time'  => time(),
				'modified_time' => time(),
				'user_id'       => $_SESSION["user_id"],
				'attached'		=> $db->clearText($file_name)
			);
			$db->insert($data);

			loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][2]['link']);
		}			
		$OK = true;

	}
}
else {
    $content        = "";
    $archives       = "";
    $created_date   = "";
    $note           = "";
    $keywords       = "";
	$files 			= "";
}
if(!$OK) trackingTopic(HOME_URL_LANG . $mmenu['tracking']['link'] . '/tracking-topic-add', "add", 0, $content, $archives, $created_date, $note, $keywords,$files, $error);