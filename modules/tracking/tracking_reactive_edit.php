<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][3]['link'] . '">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][3]['link'] . '">' . $mmenu['tracking']['sub'][3]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa chủ đề</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

$reactive_id    = isset($_GET['id']) ? intval($_GET['id']) : intval($reactive_id);
$db->table 		= "reactive";
$db->condition 	= "`reactive_id` = $reactive_id";
$db->order 		= "";
$db->limit 		= 1;
$rows 		= $db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][3]['link']);

include_once (_F_TEMPLATES . DS . "tracking_reactive.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='edit'){
    $date = new DateClass();
    if(empty($name)) $error = '<span class="show-error">Vui lòng nhập tên trang.</span>';
    else {
		$db->table = "reactive";
		$data = array(
            'name'          => $db->clearText($name),
            'link'          => $db->clearText($link),
            'user'          => intval($user),
            'faction'       => $db->clearText($faction),
            'note'          => $db->clearText($note),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
		);
		$db->condition = "`reactive_id` = $reactive_id";
		$db->update($data);

		loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][3]['link']);
		$OK = true;
	}
}
else {
	foreach($rows as $row) {
        $name       = $row['name'];
        $link       = $row['link'];
        $user       = intval($row['user']);
        $faction    = $row['faction'];
        $note       = $row['note'];
	}
}
if(!$OK) trackingReactive(HOME_URL_LANG . $mmenu['tracking']['link'] . '/tracking-reactive-edit', "edit", $reactive_id, $name, $link, $user, $faction, $note, $error);