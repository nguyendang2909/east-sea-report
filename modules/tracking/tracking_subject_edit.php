<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][1]['link'] . '">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][1]['link'] . '">' . $mmenu['tracking']['sub'][1]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa đối tượng</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

$subject_id     = isset($_GET['id']) ? intval($_GET['id']) : intval($subject_id);
$db->table 		= "subject";
$db->condition 	= "`subject_id` = $subject_id";
$db->order 		= "";
$db->limit 		= 1;
$rows 			= $db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][1]['link']);

include_once (_F_TEMPLATES . DS . "tracking_subject.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='edit'){
    $date = new DateClass();
    if(empty($name)) $error = '<span class="show-error">Vui lòng nhập tên đối tượng.</span>';
    else {
        $friends = isset($_POST['friends']) ? $_POST['friends'] : array();
        
		$db->table = "subject";
		$data = array(
            'name'          => $db->clearText($name),
            'age'           => formatNumberToInt($age),
            'address'       => $db->clearText($address),
            'religion'      => $db->clearText($religion),
            'faction'       => $db->clearText($faction),
            'social'        => $db->clearText($social),
            'local'         => $db->clearText($local),
            'friends'       => $db->clearText(json_encode($friends)),
            'note'       	=> $db->clearText($note),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
		);
		$db->condition = "`subject_id` = $subject_id";
		$db->update($data);
		loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][1]['link']);
		$OK = true;
	}
}
else {
	foreach($rows as $row) {
        $name       = $row['name'];
        $age        = $row['age'];
        $address    = $row['address'];
        $religion   = $row['religion'];
        $faction    = $row['faction'];
        $social     = $row['social'];
        $local      = $row['local'];
        $friends    = json_decode($row['friends']);
        $note       = $row['note'];
	}
}
if(!$OK) trackingSubject(HOME_URL_LANG . $mmenu['tracking']['link'] . '/tracking-subject-edit', "edit", $subject_id, $name, $age, $address, $religion, $faction, $social, $local, $friends, $note, $error);