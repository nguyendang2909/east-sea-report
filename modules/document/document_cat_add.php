<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
?>
<!-- Menu path -->
<div class="row">
	<div class="col-md-12">
		<ul class="breadcrumbs-alt">
			<li>
				<a href="/"><?php echo $menu_ol[0];?></a>
			</li>
			<li>
				<a href="<?php echo TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][1];?>"><?php echo $menu_ol[23];?></a>
			</li>
			<li>
				<a href="<?php echo TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][1];?>"><?php echo $menu_op[23][1];?></a>
			</li>
			<li>
				<a class="current">Thêm danh mục</a>
			</li>
		</ul>
	</div>
</div>
<!-- /.row -->
<?php
include_once (_F_TEMPLATES . DS . "document_cat.php");
if(empty($typeFunc)) $typeFunc = "no";

$OK = false;
$error = '';
if($typeFunc=='add'){
    if(empty($title)) $error = '<span class="show-error">Vui lòng nhập tên danh mục.</span>';
	else {
        $db->table = "document";
        $data = array(
            'title'         => $db->clearText($title),
            'parent'        => intval($parent),
            'comment'       => $db->clearText($comment),
            'created_time'  => time(),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->insert($data);

		loadPageSuccess("Đã thêm dữ liệu thành công.", TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][1]);
		$OK = true;
	}
}
else {
	$title      = "";
    $parent     = isset($_GET['parent']) ? intval($_GET['parent']) : 0;
    $comment    = "";
}
if(!$OK) documentCat(TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . 'document_cat_add', "add", 0, $title, $parent, $comment, $error);