<?php

if (!defined('TTH_SYSTEM')) {
	die('Please stop!');
}
if ($account["id"] > 0) {

	if (isset($_GET['id'])) {
		$groupId =  $_GET['id'];
	}


	if (isset($_GET['startdate'])) {
		$startDate =  $_GET['startdate'];
	}

	if (isset($_GET['trackingTargetRadio'])) {
		$trackingTargetRadio = $_GET['trackingTargetRadio'];
	}

	if (isset($_GET['enddate'])) {
		$endDate =  $_GET['enddate'];
	}

	if (isset($_GET['selectTrackingTarget'])) {
		$selectTrackingTarget = $_GET['selectTrackingTarget'];
	}

	if (isset($_GET['keywords'])) {
		$keywords = str_replace(
			[
				', ',
				',',
				'; ',
				';'
			],
			'|',
			$_GET['keywords']
		);
	}

	$date   = new DateClass();

	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	$objPHPExcel = $objReader->load(_F_MODULES . DS . "print/_temp/facebook_target_group.xls");
	$db->table = "facebook_target_group";
	$db->condition = "`subject_id` = $groupId AND `is_active` = 1";
	$db->order = "`subject_id` ASC";
	$db->limit = "1";
	$data = $db->select("`tracking_facebook_target`");
	$targetId = $data[0]['tracking_facebook_target'];
	$targetId =  str_replace("[", "", $targetId);
	$targetId =  str_replace("]", "", $targetId);

	$db->table = "fb_report";
	$db->join = 'a LEFT JOIN ' . TTH_DATA_PREFIX . 'fb_target b ON a.`fbTargetId` = b.`fbTargetId`';
	if ($selectTrackingTarget) {
		$db->condition = "a.`fbTargetId` = $selectTrackingTarget AND a.`dateTime` BETWEEN '$startDate' AND '$endDate' AND a.`content` REGEXP '$keywords'";
	} else {
		$db->condition = "a.`fbTargetId` IN ($targetId) AND a.`dateTime` BETWEEN '$startDate' AND '$endDate' AND a.`content` REGEXP '$keywords'";
	}

	$db->order = "";
	$db->limit = "";
	$data = $db->select('
		a.`fbReportId`,
		b.`name` AS `page`,
		a.`content`,
		a.`react`,
		a.`share`,
		a.`comment`,
		a.`dateTime`,
		a.`image`,
		a.`link`
		');

	$baseRow = 3;
	foreach ($data as $r => $dataRow) {
		// $i = 2;

		$row = $baseRow + $r;
		$objPHPExcel->getActiveSheet()->insertNewRowBefore($row, 1);
		$objPHPExcel->getActiveSheet()
			->setCellValue('A' . $row, $r + 1)
			->setCellValue('B' . $row, stripslashes($dataRow['page']))
			->setCellValue('C' . $row, stripslashes($dataRow['content']))
			->setCellValue('D' . $row, stripslashes($dataRow['react']))
			->setCellValue('E' . $row, stripslashes($dataRow['share']))
			->setCellValue('F' . $row, stripslashes($dataRow['comment']))
			->setCellValue('G' . $row, stripslashes($dataRow['dateTime']))
			->setCellValue('H' . $row, stripslashes($dataRow['link']))
			->setCellValue('I' . $row, stripslashes($dataRow['image']));

		$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(-1);
	}
	$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(300);
	$objPHPExcel->getActiveSheet()->removeRow($baseRow - 1, 1);

	$time = $date->vnOther(time(), 'd-m-Y_H-i');
	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Doi-tuong-theo-doi_(' . $time . ').xls"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;
} else loadPageError('Lỗi! Dữ liệu truy xuất không hợp lệ.', HOME_URL_LANG);
// 
