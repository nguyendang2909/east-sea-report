<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][3]['sub'][1]['link'] . '">' . $mmenu['network']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][3]['sub'][1]['link'] . '">' . $mmenu['network']['sub'][3]['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][3]['sub'][1]['link'] . '">' . $mmenu['network']['sub'][3]['sub'][1]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa nhóm thiết bị</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

$software_id    = isset($_GET['id']) ? intval($_GET['id']) : intval($software_id);
$db->table 		= "software";
$db->condition 	= "`software_id` = $software_id";
$db->order 		= "";
$db->limit 		= 1;
$rows 			= $db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][3]['sub'][1]['link']);

include_once (_F_TEMPLATES . DS . "software_category.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='edit'){
    $date = new DateClass();
    if(empty($title)) $error = '<span class="show-error">Vui lòng nhập tên nhóm phần mềm.</span>';
    else {
		$db->table = "software";
		$data = array(
            'title'         => $db->clearText($title),
            'note'       	=> $db->clearText($note),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
		);
		$db->condition = "`software_id` = $software_id";
		$db->update($data);
		loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][3]['sub'][1]['link']);
		$OK = true;
	}
}
else {
	foreach($rows as $row) {
        $title	= $row['title'];
        $note	= $row['note'];
	}
}
if(!$OK) softwareCat(HOME_URL_LANG . $mmenu['network']['link'] . '/software-category-edit', "edit", $software_id, $title, $note, $error);