<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][1]['link'] . '">' . $mmenu['network']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][1]['link'] . '">' . $mmenu['network']['sub'][1]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa IP</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

$network_ip_id  = isset($_GET['id']) ? intval($_GET['id']) : intval($network_ip_id);
$db->table 		= "network_ip";
$db->condition 	= "`network_ip_id` = $network_ip_id";
$db->order 		= "";
$db->limit 		= 1;
$rows 			= $db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][1]['link']);

include_once (_F_TEMPLATES . DS . "network_ip.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='edit'){
    $date = new DateClass();
    if(empty($ip)) $error = '<span class="show-error">Vui lòng nhập địa chỉ IP.</span>';
    else {
		$db->table = "network_ip";
		$data = array(
            'ip'            => $db->clearText($ip),
            'device_item'   => $db->clearText($device_item),
            'agency'        => intval($agency),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
		);
		$db->condition = "`network_ip_id` = $network_ip_id";
		$db->update($data);
		loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][1]['link']);
		$OK = true;
	}
}
else {
	foreach($rows as $row) {
        $ip	            = $row['ip'];
        $device_item    = $row['device_item'];
        $agency         = $row['agency'];
	}
}
if(!$OK) networkIP(HOME_URL_LANG . $mmenu['network']['link'] . '/network-ip-edit', "edit", $network_ip_id, $ip, $device_item, $agency, $error);