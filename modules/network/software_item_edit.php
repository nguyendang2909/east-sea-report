<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][2]['sub'][0]['link'] . '">' . $mmenu['network']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][2]['sub'][0]['link'] . '">' . $mmenu['network']['sub'][2]['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][2]['sub'][0]['link'] . '">' . $mmenu['network']['sub'][2]['sub'][0]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa thiết bị</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

$software_item_id = isset($_GET['id']) ? intval($_GET['id']) : intval($software_item_id);
$db->table      = "software_item";
$db->condition  = "`software_item_id` = $software_item_id";
$db->order      = "";
$db->limit      = 1;
$rows           = $db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][2]['sub'][0]['link']);

include_once (_F_TEMPLATES . DS . "software_item.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='edit'){
    $date = new DateClass();
    if(empty($software)) $error = '<span class="show-error">Vui lòng chọn nhóm phần mềm.</span>';
    elseif(empty($title)) $error = '<span class="show-error">Vui lòng nhập tên phần mềm.</span>';
    else {
        $file_max_size	= FILE_MAX_SIZE;
        $dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'software' . DS;

        $file_name      = 'os' . time() . '_' . md5(microtime(true));
        $file_size      = $_FILES['files']['size'];
        if ($file_size > 0) {
            $fileUp = new Upload($_FILES['files']);
            $fileUp->file_max_size = $file_max_size;
            $fileUp->file_new_name_body = $file_name;
            $fileUp->Process($dir_dest);
            if($fileUp->processed) {
                $file_name = $fileUp->file_dst_name;
                $OK = true;
            } else {
                $error = '<span class="show-error">Lỗi tải tệp tin: '.$fileUp->error.'</span>';
            }
        } else {
            $OK = true;
            foreach($rows as $row) {
                $file_name = stripslashes($row['files']);
            }
        }
		
		if($OK) {
			$db->table = "software_item";
			$data = array(
                'software'      => intval($software),
                'title'       	=> $db->clearText($title),
                'perform'       => $db->clearText($perform),
                'owner'       	=> $db->clearText($owner),
                'address'       => $db->clearText($address),
                'agency'        => intval($agency),
                'user'          => intval($user),
                'history'       => $db->clearText($history),
                'note'          => $db->clearText($note),
                'files'       	=> $db->clearText($file_name),
				'modified_time' => time(),
				'user_id'       =>  $_SESSION["user_id"]
			);
			$db->condition = "`software_item_id` = $software_item_id";
			$db->update($data);
			
			loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][3]['sub'][0]['link']);
		}
		$OK = true;
	}
}
else {
	foreach($rows as $row) {
        $software  	= $row['software'];
        $title    	= $row['title'];
        $perform    = $row['perform'];
        $owner      = $row['owner'];
        $address    = $row['address'];
        $agency     = $row['agency'];
        $user       = $row['user'];
        $history    = $row['history'];
        $note       = $row['note'];
        $files      = $row['files'];
	}
}
if(!$OK) softwareItem(HOME_URL_LANG . $mmenu['network']['link'] . '/software-item-edit', "edit", $software_item_id, $software, $title, $perform, $owner, $address, $agency, $user, $history, $note, $files, $error);