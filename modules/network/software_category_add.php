<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][3]['sub'][1]['link'] . '">' . $mmenu['network']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][3]['sub'][1]['link'] . '">' . $mmenu['network']['sub'][3]['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][3]['sub'][1]['link'] . '">' . $mmenu['network']['sub'][3]['sub'][1]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm nhóm thiết bị</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "software_category.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='add'){
    $date = new DateClass();
    if(empty($title)) $error = '<span class="show-error">Vui lòng nhập tên nhóm phần mềm.</span>';
	else {
        $db->table = "software";
        $data = array(
            'title'         => $db->clearText($title),
            'note'       	=> $db->clearText($note),
            'created_time'  => time(),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->insert($data);

		loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][3]['sub'][1]['link']);
		$OK = true;
	}
}
else {
	$title	= "";
    $note	= "";
}
if(!$OK) softwareCat(HOME_URL_LANG . $mmenu['network']['link'] . '/software-category-add', "add", 0, $title, $note, $error);