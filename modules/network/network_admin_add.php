<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][4]['link'] . '">' . $mmenu['network']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][4]['link'] . '">' . $mmenu['network']['sub'][4]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm IP</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "network_admin.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='add'){
    $date = new DateClass();
    if(empty($name)) $error = '<span class="show-error">Vui lòng nhập đầy đủ Họ và tên.</span>';
	else {
        $db->table = "network_admin";
        $data = array(
            'name'      	=> $db->clearText($name),
            'apply'   		=> $db->clearText($apply),
            'agency'        => intval($agency),
            'tel'   		=> $db->clearText($tel),
            'email'   		=> $db->clearText($email),
            'address'   	=> $db->clearText($address),
            'created_time'  => time(),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->insert($data);

		loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][4]['link']);
		$OK = true;
	}
}
else {
	$name	  	= "";
	$apply		= "";
    $agency  	= 0;
	$tel		= "";
	$email		= "";
	$address	= "";
}
if(!$OK) networkAdmin(HOME_URL_LANG . $mmenu['network']['link'] . '/network-admin-add', "add", 0, $name, $apply, $agency, $tel, $email, $address, $error);