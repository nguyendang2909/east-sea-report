<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][4]['link'] . '">' . $mmenu['network']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][4]['link'] . '">' . $mmenu['network']['sub'][4]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa IP</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

$network_admin_id  = isset($_GET['id']) ? intval($_GET['id']) : intval($network_admin_id);
$db->table 		= "network_admin";
$db->condition 	= "`network_admin_id` = $network_admin_id";
$db->order 		= "";
$db->limit 		= 1;
$rows 			= $db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][4]['link']);

include_once (_F_TEMPLATES . DS . "network_admin.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='edit'){
    $date = new DateClass();
    if(empty($name)) $error = '<span class="show-error">Vui lòng nhập đầy đủ Họ và tên.</span>';
    else {
		$db->table = "network_admin";
		$data = array(
            'name'      	=> $db->clearText($name),
            'apply'   		=> $db->clearText($apply),
            'agency'        => intval($agency),
            'tel'   		=> $db->clearText($tel),
            'email'   		=> $db->clearText($email),
            'address'   	=> $db->clearText($address),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
		);
		$db->condition = "`network_admin_id` = $network_admin_id";
		$db->update($data);
		loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][4]['link']);
		$OK = true;
	}
}
else {
	foreach($rows as $row) {
        $name		= $row['name'];
		$apply		= $row['apply'];
        $agency   	= $row['agency'];
		$tel		= $row['tel'];
		$email		= $row['email'];
		$address	= $row['address'];
	}
}
if(!$OK) networkAdmin(HOME_URL_LANG . $mmenu['network']['link'] . '/network-admin-edit', "edit", $network_admin_id, $name, $apply, $agency, $tel, $email, $address, $error);