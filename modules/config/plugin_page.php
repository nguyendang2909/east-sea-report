<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['config']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">' . $mmenu['config']['sub'][4]['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

if(isset($_POST['update'])) {
	function updateConstant ($constant, $value) {
		global $db;
		$db->table = "constant";
		$data =array(
			'value'=>$db->clearText($value)
		);
        $db->condition = "constant LIKE '$constant'";
		$db->update($data);
	}

	$nameConstant = $_POST["name_constant"];
	$countConstant = count($nameConstant);
	$valueConstant = $_POST["value_constant"];
	for($i = 0; $i < $countConstant; $i++) {
		updateConstant($nameConstant[$i], $valueConstant[$i]);
	}
}
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-puzzle-piece fa-fw"></i> Phần bổ sung
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form method="post" class="form-ol-3w">
						<table class="table table-no-border table-hover">
							<?php
							$db->table = "constant";
							$db->condition = "`type` = 7";
							$db->order = "`sort` ASC";
							$db->limit = "";
							$rows = $db->select();

							foreach($rows as $row) {
							?>
							<tr>
								<td>
									<label><?php echo $row['name']?>:</label>
									<input type="hidden" name="name_constant[]" value="<?php echo $row['constant']?>" >
									<textarea class="form-control ckeditor" rows="6" style="resize: none;" name="value_constant[]" ><?php echo stripslashes($row['value'])?></textarea>
								</td>
							</tr>
							<?php
							}
							?>
							<tr>
								<td class="form-ol-btn-tzc">
									<button type="submit" name="update" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='/'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>