<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['config']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">' . $mmenu['config']['sub'][0]['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

if(isset($_POST['update'])) {
	function updateConstant ($constant, $value) {
		global $db;
		$db->table = "constant";
		$data =array(
			'value'=>$db->clearText($value)
		);
		$db->condition = "constant LIKE '$constant'";
		$db->update($data);
	}

	$nameConstant = $_POST["name_constant"];
	$countConstant = count($nameConstant);
	$valueConstant = $_POST["value_constant"];
	for($i = 0; $i < $countConstant; $i++) {
		updateConstant($nameConstant[$i], $valueConstant[$i]);
	}
}
?>
<script type="text/javascript">
	function BrowseFile() {
		var finder = new CKFinder();
		finder.selectActionFunction = SetFileField;
		finder.popup();
	}
	function SetFileField(fileUrl) {
		document.getElementById('imageLogo').value = fileUrl;
	}
</script>
<div class="row">
	<div class="col-lg-10">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-gears fa-fw"></i> <?php echo $mmenu['config']['sub'][0]['title'];?>
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form method="post" class="form-ol-3w">
						<table class="table table-no-border table-hover">
							<?php
							$db->table = "constant";
							$db->condition = "`type` = 0";
							$db->order = "`sort` ASC";
							$db->limit = "";
							$rows = $db->select();

							foreach($rows as $row) {
							?>
							<tr>
								<td width="200px" align="right" class="ver-top"><label class="form-lb-tp"><?php echo $row['name']?>:</label></td>
								<td>
									<input type="hidden" name="name_constant[]" value="<?php echo $row['constant']?>" >
									<?php
									if($row['constant']=='file_logo') {
									?>
										<div class="input-group ">
											<input class="form-control" type="text" name="value_constant[]" id="imageLogo" value="<?php echo stripslashes($row['value'])?>">
											<div class="input-group-btn">
												<button  class="btn btn-primary" type="button" name="file_logo" onclick="BrowseFile();"><i class="glyphicon glyphicon-folder-open"></i> &nbsp;Chọn tệp...</button>
											</div>
										</div>
									<?php
									}
									else if($row['constant']=='error_page') {
									?>
										<textarea class="form-control" rows="4" style="resize: none;" name="value_constant[]" id="<?php echo $row['constant']?>" ><?php echo stripslashes($row['value'])?></textarea>
									<?php
									}
									else if($row['constant']=='keywords') {
									?>
										<input class="form-control" type="text" name="value_constant[]" data-role="tagsinput" value="<?php echo stripslashes($row['value'])?>" >
									<?php
									}
									else {
									?>
										<input class="form-control" type="text" name="value_constant[]" value="<?php echo stripslashes($row['value'])?>" >
									<?php
									}
									?>
								</td>
							</tr>
							<?php
							}
							?>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" name="update" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='/'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	CKEDITOR.replace( 'error_page', {
		height: 70,
		toolbar: [
			[ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
			[ 'FontSize', 'TextColor', 'BGColor' ]
		]
	});
</script>