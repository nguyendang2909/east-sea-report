<?php
include_once(_F_TEMPLATES . DS . "month_report.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . $mmenu['month-report']['link'] . '">' . $mmenu['month-report']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">Thêm</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'add') {
  $db->table = 'month_report';
  $db->condition = 'isActive = 1 AND month = ' . $month . ' AND year = ' . $year;
  $db->order = "";
  $db->limit = 1;
  $rows = $db->select();

  if (empty($month) || empty($year)) $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
  elseif ($year > date('Y')) $error = '<span class="show-error">Không nhập ngày tháng quá xa.</span>';
  elseif ( $db->RowCount>0 ) $error = '<span class="show-error">Report bị trùng, vui lòng check lại.</span>';
  else {
      $db->table = "month_report";
      $data = array(
        'year' => intval($year),
        'month' => intval($month),
        'createdBy' => $_SESSION["user_id"],
        'updatedBy' => $_SESSION["user_id"],
        'createdAt' => date("Y-m-d H:i:s"),
        'updatedAt' => date("Y-m-d H:i:s"),
      );
      $db->insert($data);

      loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['month-report']['link']);
  }
} else {
  $year = date('Y');
  $month = date('m');
}

if (!$OK) postMonthReport(
  HOME_URL_LANG . $mmenu['month-report']['link'] . '/month-report-add',
  "add",
  0,
  $year,
  $month,
  $error
);
