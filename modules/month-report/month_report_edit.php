<?php
include_once(_F_TEMPLATES . DS . "month_report_upload.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . $mmenu['month-report']['link'] . '">' . $mmenu['month-report']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">Sửa</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

// Get record to edit from db
$monthReportId = isset($_GET['id']) ? intval($_GET['id']) : intval($monthReportId);
$db->table      = "month_report";
$db->condition  = "`monthReportId` = $monthReportId";
$db->order      = "";
$db->limit      = 1;
$rows           = $db->select();
if ($db->RowCount == 0) loadPageError("Dữ liệu không tồn tại." . $monthReportId, HOME_URL_LANG . $mmenu['month-report']['link']);

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'edit') {

    $fileMaxSize  = FILE_MAX_SIZE;
    $dirDest       = ROOT_DIR . DS . 'uploads' . DS . 'month-report' . DS;
    unlink(ROOT_DIR . DS . 'uploads' . DS . 'month-report' . DS . $year . '-' . $month . '.docx');

    $fileName      = $year . '-' . $month;
    $fileSize      = $_FILES['file']['size'];
    if ($fileSize > 0) {
      $fileUp = new Upload($_FILES['file']);
      $fileUp->file_max_size = $fileMaxSize;
      $fileUp->file_new_name_body = $fileName;
      $fileUp->Process($dirDest);
      if ($fileUp->processed) {
        $fileName = $fileUp->file_dst_name;
        $OK = true;
      } else {
        $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
      }
    } elseif ($fileSize == 0) {
      $fileName = '-no-';
      $OK = true;
    } else {
      $OK = true;
      foreach ($rows as $row) {
        $fileName = stripslashes($row['file']);
      }
    }

    if ($OK) {
      loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['month-report']['link']);
    }
    $OK = true;
  
} else {
  foreach ($rows as $row) {
    $monthReportId = $row['monthReportId'];
    $year = $row['year'];
    $month = $row['month'];
    $time = date('d/m/Y', strtotime($row['time']));
    $file = '';
  }
}

if (!$OK) postMonthReportUpload(
  HOME_URL_LANG . $mmenu['month-report']['link'] . '/month-report-edit',
  "edit",
  $monthReportId,
  $year,
  $month,
  $file,
  $error
);
