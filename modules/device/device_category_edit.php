<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['device']['link'] . $mmenu['device']['sub'][1]['link'] . '">' . $mmenu['device']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['device']['link'] . $mmenu['device']['sub'][1]['link'] . '">' . $mmenu['device']['sub'][1]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa nhóm thiết bị</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

$device_id 		= isset($_GET['id']) ? intval($_GET['id']) : intval($device_id);
$db->table 		= "device";
$db->condition 	= "`device_id` = $device_id";
$db->order 		= "";
$db->limit 		= 1;
$rows 			= $db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['device']['link'] . $mmenu['device']['sub'][1]['link']);

include_once (_F_TEMPLATES . DS . "device_category.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='edit'){
    $date = new DateClass();
    if(empty($title)) $error = '<span class="show-error">Vui lòng nhập tên nhóm thiết bị.</span>';
    else {
		$db->table = "device";
		$data = array(
            'title'         => $db->clearText($title),
            'note'       	=> $db->clearText($note),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
		);
		$db->condition = "`device_id` = $device_id";
		$db->update($data);
		loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['device']['link'] . $mmenu['device']['sub'][1]['link']);
		$OK = true;
	}
}
else {
	foreach($rows as $row) {
        $title	= $row['title'];
        $note	= $row['note'];
	}
}
if(!$OK) deviceCat(HOME_URL_LANG . $mmenu['device']['link'] . '/device-category-edit', "edit", $device_id, $title, $note, $error);