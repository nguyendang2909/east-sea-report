<?php
include_once(_F_TEMPLATES . DS . "east_sea_report.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] != 0) {
// Menu
  $breadcrumbs = '<ul class="breadcrumbs-alt">';
  $breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
  $breadcrumbs .= '</ul>';
  echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

  if (empty($typeFunc)) $typeFunc = '-no-';
  $OK = false;
  $error = '';

  if ($typeFunc == 'add') {
  if (empty($startDate) || $startDate == '__/__/____'    || empty($endDate) || $endDate == '') $error = '
    <span class="show-error">Vui lòng nhập đủ thông tin.</span>
  ';
  else $OK = true;

  if ($OK) {
    $formattedStartDate = date("Y-m-d", strtotime($date->dmYtoYmd($startDate)));
    $formattedEndDate = date("Y-m-d", strtotime($date->dmYtoYmd($endDate))) 
    
?>
  <div class="row">
    <div class="col-lg-12 col-md-9">
      <div class="panel">
        <div class="panel-heading text-center">
          <i class="fa fa-sitemap fa-fw"></i> Báo cáo Biển Đông từ ngày <?php echo $startDate ?> đến ngày <?php echo $endDate ?>
          <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
            <a href="javascript:;" class="fa fa-chevron-down"></a>
            <a href="javascript:;" class="fa fa-eye"></a>
            <a href="javascript:;" class="fa fa-compress"></a>
            <a href="javascript:;" class="fa fa-times"></a>
          </span>
        </div>
        <div class="panel-body">
          <div class="form-responsive">
            <form action="<?php echo $act ?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
              <input type="hidden" name="typeFunc" value="<?php echo $typeFunc ?>" />
              <div class="panel-show-error">
                <?php echo $error ?>
              </div>
              <table class="table table-no-border table-hover">

                <tr>
                  <td>
                    <b>1. Tình hình trên thực địa</b>
                  </td>
                </tr>

                <tr>
                  <td>
                    <b>2. Quan điểm, lập trường, chính sách về Biển Đông</b>
                    <br><?php
                    foreach($opinionCountries as $key => $val) {
                      echo '<ul class="list-group">' . $key;
                      foreach($val as $value) {
                        echo '<li class="list-group-item">- ' . $value . '</li>';
                      };
                      echo '</ul>';

    }
    ?>
                  </td>
                </tr>

                <tr>
                  <td colspan="2" class="form-ol-btn-tzc">
                    <button type="submit" class="btn btn-primary btn-round">Xuất file Word</button> &nbsp;
                    <button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['east-sea-report']['link']; ?>'">Thoát</button>
                  </td>
                </tr>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
 }

} else {
  $startDate = '';
  $endDate = '';
}
if (!$OK) postEastSeaReport(
  HOME_URL_LANG . $mmenu['east-sea-report']['link'],
  "add",
  $startDate,
  $endDate,
  $error
);

}