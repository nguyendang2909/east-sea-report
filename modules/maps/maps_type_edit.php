<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['maps']['link'] . $mmenu['maps']['sub'][1]['link'] . '">' . $mmenu['maps']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['maps']['link'] . $mmenu['maps']['sub'][1]['link'] . '">' . $mmenu['maps']['sub'][1]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa thể loại</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

$maps_type_id = isset($_GET['id']) ? intval($_GET['id']) : intval($maps_type_id);
$db->table      = "maps_type";
$db->condition  = "`maps_type_id` = $maps_type_id";
$db->order      = "";
$db->limit      = 1;
$rows           = $db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['maps']['link'] . $mmenu['maps']['sub'][1]['link']);

include_once (_F_TEMPLATES . DS . "maps_type.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='edit'){
    $date = new DateClass();
    if(empty($title)) $error = '<span class="show-error">Vui lòng nhập tên loại vị trí.</span>';
    else {
		$handleUploadImg = false;
		$file_max_size  = FILE_MAX_SIZE;
		$dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'maps' . DS;
		$file_size      = $_FILES['icon']['size'];

		if($file_size>0) {
			$imgUp = new Upload($_FILES['icon']);

			$imgUp->file_max_size = $file_max_size;
			if ($imgUp->uploaded) {
				$handleUploadImg = true;
				$OK = true;
			}
			else {
				$error = '<span class="show-error">Lỗi tải hình: ' . $imgUp->error . '</span>';
			}
		}
		else {
			$handleUploadImg = false;
			$OK = true;
		}
		
		if($OK) {
			$db->table = "maps_type";
			$data = array(
				'title'       	=> $db->clearText($title),
				'note'          => $db->clearText($note),
				'modified_time' => time(),
				'user_id'       => intval($_SESSION["user_id"])
			);
			$db->condition = "`maps_type_id` = $maps_type_id";
			$db->update($data);
			$id_query = $maps_type_id;
			
			if ($handleUploadImg) {
                $stringObj = new String();
                if(glob($dir_dest .'*'. $icon)) array_map("unlink", glob($dir_dest .'*'.$icon));
                $name_icon = $stringObj->getSlug(mb_substr($title, 0, 50, 'UTF-8') . '-' . $id_query . '-' . time());

                $imgUp->file_new_name_body = 'full_' . $name_icon;
                $imgUp->Process($dir_dest);

                $imgUp->file_new_name_body = $name_icon;
                $imgUp->image_resize = true;
                $imgUp->image_ratio_fill = true;
                $imgUp->image_x = 24;
                $imgUp->image_y = 32;
                $imgUp->Process($dir_dest);
                if ($imgUp->processed) {
                    $db->table = "maps_type";
                    $data = array(
                        'icon' => $db->clearText($imgUp->file_dst_name)
                    );
                    $db->condition = "`maps_type_id` = $id_query";
                    $db->update($data);
                } else {
                    loadPageError("Lỗi tải hình: " . $imgUp->error, HOME_URL_LANG . $mmenu['maps']['link'] . $mmenu['maps']['sub'][1]['link']);
				}

				$imgUp->Clean();
			}

			loadPageSuccess("Đã chỉnh sửa liệu thành công", HOME_URL_LANG . $mmenu['maps']['link'] . $mmenu['maps']['sub'][1]['link']);
			$OK = true;
		}
		$OK = true;
	}
}
else {
	foreach($rows as $row) {
        $icon      	= $row['icon'];
        $title    	= $row['title'];
        $note       = $row['note'];
	}
}
if(!$OK) mapsType(HOME_URL_LANG . $mmenu['maps']['link'] . '/maps-type-edit', "edit", $maps_type_id, $icon, $title, $note, $error);