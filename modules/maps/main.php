<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['maps']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">' . $mmenu['maps']['sub'][0]['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
?>
<div id="web_map" class="wrapper">
	<nav id="sidebar">
    abc
	</nav>
	<div id="content">
	<ul id="map_draw">
        <li data-value="Point"><i class="fa fa-dot-circle-o"></i></li>
        <li data-value="LineString"><i class="fa fa-arrows-h"></i></li>
        <li data-value="Polygon"><i class="fa fa-square"></i></li>
    </ul>
	</div>
</div>
<script type="text/javascript">
    $('#document').ready(function() {
        var format = 'image/png';

        var bounds = [ 103, 8, 118, 24 ];
        var Vietnam = new ol.layer.Image({
            title: 'Hành chính Việt Nam',
            source: new ol.source.ImageWMS({
                ratio: 1,
                url: 'http://192.168.10.79:8181/geoserver/GIS/wms',
                params: {
                    'FORMAT': format,
                    'VERSION': '1.1.1',
                    STYLES: '',
                    LAYERS: 'GIS:timezones,GIS:world,GIS:border,GIS:provinces,GIS:districts,GIS:rivers,GIS:railways,GIS:roads',
                }
            })
        });

        <?php
        $layers = array();
        $db->table = "maps_type";
        $db->condition = "`is_active` = 1";
        $db->order = "`title` ASC";
        $db->limit = "";
        $rows = $db->select();
        if($db->RowCount>0) {
            foreach ($rows as $row) {
                $icon = '';
                if($row['icon']=='-no-') $icon = HOME_URL_LANG . '/images/icon.png';
                else $icon = HOME_URL_LANG . '/uploads/maps/' . stripslashes($row['icon']);

                echo 'var iconStyle' . $row['maps_type_id'] . ' = new ol.style.Style({ image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({ anchorYUnits: \'pixels\', src: \'' . $icon . '\' })), stroke: new ol.style.Stroke({ color: \'#ec6459\', width: 2 }), fill: new ol.style.Fill({ color: \'rgba(236, 100, 89, 0.1)\' }) });';
                echo 'var geo' . md5($row['maps_type_id']) . ' = new ol.layer.Vector({ title: \'' . htmlspecialchars(stripslashes($row['title'])) . '\', source: new ol.source.Vector({url: \'/action.php?url=maps_layers&type=load&code=' . intval($row['maps_type_id']) . '\', format: new ol.format.GeoJSON()}) });';
                array_push($layers, 'geo' . md5($row['maps_type_id']));
                echo 'geo' . md5($row['maps_type_id']) . '.setStyle(iconStyle' . $row['maps_type_id'] . ');';
            }
        }
        ?>
        var features = new ol.Collection();
        var source = new ol.source.Vector({
            features: features
        });
        var Vector = new ol.layer.Vector({
            title: 'Vector',
            source: source,
            style: new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.2)'
                }),
                stroke: new ol.style.Stroke({
                    color: '#ffcc33',
                    width: 2
                }),
                image: new ol.style.Circle({
                    radius: 7,
                    fill: new ol.style.Fill({
                        color: '#ffcc33'
                    })
                })
            })
        });

        var projection = new ol.proj.Projection({
            code: 'EPSG:3405',
            units: 'm',
            axisOrientation: 'neu'
        });
        var map = new ol.Map({
            target: 'web_map',
            layers: [
                new ol.layer.Group({
                    title: 'Lớp nền',
                    layers: [
                        Vietnam
                    ]
                }),
                new ol.layer.Group({
                    'title': 'Lớp chuyên đề',
                    layers: [
                        Vector, <?php echo implode(',', $layers) ?>
                    ]
                })
            ],
            view: new ol.View({
                center: [0, 0],
                zoom: 5,
                minZoom: 5,
                maxZoom: 50,
                projection: projection
            })
        });
        var layerSwitcher = new ol.control.LayerSwitcher();
        map.addControl(layerSwitcher);
        map.getView().fit(bounds, map.getSize());
        var modify = new ol.interaction.Modify({ source: source });
        map.addInteraction(modify);

        var draw, snap;

        function addInteractions(typeSelect) {
            if (typeSelect !== 'None') {
                draw = new ol.interaction.Draw({
                    source: source,
                    type: typeSelect
                });
                map.addInteraction(draw);
                snap = new ol.interaction.Snap({source: source});
                map.addInteraction(snap);

                draw.on('drawend', function(evt) {
                    var justNowFeature = evt.feature;
                    var featureType = String(justNowFeature.getGeometry().getType());
                    var featureGeom = String(justNowFeature.getGeometry().getCoordinates());
                    $.ajax({
                        url: '/action.php',
                        type: 'POST',
                        data: {
                            'url'       : 'map_modal',
                            'type'      : 'add',
                            'f_type'    : featureType,
                            'f_latlong' : featureGeom
                        },
                        dataType: 'html',
                        success: function(data){
                            showResult('_os_modal', data);
                            $('#_os_modal').modal({
                                backdrop: 'static',
                                keyboard: false,
                            });
                            $('.selectpicker').selectpicker('refresh');
                        }
                    });
                });
            }
        }
        $('#map_draw').on('click', 'li', function () {
            var c = $(this).attr('class');
            var type = $(this).attr('data-value');
            $(this).parent().find('li.active').removeClass('active');
            map.removeInteraction(draw);
            map.removeInteraction(snap);
            if( c!='active' ) {
                $(this).addClass('active');
                addInteractions(type);
            } else {
                addInteractions('None');
            }
        });

        $('#_os_modal').on('click', 'form#_maps button[type="submit"]', function () {
            $('form#_maps').validate({
                submitHandler: function (form) {
                    showLoader();
                    var dataList = new FormData($(form)[0]);
                    dataList.append("url", 'maps');
                    dataList.append("type", 'add');
                    $.ajax({
                        url: '/action.php',
                        type: 'POST',
                        data: dataList,
                        dataType: 'json',
                        success: function (data) {
                            closeLoader();
                            if (data.process == true) {
                                map.render();
                                $('#_os_modal').modal('hide');
                            } else alert(data.msg);
                        },
                        error: function () {
                            closeLoader();
                            alert('Lỗi dữ liệu, vui lòng kiểm tra lại.');
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                    return false;
                },
                ignore: ':not(select.selectpicker:hidden, select:visible, input:visible, textarea:visible)'
            });
        });
    });
</script>
