<?php
$reactiveId = 4;
    $db->table = "reactive";
    $db->condition = "`reactiveId` = " .$reactiveId;
    $db->order = '';
    $db->limit = '';
    $rows = $db->select('`fbTargetId`');
    $listFbTargetId = json_decode($rows[0]['fbTargetId']);
  
    $query = "(a.`fbTargetId` = " .$listFbTargetId[0];
    if(count($listFbTargetId)>1)
    {
      $dem;
      for($dem =1; $dem< count($listFbTargetId); $dem++)
      {
        $query .= " OR a.`fbTargetId` =" .$listFbTargetId[$dem];
      }
    }
    $query .= ")";

    $data = array();
    $db->table = "fb_report";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "fb_target` b ON a.`fbTargetId` = b.`fbTargetId`";
    $db->condition = $query;
    $db->order = "a.`dateTime` DESC";
    $db->limit='';
    $rows = $db->select("
        b.`name`,
        a.`content`,
        a.`react`,
        a.`share`,
        a.`comment`,
        a.`dateTime`
        ");

        $i = 0;
        // $i = $requestData['start'];
        foreach ($rows as $row) {
          $i++;
      
          $nestedData =   array();
          $nestedData['no'] = $i;
          $nestedData['name'] = stripslashes($row['name']);
          $nestedData['content'] = stripslashes($row['content']);
          $nestedData['react'] = stripslashes($row['react']);
          $nestedData['share'] = stripslashes($row['share']);
          $nestedData['comment'] = stripslashes($row['comment']);
          $nestedData['dateTime'] = stripslashes($row['dateTime']);
      
          $data[] = $nestedData;
        }

        $json_data = array(
          "data"            => $data
        );
      
        echo json_encode($json_data);