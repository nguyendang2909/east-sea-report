<?php
include_once(_F_TEMPLATES . DS . "field_situation.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . $mmenu['field-situation']['link'] . '">' . $mmenu['field-situation']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">Sửa</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

// Get record to edit from db
$fieldSituationId = isset($_GET['id']) ? intval($_GET['id']) : intval($fieldSituationId);
$db->table      = "field_situation";
$db->condition  = "`fieldSituationId` = $fieldSituationId";
$db->order      = "";
$db->limit      = 1;
$rows           = $db->select();
if ($db->RowCount == 0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['field-situation']['link']);

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'edit') {
  // Validate data
  if (empty($time) || $time == '__/__/____' || empty($localId) || $localId == '' || empty($shipId) || $shipId == '') $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
      $db->table = "field_situation";
      $data = array(
        'time' => date("Y-m-d", strtotime($date->dmYtoYmd($time))),
        'localId' => intval($localId),
        'countryId' => intval($countryId),
        'shipId' => intval($shipId),
        'purpose' => stripslashes($purpose),
        'note' => stripslashes($note),
        'updatedAt' => date("Y-m-d H:i:s"),
        'updatedBy' =>  $_SESSION["user_id"],
      );
      $db->condition = "`fieldSituationId` = $fieldSituationId";
      $db->update($data);

      loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['field-situation']['link']);
    $OK = true;
} else {
  foreach ($rows as $row) {
    $localId = $row['localId'];
    $note = $row['note'];
    $countryId = $row['countryId'];
    $shipId = $row['shipId'];
    $purpose = $row['purpose'];
    $time = date('d/m/Y', strtotime($row['time']));
  }
}
if (!$OK) postFieldSituation(
  HOME_URL_LANG . $mmenu['field-situation']['link'] . '/field-situation-edit',
  "edit",
  $fieldSituationId,
  $time,
  $localId,
  $countryId,
  $shipId,
  $purpose,
  $note,
  $error
);
