<?php
include_once(_F_TEMPLATES . DS . "opinion.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . $mmenu['opinion']['link'] . '">' . $mmenu['opinion']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">Thêm</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'add') {
  // Validate data
  if (empty($time) || $time == '__/__/____' || empty($countryId) || $countryId == '') $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
  else {
      $db->table = "opinion";
      $data = array(
        'time' => date("Y-m-d", strtotime($date->dmYtoYmd($time))),
        'countryId' => intval($countryId),
        'source' => stripslashes($source),
        'note' => stripslashes($note),
        'createdBy' => $_SESSION["user_id"],
        'updatedBy' => $_SESSION["user_id"],
        'createdAt' => date("Y-m-d H:i:s"),
        'updatedAt' => date("Y-m-d H:i:s"),
      );
      $db->insert($data);

      loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['opinion']['link']);
    $OK = true;
  }  
} else {
  $note = '';
  $countryId = 1;
  $source = '';
  $time = '';
}
if (!$OK) postOpinion(
  HOME_URL_LANG . $mmenu['opinion']['link'] . '/opinion-add',
  "add",
  0,
  $time,
  $countryId,
  $source,
  $note,
  $error
);
