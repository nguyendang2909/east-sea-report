<?php
include_once(_F_TEMPLATES . DS . "opinion.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . $mmenu['opinion']['link'] . '">' . $mmenu['opinion']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">Sửa</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

// Get record to edit from db
$opinionId = isset($_GET['id']) ? intval($_GET['id']) : intval($opinionId);
$db->table      = "opinion";
$db->condition  = "`opinionId` = $opinionId";
$db->order      = "";
$db->limit      = 1;
$rows           = $db->select();
if ($db->RowCount == 0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['opinion']['link']);

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'edit') {
  // Validate data
  if (empty($time) || $time == '__/__/____' || empty($countryId) || $countryId == '') $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
      $db->table = "opinion";
      $data = array(
        'time' => date("Y-m-d", strtotime($date->dmYtoYmd($time))),
        'countryId' => intval($countryId),
        'source' => stripslashes($source),
        'note' => stripslashes($note),
        'updatedAt' => date("Y-m-d H:i:s"),
        'updatedBy' =>  $_SESSION["user_id"],
      );
      $db->condition = "`opinionId` = $opinionId";
      $db->update($data);

      loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['opinion']['link']);
    $OK = true;
} else {
  foreach ($rows as $row) {
    $note = $row['note'];
    $countryId = $row['countryId'];
    $source = $row['source'];
    $time = date('d/m/Y', strtotime($row['time']));
  }
}
if (!$OK) postOpinion(
  HOME_URL_LANG . $mmenu['opinion']['link'] . '/opinion-edit',
  "edit",
  $opinionId,
  $time,
  $countryId,
  $source,
  $note,
  $error
);
