<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . $mmenu['opinion']['link'] . '">' . $mmenu['opinion']['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

// Xoa record
if (isset($_POST['tick']) && in_array("opinion;delete", $corePrivilegeSlug['op'])) {
  $tick = empty($_POST['tick']) ? array() : $_POST['tick'];
  $tick = array_filter($tick);
  if (count($tick) > 0) {
    $tick = implode(',', $tick);
    $db->table = "opinion";
    $data = array(
      'isActive' => 0,
      'updatedBy' => $_SESSION["user_id"],
      'updatedAt' => time(),
    );
    $db->condition = "`opinionId` IN ($tick)";
    $db->update($data);
    loadPageSuccess("Đã xoá dữ liệu thành công.", HOME_URL_LANG .  $mmenu['opinion']['link']);
  }
}
?>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-no-border">
      <div class="row">
        <div class="col-xs-12 top-tool">
          <div class="pull-left">
            <a class="btn-add-new not-abs" href="<?php echo HOME_URL_LANG . $mmenu['opinion']['link'] . '/opinion-add'; ?>">Thêm</a>
          </div>
        </div>
      </div>
      <div class="table-responsive">
        <form method="post" id="_ol_delete">
          <table class="table display table-bordered" cellspacing="0" cellpadding="0" id="dataTablesList">
            <thead>
              <tr>
                <th>ID</th>
                <th>Ngày/tháng</th>
                <th>Quốc gia</th>
                <th>Nguồn</th>
                <th>Nội dung</th>
                <th width="80px">Chọn</th>
              </tr>
            </thead>

            <thead>
              <tr>
                <td align="center">-</td>
                <td align="center">-</td>
                <td><input type="text" data-column="2" class="form-control filter"></td>
                <td><input type="text" data-column="3" class="form-control filter"></td>
                <td><input type="text" data-column="4" class="form-control filter"></td>
                <td align="center">-</td>
              </tr>
            </thead>
          </table>

          <!-- Nut xoa -->
          <?php
          if (in_array("field-situation;delete", $corePrivilegeSlug['op']))
            echo '
            <div class="row">
              <div class="col-sm-12" align="right">
                <label class="radio-inline"><input type="checkbox" id="_ol_select_all"  data-toggle="tooltip" data-placement="top" title="Chọn xóa tất cả" ></label>
                <input type="button" class="btn btn-danger btn-round btn-xs ol-confirm" value="Xoá hồ sơ" name="delete">
              </div>
            </div>';
          ?>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#dataTablesList tfoot th').each(function() {
      var title = $(this).text();
      $(this).html('<input type="text" placeholder="Search ' + title + '" />');
    });
    var table = $('#dataTablesList').DataTable({
      language: {
        url: "/js/data-tables/de_DE.txt"
      },
      lengthMenu: [100, 200, 300],
      info: false,
      processing: true,
      serverSide: true,
      ajax: {
        url: '/action.php',
        type: 'GET',
        data: {
          url: 'opinion',
          type: 'load'
        }
      },
      "columns": [{
          data: 'no'
        },
        {
          data: 'time'
        },
        {
          data: 'country'
        },
        {
          data: 'source'
        },
        {
          data: 'note'
        },
        {
          data: 'tool'
        }
      ],
      // Can le cho cot
      fnRowCallback: function(nRow, aData, iDisplayIndex) {
        $('td:eq(0)', nRow).css("text-align", "center");
        $('td:eq(2)', nRow).css("text-align", "center");

        return nRow;
      },

      fnDrawCallback: function() {
        $(".ol-alert-core").boxes('alert', 'Bạn không được phân quyền với chức năng này.');
      },

      order: [0, "asc"],

      aoColumnDefs: [{
        searchable: false,
        orderable: false
      }]
    });

    // Apply the search
    $('.filter').on('change', function() {
      var i = $(this).attr('data-column');
      var v = $(this).val();
      table.columns(i).search(v).draw();
    });
  });

  // Datetimepicker
  $('.input-date').datetimepicker({
    lang: 'vi',
    timepicker: false,
    format: '<?php echo TTH_DATE_FORMAT; ?>'
  });

  // Confirm xoa record
  $(".ol-confirm").click(function() {
    confirm("Tất cả các dữ liệu liên quan sẽ được xóa.\nBạn có muốn thực hiện không?", function() {
      if (this.data == true) document.getElementById("_ol_delete").submit();
    });
  });

  $(".ol-alert-core").boxes('alert', 'Bạn không được phân quyền với chức năng này.');
</script>