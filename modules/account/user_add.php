<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['account']['link'] . '">' . $mmenu['account']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm thành viên</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---
include_once (_F_TEMPLATES . DS . "user.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$date = new DateClass();
$OK = false;
$error = '';
if($typeFunc=='add'){
	$db->table = "core_user";
	$db->condition = "`user_name` LIKE '$user_name'";
	$db->order = "";
	$db->limit = 1;
	$db->select();
	if($db->RowCount>0) $error = '<span class="show-error">Tên đăng nhập này đã tồn tại.</span>';
	else {
		$handleUploadImg = false;
		$file_max_size = FILE_MAX_SIZE;
		$dir_dest = ROOT_DIR . DS . 'uploads' . DS . 'user';
		$file_size = $_FILES['img']['size'];

		if($file_size>0) {
			$imgUp = new Upload($_FILES['img']);

			$imgUp->file_max_size = $file_max_size;
			if ($imgUp->uploaded) {
				$handleUploadImg = true;
				$OK = true;
			}
			else {
				$error = '<span class="show-error">Lỗi tải hình: '.$imgUp->error.'</span>';
			}
		}
		else {
			$handleUploadImg = false;
			$OK = true;
		}

		if($OK) {
			$id_query = 0;
			$full_name = mb_convert_case($full_name, MB_CASE_TITLE, "UTF-8");

            $list = isset($_POST['list']) ? $_POST['list'] : array();
			$db->table = "core_user";
			$data = array(
				'user_name'     => $db->clearText($user_name),
				'password'      => $db->clearText(md5($password)),
				'full_name'     => $db->clearText($full_name),
				'gender'        => intval($gender),
				'birthday'      => $date->dmYtoYmd2($birthday),
				'identity_card' => $db->clearText($identity_card),
				'date_of_issue' => $date->dmYtoYmd2($date_of_issue),
				'email'         => $db->clearText($email),
				'phone'         => $db->clearText($phone),
				'address'       => $db->clearText($address),
				'city'          => intval($city),
				'agency'        => $db->clearText(json_encode(array_values($list))),
				'note'          => $db->clearText($note),
				'is_show'       => intval($is_show),
				'created_time'  => time(),
                'modified_time' => time(),
				'user_id_edit'  => $_SESSION["user_id"]
			);
			$db->insert($data);
			$id_query = $db->LastInsertID;

			//---- Role-User
			$role_arr = isset($_POST['role']) ? $_POST['role'] : array();
            $role_arr = array_keys(array_flip($role_arr));
			if(count($role_arr)>0) {
                for($i=0; $i<count($role_arr); $i++) {
                    $db->table = "role_user";
                    $data = array(
                        'role'          => $role_arr[$i],
                        'user'          => $id_query,
                        'created_time'  => time(),
                        'user_id'       => $_SESSION["user_id"]
                    );
                    $db->insert($data);
                }
            }

			if($handleUploadImg) {
				$stringObj = new String();

				$img_name_file = 'u_' . time() . "_" . md5(uniqid());

				$imgUp->file_new_name_body      = $img_name_file;
				$imgUp->image_resize            = true;
				$imgUp->image_ratio_crop        = true;
				$imgUp->image_y                 = 200;
				$imgUp->image_x                 = 200;
				$imgUp->Process($dir_dest);

				if ($imgUp->processed) {
					$name_img = $imgUp->file_dst_name;
					$db->table = "core_user";
					$data = array(
						'img' => $db->clearText($name_img)
					);
					$db->condition = "user_id = " . $id_query;
					$db->update($data);
				} else {
					loadPageError("Lỗi tải hình: " . $imgUp->error, HOME_URL_LANG . $mmenu['account']['link']);
				}
				$imgUp->file_new_name_body      = 'sm_' . $img_name_file;
				$imgUp->image_resize            = true;
				$imgUp->image_ratio_crop        = true;
				$imgUp->image_y                 = 90;
				$imgUp->image_x                 = 90;
				$imgUp->Process($dir_dest);
				$imgUp->Clean();
			}
			loadPageSuccess("Đã thêm thành viên thành công.", HOME_URL_LANG . $mmenu['account']['link']);
			$OK = true;
		}
	}
}
else {
	$role		    = array();
	$user_name      = '';
	$full_name      = '';
	$gender         = 0;
	$birthday       = '01/01/1990';
	$identity_card  = '';
	$date_of_issue  = '';
	$email          = '';
	$phone          = '';
	$address        = '';
	$city           = 550000;
	$list           = isset($_GET['agency']) ? array(intval($_GET['agency'])) : array();
	$note           = '';
	$img            = '';
	$is_show        = 1;
}
if(!$OK) memberUser(HOME_URL_LANG . $mmenu['account']['link'] . '/user-add', "add", 0, $role, $user_name, $full_name, $gender, $birthday, $identity_card, $date_of_issue, $email, $phone, $address, $city, $list, $note, $is_show, $img, $error);
