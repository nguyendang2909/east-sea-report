<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['calendar']['link'] . $mmenu['calendar']['sub'][3]['link'] . '">' . $mmenu['calendar']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['calendar']['link'] . $mmenu['calendar']['sub'][3]['link'] . '">' . $mmenu['calendar']['sub'][3]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm lịch</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "calendar.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
$date = new DateClass();
if($typeFunc=='add'){
	$week 	= isset($_POST['week']) ? intval($_POST['week']) : 0;
	$month 	= isset($_POST['month']) ? intval($_POST['month']) : 0;
	$year 	= isset($_POST['year']) ? intval($_POST['year']) : 0;
    if(empty($_FILES['files']['size'])) $error = '<span class="show-error">Vui lòng chọn tệp tin.</span>';
	else {
		$file_max_size	= FILE_MAX_SIZE;
		$dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'calendar' . DS;
		
		$file_name      = 'os' . time() . '_' . md5(microtime(true));
        $file_size      = $_FILES['files']['size'];
        if ($file_size > 0) {
            $fileUp = new Upload($_FILES['files']);
            $fileUp->file_max_size = $file_max_size;
            $fileUp->file_new_name_body = $file_name;
            $fileUp->Process($dir_dest);
            if($fileUp->processed) {
                $file_name = $fileUp->file_dst_name;
                $OK = true;
            } else {
				$error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
            }
        } else {
            $OK = true;
            $file_name = '-no-';
        }
		
		if($OK) {			
			$db->table = "calendar";
			$data = array(
				'type'  	    => intval($type),
				'week'       	=> $week,
				'month'       	=> $month,
				'year'       	=> $year,
				'files'       	=> $db->clearText($file_name),
				'note'          => $db->clearText($note),
				'created_time'  => time(),
				'modified_time' => time(),
				'user_id'       =>  $_SESSION["user_id"]
			);
			$db->insert($data);
			
			loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['calendar']['link'] . $mmenu['calendar']['sub'][3]['link']);
		}
		$OK = true;
	}
}
else {
    $type  		= 1;
    $week    	= date("W", time());
    $month     	= date("m", time());
    $year     	= date("Y", time());
    $files      = "";
    $note       = "";
}
if(!$OK) calendar(HOME_URL_LANG . $mmenu['calendar']['link'] . '/calendar-add', "add", 0, $type, $week, $month, $year, $files, $note, $error);