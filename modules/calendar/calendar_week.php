<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['calendar']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">' . $mmenu['calendar']['sub'][2]['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-no-border">			
			<div class="panel-heading">
				<i class="fa fa-calendar fa-fw"></i> Lịch công tác tuần
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<?php
				$week = date("W", time());
				$year = date("Y", time());
				$db->table = "calendar";
				$db->condition = "`is_active` = 1 AND `type` = 1 AND `week` = $week AND `year` = $year";
				$db->order = "`created_time` DESC";
				$db->limit = 1;
				$rows = $db->select();
				if($db->RowCount>0) {
					foreach($rows as $row) {
						$file = '';
						if(file_exists(ROOT_DIR  . DS . 'uploads' . DS . 'calendar' . DS . $row['files']) && ($row['files']!='')) {
							$file = HOME_URL . '/uploads/calendar/' . stripslashes($row['files']);
							echo '<iframe class="view-file" src="' . $file . '" allowfullscreen webkitallowfullscreen></iframe>';
						}
					}
				}
				?>
			</div>
		</div>
	</div>
</div>