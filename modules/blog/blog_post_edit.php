<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][01]['link'] . '">' . $mmenu['blog']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][0]['link'] . '">' . $mmenu['blog']['sub'][0]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa bài viết</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "blog_post.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$blog_post_id = isset($_GET['id']) ? intval($_GET['id']) : intval($blog_post_id);
$db->table = "blog_post";
$db->condition = "`blog_post_id` = $blog_post_id";
$db->order = "";
$db->limit = 1;
$rows = $db->select();
if($db->RowCount==0) loadPageError("Mục không tồn tại.", HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][0]['link']);

$OK = false;
$error = '';
if($typeFunc=='edit'){
    $tags = isset($_POST['tags']) ? $_POST['tags'] : array();
	if(empty($name)) $error = '<span class="show-error">Vui lòng nhập tên bài viết.</span>';
	elseif(empty($blog)) $error = '<span class="show-error">Vui lòng chọn chuyên mục bài viết.</span>';
    else {
		$handleUploadImg = false;
		$file_max_size  = FILE_MAX_SIZE;
		$dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'blog' . DS;
		$file_size      = $_FILES['img']['size'];

		if($file_size>0) {
			$imgUp = new Upload($_FILES['img']);

			$imgUp->file_max_size = $file_max_size;
			if ($imgUp->uploaded) {
				$handleUploadImg = true;
				$OK = true;
			}
			else {
				$error = '<span class="show-error">Lỗi tải hình: ' . $imgUp->error . '</span>';
			}
		}
		else {
			$handleUploadImg = false;
			$OK = true;
		}

		if($OK) {
            $stringObj = new String();

            $slug = (empty($slug)) ? $name : $slug;
            $slug = $stringObj->getSlug($slug);
            $db->table = "blog_post";
            $db->condition = "`slug` LIKE '$slug'";
            $db->order = "";
            $db->limit = "";
            $db->select();
            if($db->RowCount > 0) {
                foreach ($rows as $row) {
                    if($row['blog_post_id']!=$blog_post_id)
                        $slug = $slug . '-' . $stringObj->getSlug(getRandomString(10));
                }
            }

			$id_query = 0;
			$db->table = "blog_post";
			$data = array(
                'blog'          => $db->clearText($blog),
				'name'          => $db->clearText($name),
                'slug'          => $db->clearText($slug),
                'comment'       => $db->clearText($comment),
                'content'       => $db->clearText($content),
                'tags'          => $db->clearText(json_encode($tags)),
				'title'         => $db->clearText($title),
				'description'   => $db->clearText($description),
				'keywords'      => $db->clearText($keywords),
                'hot'           => intval($hot),
				'modified_time' => time(),
				'user_id'       => $_SESSION["user_id"]
			);
            $db->condition = "`blog_post_id` = $blog_post_id";
            $db->update($data);
			$id_query = $blog_post_id;

			if ($handleUploadImg) {
                if(glob($dir_dest .'*'. $img)) array_map("unlink", glob($dir_dest .'*'.$img));
                $name_image = $stringObj->getSlug(mb_substr($name, 0, 50, 'UTF-8') . '-' . $id_query . '-' . time());

                $imgUp->file_new_name_body = $name_image;
				$imgUp->image_resize = true;
				$imgUp->image_ratio_crop = true;
                $imgUp->image_x = 490;
				$imgUp->image_y = 256;
				$imgUp->Process($dir_dest);

				if ($imgUp->processed) {
					$db->table = "blog_post";
					$data = array(
                        'img' => $db->clearText($imgUp->file_dst_name)
					);
					$db->condition = "`blog_post_id` = $id_query";
					$db->update($data);
				} else {
					loadPageError("Lỗi tải hình: " . $imgUp->error, HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][0]['link']);
				}

                $imgUp->file_new_name_body = 'post-' . $name_image;
                $imgUp->image_resize = true;
                $imgUp->image_ratio_crop = true;
                $imgUp->image_x = 320;
                $imgUp->image_y = 220;
                $imgUp->Process($dir_dest);

				$imgUp->Clean();
			}

			loadPageSuccess("Đã chỉnh sửa liệu thành công.", HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][0]['link']);
			$OK = true;
		}
	}
}
else {
    foreach($rows as $row) {
        $blog           = intval($row['blog']);
        $name           = $row['name'];
        $slug           = $row['slug'];
        $img            = $row['img'];
        $comment        = $row['comment'];
        $content        = $row['content'];
        $upload_img_id  = doubleval($row['upload']);
        $tags           = json_decode($row['tags']);
        $hot		    = intval($row['hot']);
        $title          = $row['title'];
        $description    = $row['description'];
        $keywords       = $row['keywords'];
    }
}
if(!$OK) blogCore(HOME_URL_LANG . $mmenu['blog']['link'] . '/blog-post-edit', 'edit', $blog_post_id, $blog, $name, $slug, $img, $comment, $content, $hot, $title, $description, $keywords, $upload_img_id, $tags, $error);