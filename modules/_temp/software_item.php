<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function softwareItem($act, $typeFunc, $software_item_id, $software, $title, $perform, $owner, $address, $agency, $user, $history, $note, $files, $error) {
	global $db, $mmenu;
	if(empty($agency)) $agency = "''";
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-hdd-o fa-fw"></i> Nội dung phần mềm
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>" />
						<input type="hidden" name="software_item_id" value="<?php echo $software_item_id?>" />
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
							<tr>
								<td width="170px" align="right"><label class="form-lb-tp">Nhóm phần mềm:</label></td>
								<td><?php echo listCategory($software);?></td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Tên phần mềm:</label></td>
                                <td><input class="form-control" type="text" name="title" value="<?php echo stripslashes($title);?>" required></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Chức năng:</label></td>
                                <td><input class="form-control" type="text" name="perform" value="<?php echo stripslashes($perform);?>"></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Chủ sở hữu:</label></td>
                                <td><input class="form-control" type="text" name="owner" value="<?php echo stripslashes($owner);?>"></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Vị trí triển khai:</label></td>
                                <td><input class="form-control" type="text" name="address" value="<?php echo stripslashes($address);?>"></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Đơn vị:</label></td>
                                <td><input class="form-control easyui-combotree" data-options="url:'/action.php?url=list_agency',method:'get',value:<?php echo $agency;?>,animate:true,lines:true" name="agency" style="width: 100%;"></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Nhân sự:</label></td>
                                <td><?php echo selectUserSingle(0, $user);?></td>
                            </tr>
                            <tr>
                                <td align="right" class="ver-top"><label class="form-lb-tp">Lịch sử sử dụng:</label></td>
                                <td><textarea class="form-control" rows="2" name="history"><?php echo stripslashes($history)?></textarea></td>
                            </tr>
                            <tr>
                                <td align="right" class="ver-top"><label class="form-lb-tp">Ghi chú:</label></td>
                                <td><textarea class="form-control" rows="3" name="note"><?php echo stripslashes($note)?></textarea></td>
                            </tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Têp tin:</label></td>
								<td><input class="form-control file" type="file" name="files" data-show-upload="false" data-show-preview="false" data-max-file-count="1" value="<?php echo $files;?>" placeholder="Chọn file..."></td>
							</tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][3]['sub'][0]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('.file').fileinput({
	<?php if($files !='' && $files !='-no-') echo 'initialPreview: ["' . $files . '"]'; ?>
});
</script>
<?php
}
function listCategory($choice) {
    global $db;

    $result = '<select name="software" class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn nhóm..." required>';
    $db->table = "software";
    $db->condition = "`is_active` = 1";
    $db->order = "`title` ASC";
    $db->limit = "";
    $rows = $db->select("`software_id`, `title`");
    foreach($rows as $row) {
        $selected = '';
        if(intval($row['software_id'])==$choice) $selected = ' selected';
        $result .= '<option value="' . intval($row['software_id']) . '"' . $selected . '>' . stripslashes($row['title']) . '</option>';
    }
    $result .= '</select>';

    return $result;
}