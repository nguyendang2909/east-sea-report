<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

function postShip(
  $act,
  $typeFunc,
  $shipId,
  $name,
  $shipTypeId,
  $countryId,
  $mmsi,
  $callsign,
  $imo,
  $length,
  $width,
  $height,
  $weight,
  $maxSpeed,
  $capital,
  $sailorCount,
  $workingRange,
  $shipWeaponId,
  $mapTypeId,
  $file,
  $note,
  $error
) {
  global $mmenu;
  if (empty($agency)) $agency = "''";
?>
  <div class="row">
    <div class="col-lg-6 col-md-9">
      <div class="panel">
        <div class="panel-heading">
          <i class="fa fa-sitemap fa-fw"></i> Đối tượng tàu
          <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
            <a href="javascript:;" class="fa fa-chevron-down"></a>
            <a href="javascript:;" class="fa fa-eye"></a>
            <a href="javascript:;" class="fa fa-compress"></a>
            <a href="javascript:;" class="fa fa-times"></a>
          </span>
        </div>
        <div class="panel-body">
          <div class="form-responsive">
            <form action="<?php echo $act ?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
              <input type="hidden" name="typeFunc" value="<?php echo $typeFunc ?>" />
              <input type="hidden" name="shipId" value="<?php echo $shipId ?>" />
              <div class="panel-show-error">
                <?php echo $error ?>
              </div>
              <table class="table table-no-border table-hover">

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Tên tàu(*):</label></td>
                  <td><input class="form-control" type="text" name="name" value="<?php echo stripslashes($name); ?>" required></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Loại tàu(*):</label></td>
                  <td><?php echo selectShipTypeSingle($shipTypeId, 'required'); ?></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Icon(*):</label></td>
                  <td><?php echo selectMapTypeSingle($mapTypeId, 'required'); ?></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Quốc gia(*):</label></td>
                  <td><?php echo selectCountrySingle($countryId, 'required'); ?></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">MMSI(*):</label></td>
                  <td><input class="form-control" type="text" name="mmsi" value="<?php echo intval($mmsi); ?>" required></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Callsign:</label></td>
                  <td><input class="form-control" type="text" name="callsign" value="<?php echo stripslashes($callsign); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">IMO:</label></td>
                  <td><input class="form-control" type="text" name="imo" value="<?php echo stripslashes($imo); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Dài (m):</label></td>
                  <td><input class="form-control" type="text" name="length" value="<?php echo floatval($length); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Rộng (m):</label></td>
                  <td><input class="form-control" type="text" name="width" value="<?php echo floatval($width); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Cao (m):</label></td>
                  <td><input class="form-control" type="text" name="height" value="<?php echo floatval($height); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Trọng lượng (kg):</label></td>
                  <td><input class="form-control" type="text" name="weight" value="<?php echo floatval($weight); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Tốc độ tối đa (km/h):</label></td>
                  <td><input class="form-control" type="text" name="maxSpeed" value="<?php echo floatval($maxSpeed); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Thuyền trưởng:</label></td>
                  <td><input class="form-control" type="text" name="capital" value="<?php echo stripslashes($capital); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Số lượng thủy thủ:</label></td>
                  <td><input class="form-control" type="text" name="sailorCount" value="<?php echo stripslashes($sailorCount); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Thời gian hoạt động:</label></td>
                  <td><input class="form-control" type="text" name="workingRange" value="<?php echo stripslashes($workingRange); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Vũ khí:</label></td>
                  <td><?php echo selectShipWeaponSingle($shipWeaponId, 'required'); ?></td>
                </tr>



                <tr>
                  <td align="right"><label class="form-lb-tp">Ảnh:</label></td>
                  <td><input class="form-control file" type="file" name="file" data-show-upload="false" data-show-preview="false" data-max-file-count="1" value="<?php echo $file; ?>" placeholder="Chọn file..."></td>
                </tr>

                <tr>
                  <td align="right" class="ver-top"><label class="form-lb-tp">Ghi chú:</label></td>
                  <td><textarea class="form-control" rows="3" name="note"><?php echo stripslashes($note) ?></textarea></td>
                </tr>

                <tr>
                  <td colspan="2" class="form-ol-btn-tzc">
                    <button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
                    <button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
                    <button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship-type'; ?>'">Thoát</button>
                  </td>
                </tr>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    $('.file').fileinput({
      <?php if ($file && $file !== '' && $file != '-no-') echo 'initialPreview: ["' . $file . '"]'; ?>
    });
  </script>

<?php
}
?>