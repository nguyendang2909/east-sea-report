<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

function postFieldSituation(
  $act,
  $typeFunc,
  $fieldSituationId,
  $time,
  $localId,
  $countryId,
  $shipId,
  $purpose,
  $note,
  $file,
  $error
) {
  global $mmenu;
  if (empty($agency)) $agency = "''";
?>
  <div class="row">
    <div class="col-lg-6 col-md-9">
      <div class="panel">
        <div class="panel-heading">
          <i class="fa fa-sitemap fa-fw"></i> Tình hình trên thực địa
          <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
            <a href="javascript:;" class="fa fa-chevron-down"></a>
            <a href="javascript:;" class="fa fa-eye"></a>
            <a href="javascript:;" class="fa fa-compress"></a>
            <a href="javascript:;" class="fa fa-times"></a>
          </span>
        </div>
        <div class="panel-body">
          <div class="form-responsive">
            <form action="<?php echo $act ?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
              <input type="hidden" name="typeFunc" value="<?php echo $typeFunc ?>" />
              <input type="hidden" name="fieldSituationId" value="<?php echo $fieldSituationId ?>" />
              <div class="panel-show-error">
                <?php echo $error ?>
              </div>
              <table class="table table-no-border table-hover">

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Khu vực(*):</label></td>
                  <td><?php echo selectSingleLocal($localId); ?></td>
                </tr>

                <tr>
                  <td align="right"><label class="form-lb-tp">Ngày/tháng(*):</label></td>
                  <td><input class="form-control input-date report-date-picker" type="text" name="time" style="width: 150px;" value="<?php echo stripslashes($time); ?>" maxlength="16" autocomplete="off" required></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Quốc gia(*):</label></td>
                  <td><?php echo selectCountrySingle($countryId, 'required'); ?></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Lực lượng, phương tiện(*):</label></td>
                  <td><?php echo selectShipSingle($shipId); ?></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Mục đích:</label></td>
                  <td><input class="form-control" type="text" name="purpose" value="<?php echo $purpose; ?>"></td>
                </tr>

                <tr>
                  <td align="right" class="ver-top"><label class="form-lb-tp">Nhận định:</label></td>
                  <td><textarea class="form-control" rows="3" name="note"><?php echo stripslashes($note) ?></textarea></td>
                </tr>

                <tr>
                  <td align="right"><label class="form-lb-tp">Ảnh:</label></td>
                  <td><input class="form-control file" type="file" name="file" data-show-upload="false" data-show-preview="false" data-max-file-count="1" value="<?php echo $file; ?>" placeholder="Chọn file..."></td>
                </tr>

                <tr>
                  <td colspan="2" class="form-ol-btn-tzc">
                    <button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
                    <button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
                    <button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['field-situation']['link']; ?>'">Thoát</button>
                  </td>
                </tr>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    $('.input-date').datetimepicker({
      mask: '39/19/9999',
      lang: 'vi',
      timepicker: false,
      format: '<?php echo TTH_DATE_FORMAT; ?>',
      maxDate: '+01/01/1970'
    });
  </script>

  <script>
    $('.file').fileinput({
      <?php if ($file && $file !== '' && $file != '-no-') echo 'initialPreview: ["' . $file . '"]'; ?>,
    });
  </script>

<?php
}