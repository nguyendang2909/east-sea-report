<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function agencyCore($act, $typeFunc, $agency_id, $symbol, $name, array $manager, $address, $phone, $email, $parent, $img, $note, $is_show, $error) {
	global $mmenu;
?>
<div class="row">
	<div class="col-lg-7 col-md-10">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-bank fa-fw"></i> Thông tin đơn vị
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" enctype="multipart/form-data" class="form-ol-3w">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc;?>">
						<input type="hidden" name="agency_id" value="<?php echo $agency_id;?>">
						<input type="hidden" name="img" value="<?php echo $img;?>">
						<div class="panel-show-error"><?php echo $error;?></div>
						<table class="table table-no-border table-hover">
							<tr>
								<td width="150px" align="right"><label class="form-lb-tp">Kí hiệu:</label></td>
								<td><input class="form-control" type="text" name="symbol" maxlength="10" value="<?php echo stripslashes($symbol)?>" required="required"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Tên đơn vị:</label></td>
								<td><input class="form-control" type="text" name="name" maxlength="255" value="<?php echo stripslashes($name)?>" required="required"></td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Quản lý đơn vị:</label></td>
                                <td><?php echo selectUserMultiple(0, $manager);?></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Đơn vị cấp trên:</label></td>
                                <td><?php echo parentAgency($parent, $agency_id);?></td>
                            </tr>
							<tr>
								<td align="right" class="ver-top"><label class="form-lb-tp">Hình đại diện:</label></td>
								<td>
									<input class="form-control file file-img" type="file" name="img" data-show-upload="false" data-max-file-count="1" accept="image/*">
								</td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Địa chỉ:</label></td>
								<td><input class="form-control" type="text" name="address" maxlength="255" value="<?php echo stripslashes($address)?>"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Số điện thoại:</label></td>
								<td><input class="form-control" type="text" name="phone" maxlength="20" value="<?php echo stripslashes($phone)?>"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Email:</label></td>
								<td><input class="form-control" type="text" name="email" maxlength="255" value="<?php echo stripslashes($email)?>"></td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Lịch sử hình thành:</label></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2"><textarea class="form-control" name="note" id="note"><?php echo stripslashes($note)?></textarea></td>
                            </tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Hiển thị:</label></td>
								<td>
									<label class="radio-inline"><input type="radio" name="is_show" value="0" <?php echo $is_show==0?"checked":""?>> Đóng</label>
									<label class="radio-inline"><input type="radio" name="is_show" value="1" <?php echo $is_show==1?"checked":""?>> Mở</label>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['agency']['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
    CKEDITOR.replace('note', {
        height: 150,
        toolbar: [
            [ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
            [ 'FontSize', 'TextColor', 'BGColor' ]
        ]
    });
	$('.file-img').fileinput({
		<?php if($img!='-no-' && $img!='') { ?>
		initialPreview: [
			'<img src="../uploads/agency/<?php echo $img?>" class="file-preview-image" alt="<?php echo $img;?>">'
		],
		<?php } ?>
		allowedFileExtensions : ['jpg', 'png','gif']
	});
</script>
<?php
}

function parentAgency($par, $choice) {
	global $db;
    $result = $selected = '';
    $result .= '<select class="form-control selectpicker" name="parent" required>';

    if($par==0) $selected = ' selected';
    $result .= '<option value="0"' . $selected . '>ROOT...</option>';
    $result .= loadItemAgencyP($db, 0, 0, $par, $choice);

    $result .='</select>';

    return $result;
}

function loadItemAgencyP($db, $level, $parent, $par, $choice) {
    $child = getAgencyElementPlus($choice);
    $child = explode(',', $child);
    $result = $space = '';
	$db->table = "agency";
	$db->condition = "`parent` = $parent";
	$db->order = "`sort` ASC";
	$db->limit = "";
	$rows = $db->select();
	foreach($rows as $row) {
        $space = '&nbsp; ';
        for($i=0; $i<$level; $i++) {
            $space = $space . '&nbsp; ';
        }
        $selected = $disabled = '';
        if($par==intval($row["agency_id"])) $selected = ' selected';
        if(($choice > 0) && in_array($row["agency_id"], $child)) $disabled = ' disabled';

        $result .= '<option value="' . intval($row["agency_id"]) . '"' . $selected . $disabled . '>' . $space . '&rarr; ' . stripslashes($row["name"]) . '</option>';
        $result .= loadItemAgencyP($db, $level+1, $row["agency_id"], $par, $choice);
	}

    return $result;
}

function sortAcs($parent){
	global $db;
	$db->table = "agency";
	$db->condition = "`parent` = " . intval($parent);
	$db->order = "";
    $db->limit = 1;
    $rows = $db->select("COUNT(*) AS `count`");
    $total = $db->RowCount;
    foreach($rows as $row) {
        $total = $row['count'];
    }
	return $total;
}
