<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

function postEastSeaReport(
  $act,
  $typeFunc,
  $startDate,
  $endDate,
  $error
) {
  global $mmenu;
  if (empty($agency)) $agency = "''";
?>
  <div class="row">
    <div class="col-lg-12 col-md-9">
      <div class="panel">
        <div class="panel-heading text-center">
          <i class="fa fa-sitemap fa-fw"></i> Báo cáo Biển Đông
          <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
            <a href="javascript:;" class="fa fa-chevron-down"></a>
            <a href="javascript:;" class="fa fa-eye"></a>
            <a href="javascript:;" class="fa fa-compress"></a>
            <a href="javascript:;" class="fa fa-times"></a>
          </span>
        </div>
        <div class="panel-body">
          <div class="form-responsive">
            <form action="<?php echo HOME_URL_LANG?>:3000/report" method="get" class="form-ol-3w" enctype="multipart/form-data">
              <input type="hidden" name="typeFunc" value="<?php echo $typeFunc ?>" />
              <div class="panel-show-error">
                <?php echo $error ?>
              </div>
              <table class="table table-no-border table-hover">
                <tr>
                  <td align="right" width="50%"><label class="form-lb-tp">Từ ngày(*):</label></td>
                  <td><input class="form-control input-date report-date-picker" type="text" name="startDate" style="width: 100px;" value="<?php echo stripslashes($startDate); ?>" maxlength="16" autocomplete="off" required></td>
                </tr>

                <tr>
                  <td align="right"><label class="form-lb-tp">Đến ngày(*):</label></td>
                  <td><input class="form-control input-date report-date-picker" type="text" name="endDate" style="width: 100px;" value="<?php echo stripslashes($endDate); ?>" maxlength="16" autocomplete="off" required></td>
                </tr>

                <tr>
                  <td colspan="2" class="form-ol-btn-tzc">
                    <button type="submit" class="btn btn-primary btn-round">Xem báo cáo</button> &nbsp;
                    <button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
                    <button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['east-sea-report']['link']; ?>'">Thoát</button>
                  </td>
                </tr>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    $('.input-date').datetimepicker({
      mask: '39/19/9999',
      lang: 'vi',
      timepicker: false,
      format: '<?php echo TTH_DATE_FORMAT; ?>',
      maxDate: '31/12/2050'
    });
  </script>

<?php
}
