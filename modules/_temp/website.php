<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

function website(
  $act,
  $typeFunc,
  $websiteId,
  $name,
  $place,
  $websiteUrl,
  $ip,
  $latitude,
  $longitude,
  $mapTypeId,
  $owner,
  $isWarning,
  $file,
  $note,
  $error
) {
  global $mmenu;
  if (empty($agency)) $agency = "''";
?>
  <div class="row">
    <div class="col-lg-6 col-md-9">
      <div class="panel">
        <div class="panel-heading">
          <i class="fa fa-sitemap fa-fw"></i> Trang cổng điện tử
          <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
            <a href="javascript:;" class="fa fa-chevron-down"></a>
            <a href="javascript:;" class="fa fa-eye"></a>
            <a href="javascript:;" class="fa fa-compress"></a>
            <a href="javascript:;" class="fa fa-times"></a>
          </span>
        </div>
        <div class="panel-body">
          <div class="form-responsive">
            <form action="<?php echo $act ?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
              <input type="hidden" name="typeFunc" value="<?php echo $typeFunc ?>" />
              <input type="hidden" name="websiteId" value="<?php echo $websiteId ?>" />
              <div class="panel-show-error">
                <?php echo $error ?>
              </div>
              <table class="table table-no-border table-hover">

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Tên website(*):</label></td>
                  <td><input class="form-control" type="text" name="name" value="<?php echo stripslashes($name); ?>" required></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Địa bàn:</label></td>
                  <td><input class="form-control" type="text" name="place" value="<?php echo stripslashes($place); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Địa chỉ url(*):</label></td>
                  <td><input class="form-control" type="text" name="websiteUrl" value="<?php echo stripslashes($websiteUrl); ?>" required></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Địa chỉ IP(*):</label></td>
                  <td><input class="form-control" type="text" name="ip" value="<?php echo stripslashes($ip); ?>" required></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Vĩ độ:</label></td>
                  <td><input class="form-control" type="text" name="latitude" value="<?php echo stripslashes($latitude); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Kinh độ:</label></td>
                  <td><input class="form-control" type="text" name="longitude" value="<?php echo stripslashes($longitude); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Icon(*):</label></td>
                  <td><?php echo selectMapTypeSingle($mapTypeId, 'required'); ?></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Chủ sỡ hữu:</label></td>
                  <td><input class="form-control" type="text" name="owner" value="<?php echo stripslashes($owner); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Cảnh báo:</label></td>
                  <td><?php echo selectWarning($isWarning, 'required'); ?></td>
                </tr>

                <tr>
                  <td align="right"><label class="form-lb-tp">Têp tin:</label></td>
                  <td><input class="form-control file" type="file" name="file" data-show-upload="false" data-show-preview="false" data-max-file-count="1" value="<?php echo $file; ?>" placeholder="Chọn file..."></td>
                </tr>

                <tr>
                  <td align="right" class="ver-top"><label class="form-lb-tp">Ghi chú:</label></td>
                  <td><textarea class="form-control" rows="3" name="note"><?php echo stripslashes($note) ?></textarea></td>
                </tr>

                <tr>
                  <td colspan="2" class="form-ol-btn-tzc">
                    <button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
                    <button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
                    <button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['network']['link'] . '/website'; ?>'">Thoát</button>
                  </td>
                </tr>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    $('.file').fileinput({
      <?php if ($file && $file !== '' && $file != '-no-') echo 'initialPreview: ["' . $file . '"]'; ?>,
    });
  </script>

<?php
}
?>