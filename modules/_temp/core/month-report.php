<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
global  $role_id;
?>

<form id="core_month_report" method="post" onsubmit="return core_dashboard('core_month_report', 'month-report');">
	<?php
	echo showCoreMonthReport($role_id);
	?>
</form>
