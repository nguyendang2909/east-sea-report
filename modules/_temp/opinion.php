<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

function postOpinion(
  $act,
  $typeFunc,
  $opinionId,
  $time,
  $countryId,
  $source,
  $note,
  $error
) {
  global $mmenu;
  if (empty($agency)) $agency = "''";
?>
  <div class="row">
    <div class="col-lg-6 col-md-9">
      <div class="panel">
        <div class="panel-heading">
          <i class="fa fa-sitemap fa-fw"></i> Quan điểm, chính sách, lập trường
          <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
            <a href="javascript:;" class="fa fa-chevron-down"></a>
            <a href="javascript:;" class="fa fa-eye"></a>
            <a href="javascript:;" class="fa fa-compress"></a>
            <a href="javascript:;" class="fa fa-times"></a>
          </span>
        </div>
        <div class="panel-body">
          <div class="form-responsive">
            <form action="<?php echo $act ?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
              <input type="hidden" name="typeFunc" value="<?php echo $typeFunc ?>" />
              <input type="hidden" name="opinionId" value="<?php echo $opinionId ?>" />
              <div class="panel-show-error">
                <?php echo $error ?>
              </div>
              <table class="table table-no-border table-hover">

                <tr>
                  <td align="right"><label class="form-lb-tp">Ngày/tháng(*):</label></td>
                  <td><input class="form-control input-date report-date-picker" type="text" name="time" style="width: 150px;" value="<?php echo stripslashes($time); ?>" maxlength="16" autocomplete="off" required></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Quốc gia(*):</label></td>
                  <td><?php echo selectCountrySingle($countryId, 'required'); ?></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Nguồn:</label></td>
                  <td><input class="form-control" type="text" name="source" value="<?php echo $source; ?>"></td>
                </tr>

                <tr>
                  <td align="right" class="ver-top"><label class="form-lb-tp">Nội dung:</label></td>
                  <td><textarea class="form-control" rows="3" name="note"><?php echo stripslashes($note) ?></textarea></td>
                </tr>

                <tr>
                  <td colspan="2" class="form-ol-btn-tzc">
                    <button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
                    <button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
                    <button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['opinion']['link']; ?>'">Thoát</button>
                  </td>
                </tr>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    $('.input-date').datetimepicker({
      mask: '39/19/9999',
      lang: 'vi',
      timepicker: false,
      format: '<?php echo TTH_DATE_FORMAT; ?>',
      maxDate: '+01/01/1970'
    });
  </script>

<?php
}