<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function trackingSubject($act, $typeFunc, $subject_id, $name, $age, $address, $religion, $faction, $social, $local, array $friends, $note, $error) {
	global $mmenu;
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-dot-circle-o fa-fw"></i> Thông tin đối tượng theo dõi
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>" />
						<input type="hidden" name="subject_id" value="<?php echo $subject_id?>" />
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
							<tr>
								<td align="right" width="150px"><label class="form-lb-tp">Tên đối tượng:</label></td>
                                <td><input class="form-control" type="text" name="name" value="<?php echo stripslashes($name);?>" autocomplete="off" required></td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Tuổi:</label></td>
                                <td><input class="form-control auto-number" type="text" name="age" style="width: 150px;" value="<?php echo $age?>" data-a-sep="" data-v-max="9999" data-v-min="0" autocomplete="off"></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Quê quán:</label></td>
                                <td><input class="form-control" type="text" name="address" value="<?php echo stripslashes($address);?>"></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Tôn giáo:</label></td>
                                <td><input class="form-control" type="text" name="religion" value="<?php echo stripslashes($religion);?>"></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Đảng phái:</label></td>
                                <td><input class="form-control" type="text" name="faction" value="<?php echo stripslashes($faction);?>"></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Mạng xã hội:</label></td>
                                <td><input class="form-control" type="text" name="social" value="<?php echo stripslashes($social);?>"></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Địa bàn hoạt động:</label></td>
                                <td><?php echo listLocal($local);?></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Bạn bè:</label></td>
                                <td><?php echo listFriends($friends);?></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Lịch sử bản thân:</label></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2"><textarea id="note" class="form-control" rows="3" name="note"><?php echo stripslashes($note)?></textarea></td>
                            </tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][1]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    CKEDITOR.replace('note', {
        height: 150,
        toolbar: [
            [ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
            [ 'FontSize', 'TextColor', 'BGColor' ],
            [ 'Image' ]
        ]
    });
</script>
<?php
}

function listLocal($choice) {
    global $db;
    $result = '';

    $result .= '<select name="local" class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn địa bàn...">';
    $db->table = "local";
    $db->condition = "`is_active` = 1";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows = $db->select();
    if($db->RowCount>0) {
        foreach($rows as $row) {
            $selected = '';
            if ($row["local_id"]==$choice) $selected = ' selected';

            $result .= '<option value="' . intval($row["local_id"]) . '"' . $selected . '>' . stripslashes($row["title"]) . '</option>';
        }
    }
    $result .= '</select>';
    return $result;
}

function listFriends(array $choice) {
    global $db;
    $result = '';

    $result .= '<select name="friends[]" class="form-control selectpicker" multiple data-live-search="true" data-selected-text-format="count" data-live-search-placeholder="Tìm..." title="Chọn bạn bè đối tượng...">';
    $db->table = "subject";
    $db->condition = "`is_active` = 1";
    $db->order = "`name` ASC";
    $db->limit = "";
    $rows = $db->select();
    if($db->RowCount>0) {
        foreach($rows as $row) {
            $selected = '';
            if (in_array($row["subject_id"], $choice)) $selected = ' selected';

            $result .= '<option value="' . intval($row["subject_id"]) . '"' . $selected . '>' . stripslashes($row["name"]) . ' (' . $row["age"] . ' tuổi)</option>';
        }
    }
    $result .= '</select>';
    return $result;
}