<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function documentItem($act, $typeFunc, $document_item_id, $document, $code, $title, $comment, $files, $error) {
	global $db, $link_ol,  $link_op;
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-cloud fa-fw"></i> Nội dung tài liệu
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>" />
						<input type="hidden" name="document_item_id" value="<?php echo $document_item_id?>" />
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
							<tr>
								<td width="170px" align="right"><label class="form-lb-tp">Danh mục tài sản:</label></td>
								<td><?php echo listAsset($document);?></td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Mã tài liệu:</label></td>
                                <td><input class="form-control" type="text" name="code" value="<?php echo stripslashes($code);?>" maxlength="50" required></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Tiêu đề:</label></td>
                                <td><input class="form-control" type="text" name="title" value="<?php echo stripslashes($title);?>" required></td>
                            </tr>
                            <tr>
                                <td align="right" class="ver-top"><label class="form-lb-tp">Ghi chú:</label></td>
                                <td>
                                    <textarea class="form-control" rows="3" name="comment"><?php echo stripslashes($comment)?></textarea>
                                </td>
                            </tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Têp hồ sơ:</label></td>
								<td><input class="form-control file" type="file" name="files" data-show-upload="false" data-show-preview="false" data-max-file-count="1" value="<?php echo $files;?>" placeholder="Chọn file..."></td>
							</tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][0];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('.input-date').datetimepicker({
    mask: '39/19/9999',
    lang: 'vi',
    timepicker: false,
    format: '<?php echo TTH_DATE_FORMAT;?>',
    maxDate: '+01/01/1970'
});
$('.file').fileinput({
	<?php if($files !='' && $files !='-no-') echo 'initialPreview: ["' . $files . '"]'; ?>
});
</script>
<?php
}

function listAsset($choice) {
    global $db;

    $result = '<select name="document" class="selectpicker" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn danh mục..." required>';
    $db->table = "parents";
    $db->condition = "`type` LIKE 'document' AND `is_active` = 1";
    $db->order = "`created_time` DESC";
    $db->limit = "";
    $rows_p = $db->select();
    foreach ($rows_p as $row_p) {
        $db->table = "document";
        $db->condition = "`is_active` = 1 AND `parent` = " . intval($row_p['parents_id']);
        $db->order = "`created_time` DESC";
        $db->limit = "";
        $rows = $db->select();
        if($db->RowCount>0) {
            $result .= '<optgroup label="' . stripslashes($row_p['title']) . '">';
            foreach($rows as $row) {
                $selected = '';
                if(intval($row['document_id'])==$choice) $selected = ' selected';
                $result .= '<option value="' . intval($row['document_id']) . '"' . $selected . '>' . stripslashes($row['title']) . '</option>';
            }
            $result .= '</optgroup>';
        }

    }

    $db->table = "document";
    $db->condition = "`is_active` = 1 AND `parent` = 0";
    $db->order = "`created_time` DESC";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {
        $selected = '';
        if(intval($row['document_id'])==$choice) $selected = ' selected';
        $result .= '<option value="' . intval($row['document_id']) . '"' . $selected . '>' . stripslashes($row['title']) . '</option>';
    }
    $result .= '</select>';

    return $result;
}