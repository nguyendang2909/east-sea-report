<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function memberUser($act, $typeFunc, $user_id, array $role_id, $user_name, $full_name, $gender, $birthday, $identity_card, $date_of_issue, $email, $phone, $address, $city, array $agency, $note, $is_show, $img, $error) {
	global $mmenu, $tth_province;
    $agency = str_replace('"', "'", json_encode($agency));
?>
<div class="row">
	<div class="col-lg-8 col-md-10">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-user fa-fw"></i> Thông tin thành viên
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form id="memberUser" class="form-ol-3w" action="<?php echo $act;?>" method="post" enctype="multipart/form-data" name="member">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc;?>">
						<input type="hidden" name="user_id" value="<?php echo $user_id;?>">
						<input type="hidden" name="img" value="<?php echo $img;?>">
						<div class="panel-show-error"><?php echo $error;?></div>
						<table class="table table-no-border table-hover">
							<tr>
								<td width="150px" align="right"><label class="form-lb-tp">Tên đăng nhập:</label></td>
								<td><input class="form-control required" type="text" name="user_name" id="user_name" autocomplete="off" value="<?php echo stripslashes($user_name)?>" maxlength="16" required></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Mật khẩu mới:</label></td>
								<td><input class="form-control required" type="password" name="password" id="password" value="" autocomplete="off" maxlength="16"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Nhập lại mật khẩu:</label></td>
								<td><input class="form-control required" type="password" name="rePassword" id="rePassword" value="" autocomplete="off" maxlength="16"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Nhóm nhân viên:</label></td>
								<td><?php echo groupAdminSelect($role_id);?></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Họ và tên:</label></td>
								<td><input class="form-control required" type="text" name="full_name" id="full_name" value="<?php echo stripslashes($full_name)?>" autocomplete="off" maxlength="150" required></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Giới tính:</label></td>
								<td>
									<select class="form-control" name="gender" id="gender" style="width: 150px;">
										<option value="0" <?php echo $gender==0?"selected":""?>>Khác...</option>
										<option value="1" <?php echo $gender==1?"selected":""?>>Nam</option>
										<option value="2" <?php echo $gender==2?"selected":""?>>Nữ</option>
									</select>
								</td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Ngày sinh:</label></td>
								<td><input class="form-control input-datetime" type="text" name="birthday" style="width: 150px;" value="<?php echo $birthday?>" autocomplete="off"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Số CMND:</label></td>
								<td><input class="form-control auto-number" type="text" name="identity_card" style="width: 150px;" value="<?php echo $identity_card;?>" data-a-sep="" data-v-max="999999999999999" data-v-min="0" maxlength="15"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Ngày cấp:</label></td>
								<td><input class="form-control input-datetime" type="text" name="date_of_issue" style="width: 150px;" value="<?php echo $date_of_issue;?>" autocomplete="off"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Email:</label></td>
								<td><input class="form-control" type="email" name="email" id="email" value="<?php echo stripslashes($email)?>" autocomplete="off" maxlength="200" ></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Số điện thoại:</label></td>
								<td><input class="form-control" type="text" name="phone" id="phone" value="<?php echo stripslashes($phone)?>" autocomplete="off" maxlength="20"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Địa chỉ:</label></td>
								<td><input class="form-control" type="text" name="address" id="address" value="<?php echo stripslashes($address)?>" autocomplete="off" maxlength="200"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Thành phố:</label></td>
								<td>
									<?php
									echo '<select class="form-control selectpicker" data-live-search="true" name="city" id="city" style="width: 180px;">';
									foreach ($tth_province as $key => $value) {
									    $selected = '';
									    if($city==$key)
                                            $selected = ' selected';
                                        echo '<option value="' . $key . '"' . $selected . '>' . $value . '</option>';
                                    }
									echo '</select>';
									?>
								</td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Thuộc đơn vị:</label></td>
								<td><input id="agency" class="form-control easyui-combotree" data-options="url:'/action.php?url=list_agency',method:'get',multiple:true,cascadeCheck:false,value:<?php echo $agency;?>,animate:true,lines:true" name="list[]" required style="width: 100%;"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Giới thiệu:</label></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2">
									<textarea class="form-control" name="note" id="note"><?php echo stripslashes($note)?></textarea>
								</td>
							</tr>
							<tr>
								<td align="right" class="ver-top"><label class="form-lb-tp">Hình đại diện:</label></td>
								<td>
									<input class="form-control file file-img" type="file" name="img" data-show-upload="false" data-max-file-count="1" accept="image/*">
								</td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Hiển thị:</label></td>
								<td>
									<label class="radio-inline"><input type="radio" name="is_show" value="0" <?php echo $is_show==0?"checked":""?> > Đóng</label>
									<label class="radio-inline"><input type="radio" name="is_show" value="1" <?php echo $is_show==1?"checked":""?> > Mở</label>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round" id="user">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['account']['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/members.js"></script>
<script>
	CKEDITOR.replace('note', {
	height: 150,
		toolbar: [
		[ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
		[ 'FontSize', 'TextColor', 'BGColor' ]
	]
	});
	$('.input-datetime').datetimepicker({
		mask: '39/19/9999',
		lang: 'vi',
		timepicker: false,
		format: '<?php echo TTH_DATE_FORMAT;?>'
	});
	$('.file-img').fileinput({
		<?php if($img!='-no-' && $img!='') { ?>
		initialPreview: [
			"<img src='../uploads/user/<?php echo $img?>' class='file-preview-image' alt='<?php echo $img?>'>"
		],
		<?php } ?>
		allowedFileExtensions : ['jpg', 'png','gif']
	});
	<?php
    if($typeFunc=='add') echo "window.onload = check_add_user();";
    else echo "window.onload = check_edit_user();";
    ?>
</script>
<?php
}

function groupAdminSelect(array $id) {
	global $db;
    $result = '';

    $result .= '<select name="role[]" id="role" class="selectpicker required label-right-10" multiple data-live-search="true" data-selected-text-format="count" data-live-search-placeholder="Tìm..." title="Nhóm nhân viên..." required>';
	$db->table      = "core_role";
    $db->condition  = "`is_active` = 1";
	$db->order      = "`name` ASC";
	$db->limit      = "";
	$rows           = $db->select();
	foreach($rows as $row) {
        $selected = '';
        if(in_array($row['role_id'], $id)) $selected = ' selected';
        $result .= '<option value="' . intval($row['role_id']) . '"' . $selected . '>&rarr; ' . stripslashes($row['name']) . '</option>';
    }
    $result .= '</select>';

	return $result;
}