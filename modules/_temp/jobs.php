<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function jobs($act, $typeFunc, $jobs_id, $title, $list, $level, $begin, $end, array $files, $note, $error) {
	global $db, $mmenu;
    $list = str_replace('"', "'", json_encode($list));
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-pencil-square-o fa-fw"></i> Giao công việc
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>">
						<input type="hidden" name="jobs_id" value="<?php echo $jobs_id?>">
						<div class="panel-show-error"><?php echo $error?></div>
						<table class="table table-no-border table-hover">
                            <tr>
                                <td width="170px" align="right"><label class="form-lb-tp">Tên công việc:</label></td>
                                <td><input class="form-control" type="text" name="title" value="<?php echo stripslashes($title);?>" autocomplete="off" required></td>
                            </tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Người nhận:</label></td>
                                <td><input class="form-control easyui-combotree" data-options="url:'/action.php?url=list_agency_user',method:'get',multiple:true,cascadeCheck:false,value:<?php echo $list;?>,animate:true,lines:true" name="list[]" required style="width: 100%;"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Mức độ ưu tiên:</label></td>
								<td>
									<select class="selectpicker" name="level">
										<option value="0" selected data-content="<span class='lb-level green'>Bình thường</span>"<?php if($level==0) echo ' selected';?>>Bình thường</option>
										<option value="1" data-content="<span class='lb-level orange'>Khá</span>"<?php if($level==1) echo ' selected';?>>Khá</option>
										<option value="2" data-content="<span class='lb-level red'>Cao</span>"<?php if($level==2) echo ' selected';?>>Cao</option>
									</select>
								</td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Bắt đầu:</label></td>
                                <td><input class="form-control ip-begin-datetime" type="text" name="begin" style="width: 150px;" value="<?php echo $begin;?>" maxlength="16" autocomplete="off"></td>
                            </tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Hạn chốt:</label></td>
								<td><input class="form-control ip-end-datetime" type="text" name="end" style="width: 150px;" value="<?php echo $end;?>" maxlength="16" autocomplete="off"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Têp đính kèm:</label></td>
								<td><input class="form-control file" type="file" name="files[]" data-show-upload="false" data-show-preview="false" multiple data-max-file-count="10" value="<?php echo $files;?>" placeholder="Chọn file..."></td>
							</tr>
							<tr>
								<td class="ver-top" align="right"><label class="form-lb-tp">Ghi chú:</label></td>
								<td><textarea class="form-control" name="note" rows="5"><?php echo stripslashes($note);?></textarea></td>
							</tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['jobs']['link'] . $mmenu['jobs']['sub'][1]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
    var map = new mapboxgl.Map({
        container: 'map',
        style: '/map/style-cdn.json',
        attributionControl: false,
        center: [<?php echo $lng;?>, <?php echo $lat;?>],
        zoom: 14,
        hash: true
    });
    map.addControl(new mapboxgl.NavigationControl());

    var marker = new mapboxgl.Marker({
        draggable: true
    }).setLngLat([<?php echo $lng;?>, <?php echo $lat;?>]).addTo(map);

    function onDragEnd() {
        var lngLat = marker.getLngLat();
        $('input[name="lng"]').val(lngLat.lng);
        $('input[name="lat"]').val(lngLat.lat);
    }

    marker.on('dragend', onDragEnd);
</script>
<script>
    jQuery(function(){
        $('.ip-begin-datetime').datetimepicker({
            format  : 'd/m/Y H:i',
            lang    : 'vi',
            onShow  : function( ct ){
                this.setOptions({
                    minDate : '+01/01/1970',
                    maxDate : $('.ip-end-datetime').val() ? $('.ip-end-datetime').val() : false,
                    format  : 'd/m/Y H:i',
                    formatDate : 'd/m/Y H:i'
                })
            }
        });
        $('.ip-end-datetime').datetimepicker({
            format  : 'd/m/Y H:i',
            lang    : 'vi',
            onShow  :function( ct ){
                this.setOptions({
                    minDate : $('.ip-begin-datetime').val()?$('.ip-begin-datetime').val():'+01/01/1970',
                    maxDate : false,
                    format  : 'd/m/Y H:i',
                    formatDate : 'd/m/Y H:i'
                })
            }
        });
    });
	$('.file').fileinput({
		<?php if($files !='' && $files !='-no-') echo 'initialPreview: ' . json_encode($files) . ','; ?>
        maxFileSize: 10240
    });
</script>
<?php
}