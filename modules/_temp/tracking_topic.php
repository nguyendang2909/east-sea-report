<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function trackingTopic($act, $typeFunc, $topic_id, $content, $archives, $created_date, $note, $keywords,$files, $error) {
	global $mmenu;
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-dot-circle-o fa-fw"></i> Chủ đề (theo chỉ thị cấp trên)
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" enctype="multipart/form-data" class="form-ol-3w">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>" />
						<input type="hidden" name="topic_id" value="<?php echo $topic_id?>" />
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
							<tr>
								<td align="right" class="ver-top" width="150px"><label class="form-lb-tp">Nội dung:</label></td>
                                <td>
                                    <textarea class="form-control" rows="3" name="content" required><?php echo stripslashes($content)?></textarea>
                                </td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Theo công văn:</label></td>
                                <td><input class="form-control" type="text" name="archives" value="<?php echo stripslashes($archives);?>"></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Thời gian thực hiện:</label></td>
                                <td><input class="form-control input-date" type="text" name="created_date" style="width: 150px;" value="<?php echo $created_date?>" maxlength="10" autocomplete="off"></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Thông tin:</label></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2"><textarea id="note" class="form-control" rows="3" name="note"><?php echo stripslashes($note)?></textarea></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Từ khóa:</label></td>
                                <td><input class="form-control" type="text" name="keywords" data-role="tagsinput" maxlength="255" value="<?php echo stripslashes($keywords)?>"></td>
                            </tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Têp tin:</label></td>
								<td><input class="form-control file" type="file" name="files" data-show-upload="false" data-show-preview="false" data-max-file-count="1" value="<?php echo $files;?>" placeholder="Chọn file..."></td>
							</tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][2]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('.file').fileinput({
		<?php if($files !='' && $files !='-no-') echo 'initialPreview: ["' . $files . '"]'; ?>
	});
    $('.input-date').datetimepicker({
        mask: '39/19/9999',
        lang: 'vi',
        timepicker: false,
        format: '<?php echo TTH_DATE_FORMAT;?>'
    });
    CKEDITOR.replace('note', {
        height: 150,
        toolbar: [
            [ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
            [ 'FontSize', 'TextColor', 'BGColor' ],
            [ 'Image' ]
        ]
    });
</script>
<?php
}
