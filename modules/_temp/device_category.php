<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function deviceCat($act, $typeFunc, $device_id, $title, $note, $error) {
	global $mmenu;
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-desktop fa-fw"></i> Nhóm thiết bị phần cứng
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>" />
						<input type="hidden" name="device_id" value="<?php echo $device_id?>" />
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
							<tr>
								<td align="right" width="150px"><label class="form-lb-tp">Tên nhóm:</label></td>
                                <td><input class="form-control" type="text" name="title" value="<?php echo stripslashes($title);?>" autocomplete="off" required></td>
							</tr>
                            <tr>
                                <td align="right" class="ver-top"><label class="form-lb-tp">Ghi chú:</label></td>
                                <td>
                                    <textarea class="form-control" rows="3" name="note"><?php echo stripslashes($note)?></textarea>
                                </td>
                            </tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][2]['sub'][1]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}
