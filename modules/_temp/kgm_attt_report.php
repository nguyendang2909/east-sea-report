<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

function kgmAtttReport($act, $typeFunc, $newsId, $newsSummary, $newsContent, $newsRecommendation, $newsSource, $categoryId, $reportDate, $note, $files, $error)
{
  global $mmenu;
  if (empty($agency)) $agency = "''";
?>
  <div class="row">
    <div class="col-lg-6 col-md-9">
      <div class="panel">
        <div class="panel-heading">
          <i class="fa fa-sitemap fa-fw"></i> Báo cáo KGM ATTT
          <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
            <a href="javascript:;" class="fa fa-chevron-down"></a>
            <a href="javascript:;" class="fa fa-eye"></a>
            <a href="javascript:;" class="fa fa-compress"></a>
            <a href="javascript:;" class="fa fa-times"></a>
          </span>
        </div>
        <div class="panel-body">
          <div class="form-responsive">
            <form action="<?php echo $act ?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
              <input type="hidden" name="typeFunc" value="<?php echo $typeFunc ?>" />
              <input type="hidden" name="newsId" value="<?php echo $newsId ?>" />
              <div class="panel-show-error">
                <?php echo $error ?>
              </div>
              <table class="table table-no-border table-hover">
                <tr>
                  <td align="right"><label class="form-lb-tp">Ngày báo cáo</label></td>
                  <td><input class="form-control report-date-picker" type="text" name="reportDate" style="width: 150px;" value="<?php echo stripslashes($reportDate); ?>" maxlength="16" autocomplete="off"></td>
                </tr>

                <tr>
                  <td align="right"><label class="form-lb-tp">Loại tin</label></td>
                  <td><?php echo selectKgmAtttReportCategory($categoryId); ?></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Tóm tắt tin</label></td>
                  <td><input class="form-control" type="text" name="newsSummary" value="<?php echo stripslashes($newsSummary); ?>" required></td>
                </tr>

                <tr>
                  <td align="right" class="ver-top"><label class="form-lb-tp">Nội dung tin:</label></td>
                  <td><textarea class="form-control" rows="5" name="newsContent"><?php echo stripslashes($newsContent) ?></textarea></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Khuyến cáo</label></td>
                  <td><input class="form-control" type="text" name="newsRecommendation" value="<?php echo stripslashes($newsRecommendation); ?>" required></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Nguồn tin</label></td>
                  <td><input class="form-control" type="text" name="newsSource" value="<?php echo stripslashes($newsSource); ?>" required></td>
                </tr>

                <tr>
                  <td align="right" class="ver-top"><label class="form-lb-tp">Ghi chú:</label></td>
                  <td><textarea class="form-control" rows="3" name="note"><?php echo stripslashes($note) ?></textarea></td>
                </tr>

                <tr>
                  <td align="right"><label class="form-lb-tp">Tệp tin:</label></td>
                  <td><input class="form-control file" type="file" name="files" data-show-upload="false" data-show-preview="false" data-max-file-count="1" value="<?php echo $files; ?>" placeholder="Chọn file..."></td>
                </tr>
                <tr>
                  <td colspan="2" class="form-ol-btn-tzc">
                    <button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
                    <button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
                    <button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['report']['link'] . '/kgm-attt-report'; ?>'">Thoát</button>
                  </td>
                </tr>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    $('.input-date').datetimepicker({
      mask: '39/19/9999',
      lang: 'vi',
      timepicker: false,
      format: '<?php echo TTH_DATE_FORMAT; ?>',
      maxDate: '+01/01/1970'
    });
    $('.file').fileinput({
      <?php if ($files != '' && $files != '-no-') echo 'initialPreview: ["' . $files . '"]'; ?>
    });
  </script>
<?php
}


function selectKgmAtttReportCategory($choice)
{
  global $db;
  $result = '';

  $result .= '<select name="categoryId" class="form-control selectpicker" data-live-search="true" data-selected-text-format="count" data-live-search-placeholder="Tìm..." title="Chọn loại tin...">';
  $db->table = "kgm_attt_report_categories";
  $db->condition = "`isActive` = 1";
  $db->order = "`categoryId` ASC";
  $db->limit = "";
  $rows = $db->select();
  if ($db->RowCount > 0) {
    foreach ($rows as $row) {
      $selected = '';
      if (($row["categoryId"] == $choice)) $selected = ' selected';

      $result .= '<option value="' . intval($row["categoryId"]) . '"' . $selected . '>' . stripslashes($row["categoryName"]) . '</option>';
    }
  }
  $result .= '</select>';
  return $result;
}
?>

<script>
  jQuery(function() {
    $('.report-date-picker').datetimepicker({
      timepicker: false,
      format: 'dd/mm/YYYY',
      lang: 'vi',
      onShow: function(ct) {
        this.setOptions({
          minDate: '+01/01/1970',
          format: 'd/m/Y',
          formatDate: 'd/m/Y'
        })
      }
    });
  });
</script>